import React from "react";
import { makeStyles } from "@mui/styles";
import { AppBar, Toolbar, Grid, Typography, Button } from "@mui/material";
import { useHistory } from "react-router-dom";
import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew";
import HomeWorkOutlinedIcon from '@mui/icons-material/HomeWorkOutlined';
import HomeOutlinedIcon from '@mui/icons-material/HomeOutlined';
import CommentOutlinedIcon from '@mui/icons-material/CommentOutlined';
import PowerSettingsNewOutlinedIcon from '@mui/icons-material/PowerSettingsNewOutlined';

const useStyles = makeStyles((theme) => ({
  root: {
    background: theme.palette.primary.dark,
    zIndex: 1,
    position: "fixed",
    top: 0,
    boxShadow: "none",
    borderBottom: "1px solid #FFFFFF29",
  },
  text: {
    fontSize: 14,
    marginLeft: 4,
    cursor: "pointer",
    position: "relative",
    display: 'flex',
    alignItems: 'center',

  },
  active: {
    borderBottom: `2px solid ${theme.palette.tertiary.main}`,
    bottom: "-19px",
    position: "absolute",
    zIndex: 1000,
    width: "100%",
  },
  activeText: {
    color: theme.palette.tertiary.main,
    display: 'flex',
    alignItems: 'center',
    cursor: 'pointer'
  },
  back: {
    color: theme.palette.tertiary.main,
    display: "flex",
    alignItems: "center",
    fontSize: "16px",
    cursor: "pointer",
  },
}));

export const TopNavBar = (props) => {
  const classes = useStyles();
  const history = useHistory();

  const menuData = (active) => {
    return [
      {
        name: "Home",
        link: "/",
        // eslint-disable-next-line jsx-a11y/alt-text
        icon: (
          <HomeOutlinedIcon />
        ),
      },
      {
        name: "My Property",
        link: "/myproperties",
        // eslint-disable-next-line jsx-a11y/alt-text
        icon: (
          // eslint-disable-next-line jsx-a11y/alt-text
          <HomeWorkOutlinedIcon />
        ),
      },
      {
        name: "Chat",
        link: "/profile",
        // eslint-disable-next-line jsx-a11y/alt-text
        icon: (
          // eslint-disable-next-line jsx-a11y/alt-text
          <CommentOutlinedIcon />
        ),
      },
      // {
      //   name: "Profile",
      //   link: "/profile",
      //   // eslint-disable-next-line jsx-a11y/alt-text
      //   icon: (
      //     // eslint-disable-next-line jsx-a11y/alt-text
      //     <PersonOutlinedIcon />
      //   ),
      // },
    ];
  };

  const onListClick = (data, index) => {
    if (data.link) {
      history.push(data.link);
    }
  };

  const isSelected = (data) => {
    if (data.link) {
      return window.location.pathname === data.link;
    }
  };

  const goBack = () => {
    history.goBack(-1);
  };
  const logout = () => {
    localStorage.clear();
    history.push('/login')
  }
  return (
    <AppBar position="fixed" className={classes.root}>
      <Toolbar>
        {props?.isBack ? (
          <>
          {
            props?.isComponent ? props?.component :  <div className={classes.back} onClick={goBack}>
            <ArrowBackIosNewIcon style={{ fontSize: "16px" }} />
            &nbsp;Back
          </div>
          }
          </>

        ) : (
          <>
            <Grid container spacing={6} justifyContent="center" alignItems='center'>
              {(menuData() || []).map((navBar, index) => (
                <Grid item onClick={() => onListClick(navBar, index)} style={{ cursor: 'pointer' }}>
                  <Typography
                    className={`${classes.text} ${isSelected(navBar) && classes.activeText
                      }`}
                  >
                    {menuData(isSelected(navBar))?.[index]?.icon} <span style={{ marginLeft: '10px', cursor: 'pointer' }} >{navBar.name}</span>
                    <div className={isSelected(navBar) && classes.active}> </div>
                  </Typography>
                </Grid>
              ))}
            </Grid>
            <Button><PowerSettingsNewOutlinedIcon onClick={()=>logout()}/></Button>

          </>
        )}
      </Toolbar>
    </AppBar>
  );
};
