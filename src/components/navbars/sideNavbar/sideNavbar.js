import React from "react";
import { makeStyles } from "@mui/styles";
import { Paper, BottomNavigation, BottomNavigationAction } from "@mui/material";
import { useHistory } from "react-router-dom";
import HomeWorkOutlinedIcon from '@mui/icons-material/HomeWorkOutlined';
import HomeOutlinedIcon from '@mui/icons-material/HomeOutlined';
import CommentOutlinedIcon from '@mui/icons-material/CommentOutlined';
const useStyles = makeStyles((theme) => ({
  root: {
    "& .Mui-selected": {
      color: theme.palette.tertiary.main,
    },
    position: "fixed",
    bottom: 0,
    left: 0,
    right: 0,
    boxShadow: "none",
    borderTop: "0.5px solid #47525d",
    backgroundColor:"#2F363E"
  },
  background: {
    background: theme.palette.primary.dark,
    cursor:'pointer'
  },
  text: {
    fontSize: 12,
    marginTop: 4,
  },
  icon: {
    fontSize: 10,
  },
  activeText: {
    borderTop: `2px solid ${theme.palette.tertiary.main}`,
    cursor:'pointer',
    color: theme.palette.tertiary.main,
  },
}));

export const SideNavBar = () => {
  const classes = useStyles();

  const navigate = useHistory();
  const [active, setActive] = React.useState(0);

  const menuData = (active) => {
    return [
      {
        name: "Home",
        link: "/",
        // eslint-disable-next-line jsx-a11y/alt-text
        icon: (
          <HomeOutlinedIcon />
        ),
      },
      {
        name: "My Property",
        link: "/myproperties",
        // eslint-disable-next-line jsx-a11y/alt-text
        icon: (
          // eslint-disable-next-line jsx-a11y/alt-text
          <HomeWorkOutlinedIcon />
        ),
      },
      {
        name: "Chat",
        link: "/profile",
        // eslint-disable-next-line jsx-a11y/alt-text
        icon: (
          // eslint-disable-next-line jsx-a11y/alt-text
          <CommentOutlinedIcon />
        ),
      },
    ];
  };

  const onListClick = (data, index) => {
    setActive(index);
    if (data.link) {
      navigate.push(data.link);
    }
  };

  const isSelected = (data) => {
    if (data.link) {
      return window?.location?.pathname === data?.link;
    }
  };

  return (
    <Paper className={classes.root}>
      <BottomNavigation value={active} showLabels sx={{backgroundColor:"#22252C"}}>
        {(menuData() || []).map((navBar, index) => (
          <BottomNavigationAction
            className={`${isSelected(navBar) && classes.activeText} ${
              classes.background
            }`}
            onClick={() => onListClick(navBar, index)}
            label={<div className={classes.text}>{navBar.name}</div>}
            icon={
              <div className={classes.icon}>
                {menuData(isSelected(navBar))?.[index]?.icon}
              </div>
            }
          />
        ))}
      </BottomNavigation>
    </Paper>
  );
};
