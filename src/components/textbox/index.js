import { TextField, Typography } from "@mui/material";
import makeStyles from "@mui/styles/makeStyles";
import PropTypes from "prop-types";
import React from "react";
import './textbox.css'
const useStyles = makeStyles((theme) => ({
  Label: {
    fontSize: "12px",
    color: theme.palette.tertiary.main,
  },
  text: {},
  textbox: {
    color: "#fff",
    border: "1px solid #3E434C",
    borderRadius: 6,
    '& .MuiOutlinedInput-input':{
      color: "#fff",
      '&:focus-visible':{
        border: "none !important",
        outline:'none !important'
      },

    }
  },
}));

export const TextBox = (props) => {
  const classes = useStyles(props);

  const getLabel = (props) => {
    return (
      <Typography variant="body1" className={classes.Label} gutterBottom noWrap>
        {props.label}
        {props.isrequired && (
          <Typography variant="caption" style={{ color: "red", marginLeft: 4 }}>
            *
          </Typography>
        )}
      </Typography>
    );
  };

  return (
    <div className={classes.root}>
      <>
        {getLabel(props)}
        <div className={classes.text}>
          <TextField
            className={classes.textbox}
            type={props.type}
            onKeyPress={(e) => {
              if (props.type === "number") {
                if (e.key === "e") {
                  e.preventDefault();
                }
              }
            }}
            autoComplete={false}
            id={props.id}
            placeholder={props.placeholder}
            variant={props.variant ?? "outlined"}
            fullWidth
            InputLabelProps={{
              shrink: false,
            }}
            inputProps={{
              readOnly: props?.isReadonly ?? false,
            }}
            InputProps={{
              endAdornment: props?.endAdornment,
            }}
            disabled={props?.isReadonly ?? false}
            size="small"
            multiline={props.multiline}
            rows={5}
            rowsMax={10}
            onChange={props.onChange}
            value={props.value}
            // onKeyPress={props?.onKeyPress}
          />
        </div>
      </>
      {props.isError && (
        <Typography variant={"caption"} color={"error"}>
          {props.errorMessage}
        </Typography>
      )}
    </div>
  );
};

TextBox.propTypes = {
  value: PropTypes.string,
  label: PropTypes.string,
  id: PropTypes.string,
  placeholder: PropTypes.string,
  multiline: PropTypes.bool,
  type: PropTypes.string,
  isReadonly: PropTypes.bool,
  onChange: PropTypes.func,
};
TextBox.defaultProps = {
  label: "Textbox",
  multiline: false,
  type: "text",
  placeholder: "Type Here...",
};
