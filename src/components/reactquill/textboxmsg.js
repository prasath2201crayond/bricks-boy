import { Typography, Box } from "@mui/material";
import makeStyles from "@mui/styles/makeStyles";
import React from "react";
import Picker from "emoji-picker-react";
import MoodIcon from '@mui/icons-material/Mood';
import SendIcon from '@mui/icons-material/Send';
import './reactquill.css';
import {AlertDialog}from '../../components'
const useStyles = makeStyles((theme) => ({
      Label: {
            color: "#0D9E8D",
            fontSize: "12px",
      },
      text: {
            borderRadius: "12px",
            '&.ql-toolbar': {
                  borderRadius: "12px 12px 0px 0px"
            }
      },
      editor: {
            backgroundColor: '#22252C',
            fontFamily:'crayond_medium',
            width:'100%',
            border: 'none !important',
            outline: 'none !important',
            '&input:focus-visible': {
                  border: 'none !important',
                  outline: 'none !important'
            },
            '&input:focus': {
                  border: 'none !important',
                  outline: 'none !important'
            }
      },
      sendBox: {
            backgroundColor: '#0D9E8D',
            padding: '5px',
            borderRadius: '4px',
            display: 'flex',
            aliginItems: 'center',
            justifyContent: 'center',
            marginLeft: '10px',
            cursor: 'pointer',
      },
      textFieldMain:{
            border: '2px solid #2F363E !important',
            maxWidth: '500px',
            width:'100%',
            margin: '0 auto',
            borderRadius: '12px',
            position:'relative',
            height: '60px',
            padding:'15px',
            backgroundColor:'#22252C'
      },
      options:{
            position:'absolute',
            right:'14px',
            top:'14px',
            bottom:0
      }
}));


export const MessageBox = (props) => {


      const classes = useStyles(props);




      const getLabel = (props) => {
            return (
                  <Typography variant="body1" className={classes.Label} gutterBottom>
                        {props?.label ?? null}
                        {props.isrequired && (
                              <Typography variant="caption" style={{ color: "red" }}>
                                    *
                              </Typography>
                        )}
                  </Typography>
            );
      };

      return (
            <div className={classes.root}>
                  <AlertDialog isNormal  isnotTitle open={props?.show} onClose={()=>props?.setShow(false)} component={<Picker onEmojiClick={props?.onEmojiClick} onClose={()=>props?.setShow(false)}/>}/>
                  <>
                        {getLabel(props)}
                        <div className={classes.textFieldMain}>
                              <input value={props?.value} onChange={props?.handleChange} className={classes.editor} />
                              <Box className={classes.options}>
                                    <Box sx={{ display: 'flex', alignItems: 'center' }}>
                                          <MoodIcon onClick={() => props?.setShow(true)} sx={{ color: '#0D9E8D',zIndex:1,cursor:'pointer'  }} />
                                          <span className={classes.sendBox} onClick={()=>props?.sendData()}><SendIcon sx={{ color: '#fff',fontSize:'16px' }} /></span>
                                    </Box>
                              </Box>


                        </div>
                  </>
                  {props.isError && (
                        <Typography variant={"caption"} color={"error"}>
                              {props.errorMessage}
                        </Typography>
                  )}
            </div>
      );
};


MessageBox.defaultProps = {
      label: "",
      multiline: false,
      type: "text",
      placeholder: "Type Here...",
};
