import { Typography } from "@mui/material";
import makeStyles from "@mui/styles/makeStyles";
import PropTypes from "prop-types";
import ImageResize from "quill-image-resize-module-react";
import React from "react";
import ReactQuill, { Quill } from "react-quill";
import quillEmoji from "react-quill-emoji";
import "react-quill-emoji/dist/quill-emoji.css";
import "react-quill/dist/quill.snow.css";
import './reactquill.css'
const useStyles = makeStyles((theme) => ({
  Label: {
    color: "#0D9E8D",
    fontSize: "12px",
  },
  text: {
    borderRadius: "12px",
    '&.ql-toolbar':{
      borderRadius: "12px 12px 0px 0px"
    }
  },

  textbox: {
    [`& fieldset`]: {
      borderRadius: "10px",
      height: (props) => (props.multiline ? "unset" : 50),
      border: "1px solid #CED3DD",
      "& :hover": {
        border: "1px solid #5078E1",
      },
    },
    "& .MuiOutlinedInput-input": {
      padding: "11px 14px",
    },
  },
}));


export const Editor = (props) => {
  Quill.register("modules/imageResize", ImageResize);
  Quill.register(
    {
      "formats/emoji": quillEmoji.EmojiBlot,
      "modules/emoji-toolbar": quillEmoji.ToolbarEmoji,
      "modules/emoji-textarea": quillEmoji.TextAreaEmoji,
      "modules/emoji-shortname": quillEmoji.ShortNameEmoji,
    },
    true
  );

  const classes = useStyles(props);

  const getLabel = (props) => {
    return (
      <Typography variant="body1" className={classes.Label} gutterBottom>
        {props?.label ?? null}
        {props.isrequired && (
          <Typography variant="caption" style={{ color: "red" }}>
            *
          </Typography>
        )}
      </Typography>
    );
  };

  return (
    <div className={classes.root}>
      <>
        {getLabel(props)}
        <div>
          <ReactQuill
           className={classes.text}
           value={props?.value ?? ''}
            modules={{
              toolbar: {
                container: [
                  [{ header: [1, 2, false] }],
                  [{ color: [] }, { background: [] }],
                  ["bold", "italic"],
                  ["link"],
                  ["emoji"],
                ],
              },
              "emoji-toolbar": true,
              "emoji-textarea": true,
              "emoji-shortname": true,
            }}
            theme="snow"
            onChange={(value, a, b, c) => {
              if (b === "user") {
                props?.handleChange(value, a, b, c)
              }
            }}
            id={props?.id}
          />
        </div>
      </>
      {props.isError && (
        <Typography variant={"caption"} color={"error"}>
          {props.errorMessage}
        </Typography>
      )}
    </div>
  );
};

Editor.propTypes = {
  value: PropTypes.string,
  label: PropTypes.string,
  id: PropTypes.string,
  placeholder: PropTypes.string,
  multiline: PropTypes.bool,
  type: PropTypes.string,
  isReadonly: PropTypes.bool,
   //onChange: PropTypes.func,
};
Editor.defaultProps = {
  label: "",
  multiline: false,
  type: "text",
  placeholder: "Type Here...",
};
