import React from 'react'
import { Box, Avatar, Typography, Badge } from "@mui/material";
import makeStyles from "@mui/styles/makeStyles";
import moment from "moment";
const useStyles = makeStyles((theme) => ({
      root: {
            padding: '10px',
            borderRadius: '12px',
            backgroundColor: "#2F363E",
            border: '1px solid #FFFFFF0A',
            display: 'flex',
            alignItems: "center",
            justifyContent: 'space-between',
            margin:'10px',
            cursor:'pointer'
      },
      name: {
            color: "#fff",
            marginLeft: "10px"
      },
      date: {
            color: "#fff",
            fontSize: "10px",
            marginBottom:'5px'
      },
      msg:{
            backgroundColor:"#0D9E8D",
            fontSize:'10px',
            margin:'0 auto',
      },
      root1: {
            padding: '10px',
            borderRadius: '12px',
            backgroundColor: "#2F363E",
            border: '1px solid #146c62',
            display: 'flex',
            alignItems: "center",
            justifyContent: 'space-between',
            margin:'10px',
            cursor:'pointer'
      },
}));

export const UserCard = (props) => {
      const classes = useStyles();
      console.log(navigator.onLine)
      return (
            <Box className={props?.data?._id === props?.user?.id ? classes.root1 : classes.root} onClick={()=>props?.selectuser(props?.data)}>
                  <Box display='flex' alignItems='center'>
                        <Box>
                              <Badge
                                    color={navigator?.onLine ? "secondary":"primary"} overlap="circular" badgeContent=" " variant="dot"
                                    anchorOrigin={{
                                          vertical: 'bottom',
                                          horizontal: 'right',
                                    }}
                              >
                                    <Avatar alt="Travis Howard" src={props?.data?.profileimage} />
                              </Badge>

                        </Box>
                        <Box>
                              <Typography className={classes.name}>{props?.data?.name}</Typography>
                        </Box>
                  </Box>
                  <Box textAlign={'right'}>
                        <Typography className={classes.date}>{moment(new Date()).format("DD MMM YY,hh:mm")}</Typography>
                        <Avatar className={classes.msg} sx={{ width: 24, height: 24 }}> 1</Avatar>
                  </Box>
            </Box>
      )
}