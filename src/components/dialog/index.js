import * as React from "react";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import makeStyles from "@mui/styles/makeStyles";
import DialogTitle from "@mui/material/DialogTitle";

const useStyles = makeStyles((theme) => ({
  dialog: {
    // width: "100%",
    // maxWidth: "auto",
    padding: "0 !important",
    "& .MuiPaper-root": {
      // maxWidth: "931px !important",
      // width: "100% !important",
      borderRadius: "12px",
    },
    "& .MuiDialogContent-root": {
      padding: "0px !important",
      position: 'relative'
    },
  },
  header: {
    border: "1px solid #E4E8EE",
    fontSize: "16px",
    fontFamily: "tenant_extraBold",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
}));
export function AlertDialog(props) {
  const classes = useStyles();
  return (
    <div>
      <Dialog
        open={props?.open}
        onClose={props?.onClose}
        aria-labelledby="alert-dialog-title"
        className={classes.dialog}
        maxWidth={props?.isNormal ? "xs" : "lg"}
        fullWidth
        aria-describedby="alert-dialog-description"
      >
        {
          !props?.isnotTitle &&
          <DialogTitle className={classes.header}>
            <span>{props?.header}</span>{" "}
            <img
              src="/images/close.svg"
              alt="close"
              onClick={props?.onClose}
              style={{ cursor: 'pointer' }}
            />
          </DialogTitle>
        }

        <DialogContent style={{ padding: "0 !important" }}>
          {props?.component}
        </DialogContent>
      </Dialog>
    </div>
  );
}
