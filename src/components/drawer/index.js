import * as React from "react";
import { Drawer } from "@mui/material";

export function DrawerComponent(props) {
  return (
    <div>
      <React.Fragment>
        <Drawer
          onClose={props?.onClose}
          anchor={"bottom"}
          open={props?.open}
          style={{borderRadius:"10px 0px"}}
        >
          {props?.component}
        </Drawer>
      </React.Fragment>
    </div>
  );
}
