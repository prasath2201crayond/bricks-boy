import React from "react";
import { makeStyles } from "@mui/styles";
import Timeline from "@mui/lab/Timeline";
import TimelineItem from "@mui/lab/TimelineItem";
import TimelineSeparator from "@mui/lab/TimelineSeparator";
import TimelineConnector from "@mui/lab/TimelineConnector";
import TimelineContent from "@mui/lab/TimelineContent";
import TimelineDot from "@mui/lab/TimelineDot";
import { Grid, Hidden, Typography } from "@mui/material";

const useStyles = makeStyles((theme) => ({
  root: {
    background: theme.palette.primary.main,
    borderRadius: 10,
    margin: "18px 0px",
    position: "relative",
    padding: 16,
  },
  dispense: {
    color: "#B2C0CE",
  },
  dot: {
    border: "2px solid #D3AE7A",
  },
  doted: {
    color: "#D3AE7A",
    marginLeft: 6,
  },
  dot_line: {
    border: "1px dotted #D3AE7A",
  },
  view: {
    color: "#0D9E8D",
    cursor: "pointer",
    // [theme.breakpoints.down("sm")]: {
    //     margin: "auto",
    //   },
    fontSize: 14,
  },
  viewRight: {
    color: "#0D9E8D",
    cursor: "pointer",
    height: 40,
    textAlign: "center",
    [theme.breakpoints.up("sm")]: {
      position: "absolute",
      right: 18,
    },
    fontSize: 14,
  },
  grid: {
    [theme.breakpoints.down("sm")]: {
      margin: "auto",
    },
  },
}));

export const TimelineComponent = (props) => {
  const classes = useStyles();
  const [state, setState] = React.useState(false);
  const data = [1, 2, 3];
  return (
    <div className={classes.root} style={{ paddingBottom: state && 0 }}>
      {!state && (
        <Grid container spacing={2}>
          <Grid item xs={12} md={10}>
            <div style={{ display: "flex", color: "#fff" }}>
              <div className={classes.doted}>⬤</div>&nbsp;
              <div>
                {" "}
                First stage of loan&nbsp;
                <span className={classes.dispense}>dispense on</span> 27 May,
                2022
              </div>
            </div>
          </Grid>
          <Grid item className={classes.grid}>
            <Typography
              className={classes.viewRight}
              onClick={() => setState(!state)}
            >
              VIEW TIMELINE
            </Typography>
          </Grid>
        </Grid>
      )}
      <Hidden smDown>
        {state && (
          <Typography
            className={classes.viewRight}
            onClick={() => setState(!state)}
          >
            HIDE TIMELINE
          </Typography>
        )}
      </Hidden>

      {state &&
        data?.map((v, i) => {
          return (
            <Timeline style={{ padding: 0, color: "#fff" }}>
              <TimelineItem>
                <TimelineSeparator>
                  <TimelineDot variant="outlined" className={classes.dot} />
                  {data?.length - 1 !== i && (
                    <TimelineConnector
                      variant="TimelineDot"
                      className={classes.dot_line}
                    />
                  )}
                </TimelineSeparator>
                <TimelineContent>
                  First stage of loan&nbsp;
                  <span className={classes.dispense}>dispense on</span> 27 May,
                  2022
                </TimelineContent>
              </TimelineItem>
            </Timeline>
          );
        })}
      <Hidden mdUp>
        {state && (
          <Typography
            className={classes.viewRight}
            onClick={() => setState(!state)}
          >
            HIDE TIMELINE
          </Typography>
        )}
      </Hidden>
    </div>
  );
};
