
import RemoveRedEyeOutlinedIcon from "@mui/icons-material/RemoveRedEyeOutlined";
import VisibilityOffOutlinedIcon from "@mui/icons-material/VisibilityOffOutlined";
import {
      IconButton,
      InputAdornment,

      TextField,
      Typography
} from "@mui/material";
import makeStyles from '@mui/styles/makeStyles';
import PropTypes from "prop-types";
import React from "react";

const useStyles = makeStyles((theme) => ({
      Label: {
            fontSize: "12px",
            color: theme.palette.tertiary.main,
          },
      text: {

      },
      root: {
            textAlign: 'left',
      },
      textbox: {
            color: "#fff",
            border: "1px solid #3E434C",
            borderRadius: 6,
            '& .MuiOutlinedInput-input':{
              color: "#fff",
              '&:focus-visible':{
                border: "none !important",
                outline:'none !important'
              },

            }
          },

}));



export const Password = (props) => {

      const classes = useStyles(props);

      const getTitle = (props) => {
            return <div style={{ display: "flex" }}>
                  {
                        <Typography
                              variant="body1"
                              color="textsecondary"
                              className={classes.Label}
                              align="left"
                              gutterBottom
                        >
                              {props?.label}
                        </Typography>
                  }
                  {props?.isRequired && (
                        <Typography color="error" variant="caption">
                              &nbsp;*
                        </Typography>
                  )}
            </div>
      }
      const [showPassword, setShowPassword] = React.useState();

      const handleClickShowPassword = () => {
            setShowPassword(!showPassword);
      };
      const handleMouseDownPassword = (event) => {
            event.preventDefault();
      };

      return (
            <div className={classes.root}>
                  <>
                        {getTitle(props)}
                        <TextField
                              autoComplete="off"
                              className={classes.textbox}
                              id={props.id}
                              placeholder={props.placeholder}
                              variant={props.variant ?? "outlined"}
                              fullWidth
                              type={showPassword ? "text" : "password"}
                              InputLabelProps={{
                                    shrink: false
                              }}
                              size="small"
                              InputProps={{
                                    endAdornment: (
                                          <InputAdornment position="end">
                                                <IconButton
                                                      aria-label="Toggle password visibility"
                                                      onClick={handleClickShowPassword}
                                                      onMouseDown={handleMouseDownPassword}
                                                >
                                                      {showPassword ? (
                                                            <RemoveRedEyeOutlinedIcon sx={{color:'#0D9E8D'}}/>
                                                      ) : (
                                                            <VisibilityOffOutlinedIcon sx={{color:'#0D9E8D'}}/>
                                                      )}
                                                </IconButton>
                                          </InputAdornment>
                                    ),
                              }}
                              disabled={props?.disabled ?? false}


                              onChange={props.onChange}
                              value={props.value}


                        />
                  </>
                  {props.isError && <Typography variant={"caption"} color={"error"} maxWidth='200px'>{props.errorMessage}</Typography>}

            </div>
      );
};

Password.propTypes = {
      value: PropTypes.string,
      label: PropTypes.string,
      id: PropTypes.string,
      multiline: PropTypes.bool,
      type: PropTypes.string,
      isReadonly: PropTypes.bool,
      onChange: PropTypes.func,
};
Password.defaultProps = {
      label: "Textbox",
      multiline: false,
      type: "text",
};
