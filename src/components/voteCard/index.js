/* eslint-disable jsx-a11y/alt-text */
import React from "react";
import {
  Button,
  Card,
  CardActions,
  CardContent,
  Typography,
  Rating
} from "@mui/material";
import { makeStyles } from "@mui/styles";
import ReactQuill from "react-quill";

const useStyles = makeStyles((theme) => ({
  root: {
    background: "#2F363E",
    boxShadow: "none",
    border: "1px solid #FFFFFF0A",
    borderRadius: 8,
    borderTopLeftRadius: 0,
  },
  title: {
    color: "#FFFFFF",
    fontSize: 15,
    marginBottom: 12,
  },
  dis: {
    color: "#B2C0CE",
  },
  id: {
    color: "##2C0CE3D",
    // background: "#B2C0CE3D",
    boxShadow: "none",
    fontSize: 10,
    "&:hover": {
      boxShadow: "none",
      background: "#B2C0CE3D",
    },
  },
  chip: {
    background: "#B2C0CE",
    width: 100,
    fontSize: 10,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    padding: 2,
    color: "#22252C",
    paddingLeft: 6,
  },
  description: {
    '& .ql-editor': {
      padding: '0px 0px 16px 0px',
      color: '#fff',
      fontSize: "14px",
      fontFamily: "crayond_bold",
    }
  }

}));

export const CardComponent = (props) => {
  const classes = useStyles();
  const { item } = props;
  const modules = {
    toolbar: false,
  };

  return (
    <div onClick={() => props?.goProperty(item?._id)} style={{ cursor: "pointer" }}>
      <div className={classes.chip}>
        {item?.property_type}
      </div>
      <Card className={classes.root}>
        <CardContent>
          <Typography
            gutterBottom
            variant="h5"
            component="div"
            color="text.primary"
            className={classes.title}
          >
            {item?.name}
          </Typography>
          <ReactQuill
            readOnly
            theme="bubble"
            value={item?.description}
            modules={modules}
            className={classes.description}
          />
        </CardContent>
        <CardActions
          style={{ display: "flex", justifyContent: "space-between" }}
        >
          <Rating name="read-only" value={5} readOnly />
          <Button size="small" variant="contained"
            className={classes.id}
          >
            <img src="/images/icons8-comments.svg" width={"14px"} />
            &nbsp; 0 Suggestions
          </Button>
        </CardActions>
      </Card>
    </div>
  );
};
