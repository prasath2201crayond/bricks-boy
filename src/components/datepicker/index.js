import { Box, Divider, Popover, Stack, Typography } from "@mui/material";
import makeStyles from "@mui/styles/makeStyles";
import moment from "moment";
import React from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
const styles = makeStyles((theme) => ({
      placeholder: {
            fontSize: 14,
            whiteSpace: "nowrap",
            color: "#fff"
      },
      // div: {
      //     min: "300px"
      // },
      title: {
            fontSize: 12,
            color: "#4E5A6B",
            marginBottom: "8px",
      },
      type: {
            fontSize: 12,
            color: "#4E5A6B",
            backgroundColor: "#F2F4F7",
            borderRadius: "4px",
            padding: "4px 8px",
            textAlign: "center",
            marginRight: "4px",
            marginLeft: "4px",
            cursor: "pointer"

      },
      textField: {
            width: "70px",
            padding: "4px",
            [`& fieldset`]: {
                  borderRadius: "4px",
                  height: (props) => (props.multiline ? "unset" : 32),
                  border: "1px solid #3E434C",
                  "& :hover": {
                        border: "1px solid #3E434C",
                  },
            },
            "& .MuiOutlinedInput-input": {
                  padding: "11px 14px",
            },
      },
      dialog: {
            "& .MuiPopover-paper": {
                  borderRadius: "12px",
                  boxShadow: "0px 8px 24px #0717411F"
            }
      },
      typea: {
            fontSize: 12,

            borderRadius: "4px",
            padding: "4px 8px",
            textAlign: "center",
            marginRight: "4px",
            marginLeft: "4px",
            cursor: "pointer",
            backgroundColor: "#fff",
            color: "#000"
      },
      value: {
            fontSize: 14,
            color: "#fff",
      },
      main: {
            border: '1.5px solid #3E434C',
            borderRadius: '8px'
      },
      Label: {
            fontSize: "12px",
            color:'#0D9E8D'
      },
}));
export const DatePickerNew = ({
      startIcon = "",
      placeholder = "",
      handleChange = () => false,
      value = "",
      label = "",
      isrequired,
      isError = '',
      errorMessage = ""
}) => {
      const classes = styles()
      const [anchorEl, setAnchorEl] = React.useState(null);


      const handleClick = (event) => {
            setAnchorEl(event.currentTarget);
      };

      const handleClose = () => {
            setAnchorEl(null);
      };

      const open = Boolean(anchorEl);
      const id = open ? 'simple-popover' : undefined;
      return (

            <div>
                  <Typography variant="body1" className={classes.Label} gutterBottom noWrap>
                        {label}
                        {isrequired && (
                              <Typography variant="caption" style={{ color: "red", marginLeft: 4 }}>
                                    *
                              </Typography>
                        )}
                  </Typography>
                  <div className={classes?.main}>
                        <Box sx={{ cursor: "pointer", position: 'relative', padding: '10px' }} display="flex" alignItems="center" height="43px" onClick={handleClick}>
                              <Box flexGrow={1} marginLeft="4px">
                                    {
                                          value ?
                                                <>
                                                      <Typography className={classes.value}>
                                                            {moment(value).format("DD MMM YY")}
                                                            </Typography>
                                                </>
                                                : <Typography className={classes.placeholder}>{placeholder}</Typography>
                                    }
                              </Box>
                              <Box marginTop="4px" marginRight="8px">
                                    <img src={'/images/icons8-calendar (5).svg'} alt="" />
                              </Box>

                        </Box>
                        <Popover
                              id={id}
                              open={open}
                              anchorEl={anchorEl}
                              onClose={handleClose}
                              anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'left',
                              }}
                              className={classes.dialog}
                        >
                              <div className={classes.div}>
                                    <Box p={1.4}>


                                          <Stack
                                                direction="row"
                                                divider={<Divider orientation="vertical" flexItem />}
                                                spacing={2}
                                          >
                                                <div>
                                                      <Typography className={classes.title}>{label}</Typography>

                                                      <DatePicker
                                                      //  dateFormat="MM-DD-YYYY"
                                                            selected={value}
                                                            onChange={(value)=>{
                                                                  handleChange(value)
                                                                  setAnchorEl(null)
                                                            }}
                                                            selectsDisabledDaysInRange
                                                            inline
                                                      //showMonthDropdown
                                                      />
                                                </div>


                                          </Stack>



                                    </Box>


                              </div>
                        </Popover>
                  </div>
                  {isError && (
                        <Typography variant={"caption"} color={"error"}>
                              {errorMessage}
                        </Typography>
                  )}
            </div>
      )
}