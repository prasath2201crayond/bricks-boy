import { Box, TextField } from "@mui/material";
import makeStyles from '@mui/styles/makeStyles';
import React from "react";
import SearchIMG from "../../assets/search";
const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },

    search: {
        position: "relative",
        borderRadius: "10px",
        marginLeft: 0,
        width: "100%",
        display: "inline-flex",
        backgroundColor: color => color ? "white" : theme.palette.background.secondary
    },
    searchIcon: {
        padding: theme.spacing(0, 2),
        height: "100%",
        position: "absolute",
        pointerEvents: "none",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
    },
    inputRoot: {
        height: "60px",
        color: "#C5C5C5",
        width: "100%",
        border: "1px solid #E4E8EE"
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
        transition: theme.transitions.create("width"),
        width: "100%",
        border: "1px solid #E4E8EE",
        [theme.breakpoints.up("sm")]: {
            width: "12ch",
            "&:focus": {
                width: "20ch",
            },
        },
    },
    textfield: {
        '& .MuiOutlinedInput-root': {
            borderRadius: "5px",
            height:'40px'
            //border: "1px solid #E4E8EE"
        }
    }

}));

export const SearchFilter = ({
    handleChange = () => false,
    value = "",
    placeholder = "",
    color = false
}) => {
    const classes = useStyles(color);

    return (
        <div className={classes.root}>
            <Box>
                <div className={classes.search}>
                    <TextField
                        autoComplete="off"
                        InputProps={{
                            startAdornment: <SearchIMG color="#a4b2c1" style={{ margin: 8, marginLeft: "3px", color: "#999999" }} />,
                        }}
                        placeholder={placeholder ? placeholder : "Search"}
                        onChange={(e) => handleChange(e.target.value)}
                        value={value}

                        size={"small"}
                        fullWidth
                        className={classes.textfield}
                    />
                </div>
            </Box>
        </div>
    );
};
