import React from "react";
import Stack from "@mui/material/Stack";
import { Typography, IconButton } from "@mui/material";
import Select, { components } from "react-select";
import { AsyncPaginate } from "react-select-async-paginate";
import SerachIMG from "../../assets/search1";
import { TickIcon } from '../../assets/ticicon';
import styled from "@mui/material/styles/styled";
import useTheme from "@mui/material/styles/useTheme";
import CloseIcon from '@mui/icons-material/Close';
import makeStyles from "@mui/styles/makeStyles";
import './multiselect.css'
const Wrapper = styled(Stack)(({ theme }) => ({
      padding: 3,
      paddingLeft: theme.spacing(1),
      backgroundColor: '#22252C',
      borderRadius: 6,
      minWidth: 100,
      color:'#fff'

}));

const Control = ({ children, ...props }) => {
      return (
            <components.Control {...props}>
                  <SerachIMG color={"#98A0AC"} />
                  {children}
            </components.Control>
      );
};
const CustomTypography = styled(Typography, {
      shouldForwardProp: prop => prop !== "color" && prop !== "fontSize" && prop !== "fontFamily" && prop !== "marginBottom" && prop !== "marginTop" && prop !== "marginLeft"
})(({ color = "#0D9E8D", fontSize = 12, marginBottom, marginTop, marginLeft }) => ({
      color,
      fontSize,
      marginBottom,
      marginTop,
      marginLeft
}))
const Option = ({ children, ...props }) => {
      return (
            <components.Option {...props}>
                  <Stack
                        direction={"row"}
                        alignItems={"center"}
                        justifyContent={"space-between"}
                  >
                        {children}
                        {props?.isSelected && <TickIcon color={'#0D9E8D'}/>}
                  </Stack>
            </components.Option>
      );
};
const useStyles = makeStyles((theme) => ({
      Label: {
        fontSize: "12px",
        color: theme.palette.tertiary.main,
      },
      text: {},
      textbox: {
        color: "#fff",
        borderRadius: 6,
        '& .MuiOutlinedInput-input':{
          color: "#fff",
          backgroundColor: '#22252C',
        },
        '& .css-1jraqvo-control':{
            backgroundColor: '#22252C',
            color: "#fff",
            border:'1px solid #3E434C !important'

        }
      },
    }));
export const SelectBox = React.memo((props) => {
      const classes = useStyles(props);

      const {
            key,
            value,
            placeholder,
            loadOptions,
            onChange,
            options,
            loading,
            isReadOnly,
            isMulti,
            debounceTimeout,
            reduceOptions,
            isClearable = true,
            styles = {},
            isPaginate = false,
            label = "",
            isError = false,
            errorMessage = "",
            required = false
      } = props;

      const theme = useTheme();

      const customStyles = (props) => {
            return {
                  control: (base) => ({
                        ...base,
                        border:'1px solid #3E434C !important',
                        borderRadius: 8,
                        paddingLeft: 8,
                        color:'#fff',
                        '&:not(:focus)': {
                              boxShadow: "none",
                              backgroundColor: '#22252C',
                              border:'1px solid #3E434C !important',
                              color:'#fff',
                        },
                        '&:hover': {
                              backgroundColor: '#22252C',
                              border:'1px solid #3E434C !important',
                              color:'#fff',
                        },
                        ...styles?.control ?? {},
                  }),
                  valueContainer: (base) => ({
                        ...base,
                        paddingBlock: 3
                  }),
                  placeholder: defaultStyles => {
                        return {
                              ...defaultStyles,
                              fontSize: 14,
                              whiteSpace: "nowrap",
                              color: "#98A0AC"
                        };
                  },
                  menu: defaultStyles => {
                        return {
                              ...defaultStyles,
                              zIndex: 2,
                              boxShadow: "0px 10px 25px #0000000A",
                              border: "1px solid #E4E8EE",
                              backgroundColor: '#22252C',
                              color:'#fff',
                              borderRadius: 8
                        };
                  },
                  option: (defaultStyles, prop) => {
                        return {
                              ...defaultStyles,
                              fontSize: 14,
                              cursor: "pointer",
                              backgroundColor: '#22252C',
                              color:'#fff',
                              ...isSingleStyle(prop),
                        };
                  },
            }
      };

      const isSingleStyle = (prop) => {
            if (prop.isMulti) return {}
            return {
                  color: prop?.isSelected ? "#fff" : "#fff",
                  '&:hover,&:active': {

                        color: "#0D9E8D"
                  },
            }
      }

      const onClear = (val) => {
            if (props?.onChange) {
                  let updateValue = value?.filter(({ value }) => value !== val.value);
                  props?.onChange(updateValue)
            }
      }


      return (
            <>
                  {label &&
                        <CustomTypography fontFamily={"tenant_bold"} marginBottom={3}>
                              {label}
                              {required && <CustomTypography component={"span"} marginLeft={theme.spacing(1)} color={theme.palette.error.main} fontFamily={"tenant_bold"}>*</CustomTypography>}
                        </CustomTypography>
                  }
                  {
                        isPaginate ? (
                              <AsyncPaginate
                                    className={classes.textbox}
                                    key={key}
                                    isClearable={isClearable}
                                    isSearchable
                                    components={{
                                          IndicatorSeparator: () => null,
                                          Control,
                                          Option,
                                          MultiValueContainer: () => null,
                                    }}
                                    value={value}
                                    placeholder={placeholder}
                                    loadOptions={loadOptions}
                                    onChange={onChange}
                                    options={options}
                                    isLoading={loading}
                                    defaultOptions={options}
                                    styles={customStyles(props)}
                                    isDisabled={isReadOnly}
                                    isMulti={isMulti}
                                    debounceTimeout={debounceTimeout}
                                    reduceOptions={reduceOptions}
                              />
                        ) : (
                              <Select
                                    className={classes.textbox}
                                    //isClearable={isClearable}
                                    isSearchable
                                    components={{
                                          IndicatorSeparator: () => null,
                                          Control,
                                          Option,
                                          MultiValueContainer: () => null,
                                    }}
                                    value={value}
                                    placeholder={placeholder}
                                    options={options}
                                    isLoading={loading}
                                    onChange={onChange}
                                    styles={customStyles(props)}
                                    isMulti={isMulti}
                                    isDisabled={isReadOnly}

                              />
                        )
                  }
                  {isError &&
                        <CustomTypography fontFamily={"tenant_bold"} marginTop={theme.spacing(1)} color={theme?.palette?.error?.main} fontSize={12}>{errorMessage}</CustomTypography>
                  }
                  {
                        (isMulti && value?.length > 0) &&
                        <Stack
                              direction="row"
                              alignItems={"center"}
                              gap={1}
                              flexWrap={"wrap"}
                              marginTop={2}
                        >
                              {
                                    value?.map((_) => (
                                          <Wrapper
                                                direction={"row"}
                                                alignItems={"center"}
                                                key={_?.value}
                                                gap={1}
                                          >
                                                <CustomTypography
                                                      color={"#4E5A6B"}
                                                      fontSize={12}
                                                      fontFamily={"tenant_semiBold"}
                                                      sx={{ flex: 1 }}
                                                >
                                                      {_?.label}{props?.price ?? ""}
                                                </CustomTypography>
                                                <div
                                                      onClick={() => onClear(_)}
                                                      style={{ display: 'flex', cursor: "pointer" }}
                                                >
                                                      <IconButton size="small">
                                                            <CloseIcon htmlColor="#7C8594" fontSize={"12px"} />
                                                      </IconButton>
                                                </div>
                                          </Wrapper>
                                    ))
                              }
                        </Stack>
                  }
            </>

      )
})
