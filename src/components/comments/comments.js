import React from "react";
import { makeStyles } from "@mui/styles";
import { Grid, Typography, Avatar, Button, IconButton } from "@mui/material";
import { Box } from "@mui/system";

const useStyles = makeStyles((theme) => ({
  root: {
    background: theme.palette.primary.dark,
    boxShadow: "none",
    borderBottom: "1px solid #FFFFFF29",
  },
  commentsIdText: {
    color: "#B2C0CE",
    // color:"red"
    fontSize: "14px",
  },
  CommentsText: {
    color: "#FFFFFF",
    fontSize: "14px",
  },
  textField: {
    borderRadius: "50px",
    padding: "10px",
    backgroundColor: "#fff",
  },
  commentBox: {
    position: "fixed",
    bottom: "0",
    backgroundColor: "#2F363E",
    width: "100%",
    borderTop: "1px solid #FFFFFF29",
  },
  input: {
    width: "100%",
    padding: "14px",
    borderRadius: "50px",
    outline: "none",
    border: "none",
  },

  buttonIn: {
    position: "relative",
  },

  iconButton: {
    position: "absolute",
    borderRadius: "5px",
    right: "10px",
    zIndex: "2",
    border: "none",
    top: "7px",
    height: "30px",
    cursor: "pointer",
    color: "white",
    backgroundcolor: "#1e90ff",
    transform: "translateX(2px)",
  },
}));

export const Comments = (props) => {
  const classes = useStyles();

  const CommentsJson = [
    {
      headId: "1PKzzn7Z5RLCs",
      commentsTimeago: "16h",
      commentsText:
        "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.",
      likeIconNumbers: "28",
      dislikeIconNumbers: "42",
      profileImage: "",
    },
    {
      headId: "1PKzzn7Z5RLCs",
      commentsTimeago: "16h",
      commentsText:
        "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.",
      likeIconNumbers: "28",
      dislikeIconNumbers: "42",
      profileImage: "",
    },
    {
      headId: "1PKzzn7Z5RLCs",
      commentsTimeago: "16h",
      commentsText:
        "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.",
      likeIconNumbers: "28",
      dislikeIconNumbers: "42",
      profileImage: "",
    },
  ];

  // const unlikeHandle = ()=>{

  // }

  return (
    <div className={classes.root}>
      {CommentsJson.map((comments) => {
        return (
          <Grid container padding={"20px"}>
            <Grid item lg={2} md={1} sm={2} xs={2}>
              <Avatar>{comments.profileImage}</Avatar>
            </Grid>
            <Grid item lg={10} md={10} sm={10} xs={10} textAlign={"left"}>
              <Typography className={classes.commentsIdText}>
                {comments.headId}&nbsp;•&nbsp;{comments.commentsTimeago}
              </Typography>
              <Typography className={classes.CommentsText}>
                {comments.commentsText}
              </Typography>
              <Box display={"flex"} alignItems={"center"}>
                <Box display={"flex"} alignItems={"center"}>
                  <Box>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="12.502"
                      height="16"
                      viewBox="0 0 12.502 16"
                    >
                      <path
                        id="icons8-thumbs-up_1_"
                        data-name="icons8-thumbs-up (1)"
                        d="M21.989,11.518a2,2,0,0,0-1.515-.669h-4.1a.232.232,0,0,1-.178-.089.284.284,0,0,1,0-.267,10.818,10.818,0,0,0,.758-2.407,4.036,4.036,0,0,0,.178-1.114A1.832,1.832,0,0,0,15.66,5.1c-.089,0-.178,0-.223.089-.045.045-2.852,4.1-3.967,5.214A4.381,4.381,0,0,0,10,13.523v2.942A4.67,4.67,0,0,0,14.68,21.1h4.145a2.927,2.927,0,0,0,2.9-2.54l.758-5.437A1.942,1.942,0,0,0,21.989,11.518Z"
                        transform="translate(-10 -5.1)"
                        fill="#b2c0ce"
                      />
                    </svg>
                  </Box>
                  <Box>
                    <Button>
                      &nbsp;
                      <span className={classes.commentsIdText}>
                        {comments.likeIconNumbers}
                      </span>
                    </Button>
                  </Box>
                </Box>
                <Box display={"flex"} alignItems={"center"}>
                  <Box>
                    <Button>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="12.467"
                        height="16"
                        viewBox="0 0 12.467 16"
                      >
                        <path
                          id="icons8-thumbs-down"
                          d="M22.444,14.956l-.756-5.422A2.887,2.887,0,0,0,18.8,7H14.667A4.666,4.666,0,0,0,10,11.667V14.6a4.369,4.369,0,0,0,1.467,3.111,50.359,50.359,0,0,1,3.956,5.2A.231.231,0,0,0,15.6,23h.044a1.827,1.827,0,0,0,1.467-1.867,4.278,4.278,0,0,0-.178-1.111,10.788,10.788,0,0,0-.756-2.4.283.283,0,0,1,0-.267.231.231,0,0,1,.178-.089h4.089a2.159,2.159,0,0,0,1.511-.667A2.078,2.078,0,0,0,22.444,14.956Z"
                          transform="translate(-10 -7)"
                          fill="#b2c0ce"
                        />
                      </svg>
                      &nbsp;
                    </Button>
                  </Box>
                  <Box>
                    <span className={classes.commentsIdText}>
                      {comments.dislikeIconNumbers}
                    </span>
                  </Box>
                </Box>
              </Box>
            </Grid>
          </Grid>
        );
      })}
      <div className={classes.commentBox}>
        <Grid container padding={"10px"}>
          <Grid item lg={2} md={2} sm={2} xs={2}>
            <Avatar></Avatar>
          </Grid>
          <Grid className={classes.buttonIn} lg={10} md={10} sm={10} xs={10}>
            {/* <TextField backgroundColor="white" fullWidth placeholder="Leave your suggestion"/> */}
            <input
              className={classes.input}
              type={"text"}
              placeholder="Leave your suggestion"
            />

            <IconButton className={classes.iconButton}>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="20"
                height="20"
                viewBox="0 0 20 20"
              >
                <path
                  id="icons8-sent"
                  d="M4.723,4a.75.75,0,0,0-.672,1.02l2.75,7.1a.75.75,0,0,0,.579.469L14.852,13.8c.186.03.168.084.168.2s.018.167-.168.2L7.381,15.411a.75.75,0,0,0-.579.469l-2.75,7.1a.75.75,0,0,0,1.034.941l18.5-9.25a.75.75,0,0,0,0-1.342L5.085,4.08A.75.75,0,0,0,4.723,4Z"
                  transform="translate(-4.001 -4.001)"
                  fill="#b2c0ce"
                />
              </svg>
            </IconButton>
          </Grid>
        </Grid>
      </div>
    </div>
  );
};
