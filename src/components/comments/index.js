import React from "react";
import { makeStyles } from "@mui/styles";
import { Grid, Typography, Avatar } from "@mui/material";
import { Box } from "@mui/system";
import moment from "moment-timezone";
import ThumbUpIcon from '@mui/icons-material/ThumbUp';
import ThumbDownIcon from '@mui/icons-material/ThumbDown';
import ThumbDownOutlinedIcon from '@mui/icons-material/ThumbDownOutlined';
import ThumbUpOutlinedIcon from '@mui/icons-material/ThumbUpOutlined';

const useStyles = makeStyles((theme) => ({
  root: {
    background: theme.palette.primary.main,
    boxShadow: "none",
    borderBottom: "1px solid #FFFFFF29",
  },
  commentsIdText: {
    color: "#B2C0CE",
    fontSize: "14px",
  },
  CommentsText: {
    color: "#FFFFFF",
    fontSize: "14px",
  },
  textField: {
    borderRadius: "50px",
    padding: "10px",
    backgroundColor: "#fff",
  },
  commentsId: {
    color: "#0D9E8D",
    fontSize: "14px",
  }
}));

export const CommentsSections = (props) => {
  const classes = useStyles();
  const { comments } = props;
  return (
    <div className={classes.root}>
      <Grid container padding={"20px"}>
        <Grid item>
          <Avatar style={{ marginRight: 8 }} src={comments.profileimage}/>
        </Grid>
        <Grid item lg={10} md={10} sm={10} xs={10} textAlign={"left"}>
          <Typography className={classes.commentsIdText}>
            {comments.createdBy}&nbsp;•&nbsp;{moment(new Date(comments.createdTime)).fromNow()}
          </Typography>
          <Typography className={classes.CommentsText}>
            {comments.command}
          </Typography>
          <Box display={"flex"} alignItems={"center"}>
            <Box display={"flex"} alignItems={"center"}>
              <Box>
                {
                  props?.reactions?.like === true ? <ThumbUpIcon style={{ color: '#0D9E8D', cursor: 'pointer' }} onClick={() => props?.addReactions('like', false)} /> : <ThumbUpOutlinedIcon style={{ color: '#0D9E8D', cursor: 'pointer' }} onClick={() => props?.addReactions('like', false)} />
                }

              </Box>
              <Box>
                &nbsp;
                <span className={classes.commentsId}>
                  0
                </span>
              </Box>
            </Box>
            &nbsp;&nbsp;
            <Box display={"flex"} alignItems={"center"}>
              <Box style={{ marginTop: 4 }}>
                {
                  props?.reactions?.like === true ? <ThumbDownIcon style={{ color: '#0D9E8D', cursor: 'pointer' }} onClick={() => props?.addReactions('like', false)} /> : <ThumbDownOutlinedIcon style={{ color: '#0D9E8D', cursor: 'pointer' }} onClick={() => props?.addReactions('like', false)} />
                }
                &nbsp;
              </Box>
              <Box>
                <span className={classes.commentsId}>
                  0
                </span>
              </Box>
            </Box>
          </Box>
        </Grid>
      </Grid>
    </div>
  );
};
