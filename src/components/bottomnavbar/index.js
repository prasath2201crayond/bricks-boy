import makeStyles from "@mui/styles/makeStyles";
import * as React from "react";
import { Menu, Divider, MenuItem, Grid,Container } from "@mui/material";
import HomeOutlinedIcon from '@mui/icons-material/HomeOutlined';
import ExploreOutlinedIcon from '@mui/icons-material/ExploreOutlined';
import FavoriteBorderOutlinedIcon from '@mui/icons-material/FavoriteBorderOutlined';
import AccountCircleOutlinedIcon from '@mui/icons-material/AccountCircleOutlined';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import { Routes } from '../../router/routes';
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
      root: {
            color: theme.palette.primary.main,
      },
      containers: {
            "&.MuiContainer-root": {
                  paddingLeft: "0px",
                  paddingRight: "0px",
            },
            height: "62px",
            borderRadius: '40px',
            boxShadow: "0px -1px 6px #00000021",
            // position: 'fixed',
            background: '#fff',
            // bottom: '7px',
            // left: '0',
            // right: '0'
      },
      tabStyle: {
            cursor: "pointer",
      },
      selectedLabelTextStyle: {
            fontSize: "26px",
            color: theme.palette.primary.main,
      },
      unselectedLabelTextStyle: {
            fontSize: "26px",
            color: '#000',
      },
      menu: {
            "& .MuiPopover-paper":
            {
                  boxShadow: "0 0px 8px -4px #f2f3f5",
                  border: "1px solid #f2f3f5",
                  borderRadius: "12px",
            },
      },
}));
export const BottomNavbar = (props) => {
      const history = useHistory();
      const classes = useStyles();


      const [anchorEl, setAnchorEl] = React.useState(null);
      const open = Boolean(anchorEl);
      const handleClick = (event, key) => {
            setAnchorEl(event.currentTarget);
      };


      const handleClose = () => {
            setAnchorEl(null);
      };

      return (
            <Container maxWidth="sm" sx={{ padding: 0 }}>
            <Grid
                  container
                  justifyContent={"space-around"}
                  alignItems={"center"}
                  className={classes.containers}
            >
                  <Grid
                        item
                        xs={2}
                        className={classes.tabStyle}
                        onClick={props.onTab1Clicked}
                  >
                        <center>
                              <HomeOutlinedIcon className={window.location.pathname === "/" ? classes.selectedLabelTextStyle : classes.unselectedLabelTextStyle} />
                        </center>
                  </Grid>
                  <Grid
                        item
                        xs={2}
                        className={classes.tabStyle}
                        onClick={props.onTab2Clicked}
                  >
                        <center>
                              <ExploreOutlinedIcon className={window.location.pathname === "/explore" ? classes.selectedLabelTextStyle : classes.unselectedLabelTextStyle} />
                        </center>
                  </Grid>
                  <Grid
                        item
                        xs={2}
                        className={classes.tabStyle}
                        onClick={props.onTab3Clicked}
                  >
                        <center>
                              <AddCircleOutlineIcon className={window.location.pathname === "/service" ? classes.selectedLabelTextStyle : classes.unselectedLabelTextStyle} />
                        </center>
                  </Grid>
                  <Grid
                        item
                        xs={2}
                        className={classes.tabStyle}
                        onClick={props.onTab4Clicked}
                  >
                        <center>
                              <FavoriteBorderOutlinedIcon className={window.location.pathname === "/service" ? classes.selectedLabelTextStyle : classes.unselectedLabelTextStyle} />
                        </center>
                  </Grid>
                  <Grid
                        item
                        xs={2}
                        className={classes.tabStyle}
                        onClick={props.onTab5Clicked}
                  >
                        <center>
                              <AccountCircleOutlinedIcon className={window.location.pathname === "/requestscreen" ? classes.selectedLabelTextStyle : classes.unselectedLabelTextStyle} id="demo-positioned-button"
                                    aria-controls={open ? "demo-positioned-menu" : undefined}
                                    aria-haspopup="true"
                                    aria-expanded={open ? "true" : undefined}
                                    onClick={handleClick} />
                        </center>
                  </Grid>

                  <Menu
                        style={{ boxShadow: "none !important" }}
                        anchorEl={anchorEl}
                        keepMounted
                        open={!!anchorEl}
                        onClose={handleClose}
                        getContentAnchorEl={null}
                        anchorOrigin={{
                              vertical: 'top',
                              horizontal: 'center',
                        }}
                        transformOrigin={{
                              vertical: 'center',
                              horizontal: 'center',
                        }}
                        className={classes.menu}
                  >
                        <MenuItem
                              onClick={
                                    (e) => {
                                          handleClose(e)
                                          history.push(Routes.profile)
                                    }
                              }
                              style={{ color: "#212B36", fontSize: "14px" }}
                        >
                              <img src='/images/profile.svg' alt='user' />
                              &nbsp;&nbsp;Profile
                        </MenuItem>
                        <Divider />
                        <MenuItem
                              onClick={(e) => {
                                    handleClose(e, 'logout')
                                    localStorage.removeItem('auth');
                                    history.push(Routes.welcomepage);
                              }}
                              style={{ color: "#212B36", fontSize: "14px" }}
                        >
                              <img src='/images/logout.svg' alt='logout' />
                              &nbsp;&nbsp;Log out
                        </MenuItem>
                  </Menu>
            </Grid>
            </Container>
      );
};
