import React from "react";
import { Typography, Avatar } from "@mui/material";
import makeStyles from "@mui/styles/makeStyles";
import moment from "moment-timezone";
const useStyles = makeStyles((theme) => ({
  root: {
    borderRadius: "12px",
    border: "1px solid #FFFFFF0A",
    // border:"3px solid red",
    backgroundColor: theme.palette.dispense.primary,
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    padding: "12px",
  },
  avatar: {
    border: "1px solid #D3AE7A",
    padding: "13px",
    width: "49px",
    height: "49px",
    backgroundColor:'#2F363E'
  },
  title: {
    color: theme.palette.secondary.main,
    fontSize: "16px",
    fontFamily: "crayond_bold",
  },
  des: {
    color: "#B2C0CE",
    fontFamily: "crayond_regular",
    textAlign: "right",
    fontSize: 13,
    marginTop: 2,
  },
}));
export const LoanAmountCard = (props) => {
  // classes
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <div>
        {
          props?.data?.type === 'icon' ? <Avatar className={classes.avatar} >{props?.data?.img}</Avatar> : <Avatar src={props?.data?.img} alt="" className={classes.avatar} />
        }

      </div>
      <div style={{ textAlign: "right" }}>
        {
          props?.data?.dateFormat ? <Typography className={classes.title}>
            {
              moment(props?.data?.title)
                .tz(moment.tz.guess())
                .format("DD MMM yyyy")
            }
          </Typography>
            :
            <Typography className={classes.title}>
              {props?.data?.title}
              {props?.title}
            </Typography>
        }


        <Typography className={classes.des}>
          {props?.data?.des}
          {props?.des}
        </Typography>

      </div>
    </div>
  );
};
