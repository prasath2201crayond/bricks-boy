import React from 'react';
import {  Avatar} from "@mui/material";
// import makeStyles from "@mui/styles/makeStyles";
import CameraAltOutlinedIcon from '@mui/icons-material/CameraAltOutlined';
import { BackdropContext } from "../../contexts";
import { getDownloadURL, ref, storage, uploadBytesResumable } from "../../firebase";


// const useStyles = makeStyles((theme) => ({
//       profile: {
//             position: 'absolute',
//             bottom: '-48px',
//             border: '2px solid #fff',
//             borderRadius: '50%'
//       },
// }));


export const CreateProfileImg = (props) => {
      const backdrop = React.useContext(BackdropContext);
      const handleImg = async (file) => {
            if (!file) return
            const storageRef = ref(storage, `files/${file?.name}`)
            const uploadFile = uploadBytesResumable(storageRef, file)
            await uploadFile.on("state_changed", (snapshot) => {
                  const value = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100)
                  backdrop.setBackDrop({
                        ...backdrop,
                        open: true,
                        message: `Uploading... ${value}%`,
                  });
            }, (err) => {
                  console.log(err)
            }, () => {
                  getDownloadURL(uploadFile.snapshot.ref).then((url) => {
                        props?.onChange(props?.type, url)
                        backdrop.setBackDrop({
                              ...backdrop,
                              open: false,
                              message: "",
                        });
                  })
            })
      }
      console.log(props?.type)
      return (
            <label>
                  {
                        props?.isAvatar && <Avatar
                        sx={{ width: 100, height: 100 }}
                        />
                  }

                  <input type='file' style={{ display: 'none' }} accept="image/*" onChange={(e) => handleImg(e?.target?.files?.[0])} />
                  <CameraAltOutlinedIcon className={props?.classposition} />
            </label>

      )
}