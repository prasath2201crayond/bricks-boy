import React from "react";
import { makeStyles } from "@mui/styles";
import { TopNavBar, SideNavBar } from "../components";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    height: "100%",
  },
  content: {
    background: theme.palette.primary.dark,
    width: "100%",
    height: "calc(100% - 64px)",
    overflow: "auto",
    [theme.breakpoints.up("sm")]: {
      marginTop: 64,
    },
    [theme.breakpoints.down("sm")]: {
      paddingBottom:'135px'
    },
  },
  topNavbar: {
    [theme.breakpoints.down("sm")]: {
      display: "none",
    },
  },
  sideNavbar: {
    [theme.breakpoints.up("sm")]: {
      display: "none",
    },
  },
}));

const withNavBars = (Component) => (props) => {
  const classes = useStyles({ props });

  return (
    <div className={classes.root}>
      {/* Header */}
      <div className={classes.topNavbar}>
        <TopNavBar />
      </div>

      {/* Content */}
      <div className={classes.content}>
        <Component {...props}>{props.children}</Component>
      </div>

      {/* Footer */}
      {
        <div className={classes.sideNavbar}>
          <SideNavBar />
        </div>
      }
    </div>
  );
};

export default withNavBars;
