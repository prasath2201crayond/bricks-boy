import { Container, Grid,Box } from "@mui/material";
import makeStyles from "@mui/styles/makeStyles";
import React from "react";
import { TopNavBar } from "../components";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    height: "100%",
    overflow: "hidden",
  },

  screenPadding: {
    overflow: "overlay",
    scrollbarWidth: "none",
    "&::-webkit-scrollbar": {
      display: "none",
    },
  },
}));

const withBottombar = (Component) => (props) => {
  const classes = useStyles({ props });
    return (
    <div className={classes.root}>
      {/* Your nav bars here */}
      <Container maxWidth="sm" sx={{ padding: 0 }}>
        <Grid
          className={classes.screen}
          item
          xs={12}
        >
          <Box>
          <TopNavBar />
          </Box>
          <Grid
            className={classes.screenPadding}
          >
            <Component {...props}>{props.children}</Component>
          </Grid>
        </Grid>
      </Container>

      {/* Content */}
    </div>
  );
};

export default withBottombar;
