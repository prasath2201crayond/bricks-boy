import React from "react";
import { TopNavBar } from "../../components/navbars/topNavbar/topNavbar";
import { sumbirStyles } from "./style";
import {
      Typography,
      Container,
      Button,
      Box,
      Grid,
      Avatar,
      Hidden,
      IconButton,
      Alert,
      Stack,

} from "@mui/material";
import DeckOutlinedIcon from '@mui/icons-material/DeckOutlined';
import ApartmentOutlinedIcon from '@mui/icons-material/ApartmentOutlined';
import TtyOutlinedIcon from '@mui/icons-material/TtyOutlined';
import SpeedOutlinedIcon from '@mui/icons-material/SpeedOutlined';
import AlternateEmailIcon from '@mui/icons-material/AlternateEmail';
import DashboardOutlinedIcon from '@mui/icons-material/DashboardOutlined';

import {
      DrawerComponent,
      AlertDialog,
      CommentsSections,
      TimelineComponent,
} from "../../components";
import useWindowDimensions from "../../utils/useDimension";
import { LoanAmountCard } from "../../components";
import { useHistory, useLocation } from "react-router-dom";
import axios from "axios";
import ReactQuill from "react-quill";
import MyGoogleMap from '../../components/interactivemap/myGoogleMaps';
import {config}from '../../config'
import jwt_decode from "jwt-decode";

export const PropertyDeatils = () => {
      const state = useLocation();
      const modules = {
            toolbar: false,
      };
      // classes
      const [open, setOpen] = React.useState(false);
      const history = useHistory();
      const classes = sumbirStyles();
      const [details, setDetails] = React.useState(null);
      const [reactions, setReactions] = React.useState({
            like:false,
            dislike:false
      })
      const closeDrawer = () => {
            setOpen(!open);
      };
      const getAllData = () => {
            axios.get(`${config.api_url}get_propertyBy_id?id=${state?.state?.main?.id}`).then((res) => {
                  setDetails(res?.data ?? {})
            }).catch((err) => {
                  <Alert severity="error">SomeThing Went Wrong</Alert>
            });
      }
      React.useEffect(() => {
            if (state?.state?.main?.id?.length > 0) {
                  getAllData()
            }
            // eslint-disable-next-line
      }, [state?.id])

      const Card1 = [
            {
                  img: "/images/cashbag.svg",
                  title: details?.period,
                  des: "Payment period",
            },
            {
                  img: <DeckOutlinedIcon sx={{ color: '#D3AE7A' }} />,
                  title: details?.purpose,
                  des: "Property Purpose",
                  type: 'icon'
            },
            {
                  img: <ApartmentOutlinedIcon sx={{ color: '#D3AE7A' }} />,
                  title: details?.property_type,
                  des: "Property Type",
                  type: 'icon'
            },
            {
                  img: '/images/doller.svg',
                  title: details?.revenue_type,
                  des: "Revenue Type",
            },
            {
                  img: <SpeedOutlinedIcon sx={{ color: '#D3AE7A' }} />,
                  title: details?.measure_unit,
                  des: "Measurement Unit",
                  type: 'icon'
            },
            {
                  img: "/images/refund.svg",
                  title: `${details?.capture_area} ${details?.measure_unit}`,
                  des: "Capture Area",
            },
            {
                  img: "/images/refund.svg",
                  title: `${details?.total_area} ${details?.measure_unit}`,
                  des: "Total Area",
            },
            {
                  img: "/images/calander.svg",
                  title: details?.year_built,
                  des: "Year Build",
                  dateFormat: true
            },
            {
                  img: "/images/calander.svg",
                  title: details?.hand_over,
                  des: "Hand Over Date",
                  dateFormat: true
            },
      ];
      const Card2 = [
            {
                  img: <TtyOutlinedIcon sx={{ color: '#D3AE7A' }} />,
                  title: `${details?.mobile_number_country_code}-${details?.mobile_number}`,
                  des: "Mobile Number",
                  type: 'icon'
            },
            {
                  img: <AlternateEmailIcon sx={{ color: '#D3AE7A' }} />,
                  title: details?.email,
                  des: "Email",
                  type: 'icon'
            },
            {
                  img: <DashboardOutlinedIcon sx={{ color: '#D3AE7A' }} />,
                  title: details?.website,
                  des: "Website",
                  type: 'icon'
            },
      ];
      const GoogleMap = React.useCallback(() => {
            // google map
            return (
                  <MyGoogleMap
                        lat={details?.latitude}
                        lng={details?.longitude}
                        center={{
                              lat: details?.latitude,
                              lng: details?.longitude
                        }}
                        zoom={13}
                        readonly

                  />
            )
      }, [details])
      const editFunction = () => {
            history.push({
                  pathname: '/createpost',
                  state: {

                        details: state?.state?.main?.id ?? "",
                        isEdit: true

                  },
            });
      }
      const addReactions =(k,v)=>{
            setReactions({...reactions , [k]:v})
      }
      return (
            <div>
                  <TopNavBar isBack Url={"/"} />
                  <div className={classes.body}>
                        <Container maxWidth="lg">
                              {/* <Typography className={classes.proposal}>Timeline</Typography> */}
                              {state?.state?.state?.isTimeline && <TimelineComponent />}
                              {/* top contant */}
                              <Box style={{ display: 'flex', alignItems: 'center' }}>
                                    <Box>
                                          <Avatar src={details?.property_image} className={classes.avatar} />
                                    </Box>
                                    <Box style={{ marginLeft: '20px' }}>
                                          <Box>
                                                <Typography className={classes.title}>
                                                      {details?.name}
                                                </Typography>
                                          </Box>
                                          <Box style={{ marginTop: "12px" }}>
                                                <ReactQuill
                                                      readOnly
                                                      theme="bubble"
                                                      value={details?.description}
                                                      modules={modules}
                                                      className={classes.description}
                                                />
                                          </Box>
                                    </Box>

                              </Box>

                              <Box
                                    style={{
                                          display: "flex",
                                          alignItems: "center",
                                          textAlign: "left",
                                          marginTop: "12px",
                                          paddingBottom: "15px",
                                          borderBottom: "1px dashed #B2C0CE3D",
                                          marginLeft: '10px'
                                    }}
                              >
                                    <Box>
                                          <Button variant="contained" className={classes.btn1}>
                                                PID12345
                                          </Button>
                                    </Box>
                              </Box>

                              {/* Loan Amount */}
                              <Typography className={classes.title1} style={{ margin: "16px 0px" }}>
                                    Property Details
                              </Typography>
                              <Grid container spacing={5}>
                                    {Card1?.map((x) => {
                                          return (
                                                <Grid item xs={12} sm={12} md={4}>
                                                      <LoanAmountCard data={x} />
                                                </Grid>
                                          );
                                    })}
                              </Grid>

                              {/* loan repayment structure */}
                              <Box
                                    style={{
                                          margin: "16px 0px",
                                          borderTop: "1px dashed #B2C0CE3D",
                                          padding: '14px 0px',
                                    }}
                              >
                                    <Typography className={classes.title1}>
                                          Contact Details
                                    </Typography>
                                    <Grid container spacing={2}>
                                          {Card2?.map((x) => {
                                                return (
                                                      <Grid
                                                            item
                                                            xs={12}
                                                            sm={12}
                                                            md={4}
                                                            lg={4}
                                                            className={classes.repayment}
                                                      >
                                                            <LoanAmountCard data={x} />
                                                      </Grid>
                                                );
                                          })}
                                    </Grid>
                                    <Box className={classes.addressCard} style={{ marginTop: '24px' }}>
                                          <Grid container spacing={3}>
                                                <Grid item xs={12} sm={12} md={12} lg={5}>
                                                      <GoogleMap />
                                                </Grid>
                                                <Grid item xs={12} sm={12} md={12} lg={7}>
                                                      <Stack direction="row" >
                                                            <img src='/images/loc.svg' alt='' />
                                                            <Typography className={classes.title1}>&nbsp;ADDRESS</Typography>
                                                      </Stack>
                                                      <Box height="12px" />
                                                      <Typography className={classes.desAddress}>
                                                            {details?.door_no + "," ?? ""}
                                                            {details?.address_line1 + "," ?? ""}
                                                            {details?.address_line2 + "," ?? ""}
                                                            {details?.landmark + "," ?? ""}
                                                            {details?.district + "," ?? ""}
                                                            {details?.state + "," ?? ""}
                                                            {details?.country + "," ?? ""}
                                                            {details?.pincode ?? ""}.
                                                      </Typography>
                                                      <Box height="16px" />
                                                      <Stack direction="row" >
                                                            <Typography className={classes.title1}>Latitude : </Typography>
                                                            <Typography className={classes.desAddress} >&nbsp;{details?.latitude}</Typography>
                                                      </Stack>
                                                      <Box height="12px" />
                                                      <Stack direction="row" >
                                                            <Typography className={classes.title1}>Longitude : </Typography>
                                                            <Typography className={classes.desAddress} >&nbsp;{details?.longitude}</Typography>
                                                      </Stack>
                                                </Grid>
                                          </Grid>
                                    </Box>
                              </Box>
                              {
                                    details?.assests?.length > 0 &&
                                    <Box
                                          style={{
                                                padding: "16px 0px",
                                                borderTop: "1px dashed #B2C0CE3D",
                                                borderBottom: "1px dashed #B2C0CE3D",
                                                paddingBottom: 14,
                                          }}
                                    >
                                          <Typography className={classes.title1}>
                                                Assets
                                          </Typography>
                                          <Box className={classes.addressCard}>
                                                {
                                                      details?.assests?.map((x) => {
                                                            return (
                                                                  <div style={{ padding: '12px' }}>
                                                                        <img src={x} alt='' className={classes.assests} />
                                                                  </div>


                                                            )
                                                      })
                                                }
                                          </Box>
                                    </Box>
                              }

                              <Box className={classes.messagebox}>
                                    <img
                                          src="/images/messages.svg"
                                          alt="message"
                                          style={{ marginRight: 6, width: 18 }}
                                    />
                                    <Typography className={classes.suggestions} onClick={closeDrawer}>
                                          View all 40 suggestions
                                    </Typography>
                              </Box>
                        </Container>
                  </div>
                  {/* btn */}
                  {state?.state?.state?.isCastvote && !state?.state?.state?.isTimeline && (
                        <Box className={classes.bottomBtn}>
                              <Box>
                                    <Button
                                          variant="contained"
                                          className={classes.cancel}
                                          style={{ background: "#D34C54" }}
                                    //   onClick={() => handleGoBack()}
                                    >
                                          I DISAPPROVE
                                    </Button>
                              </Box>
                              <Box>
                                    <Button
                                          variant="contained"
                                          className={classes.sumbit}
                                          //   onClick={() => handleEndorse()}
                                          style={{ background: "#43B45E" }}
                                    >
                                          I ENDORSE
                                    </Button>
                              </Box>
                        </Box>
                  )}







                  {/* success dialog ends */}

                  <Hidden smUp>
                        <DrawerComponent
                              onClose={closeDrawer}
                              open={open}
                              component={
                                    <>
                                          <CommandsComponent closeDrawer={closeDrawer} property_id={state?.state?.main?.id} addReactions={addReactions} reactions={reactions}/>
                                    </>
                              }
                        />
                  </Hidden>

                  {/* dialog */}
                  <Hidden smDown>
                        <AlertDialog
                              isNormal
                              onClose={closeDrawer}
                              open={open}
                              isnotTitle
                              component={
                                    <>
                                          <CommandsComponent closeDrawer={closeDrawer} property_id={state?.state?.main?.id} addReactions={addReactions} reactions={reactions}/>
                                    </>
                              }
                        />
                  </Hidden>
                  {
                        state?.state?.main?.isEdit &&
                        <Box className={classes.bottomBtn}>
                              <Box>
                                    <Button
                                          variant="contained"
                                          className={classes.cancel}
                                    >
                                          Cancel
                                    </Button>
                              </Box>
                              <Box>
                                    <Button
                                          variant="contained"
                                          className={classes.sumbit}
                                          onClick={() => editFunction()}
                                    >
                                          Edit
                                    </Button>
                              </Box>
                        </Box>
                  }
            </div>



      );
};

const CommandsComponent = (props) => {

      const classes = sumbirStyles();

      const size = useWindowDimensions();

      const [data, setData] = React.useState()
      const [input, setInput] = React.useState("");


      const authToken = localStorage.getItem("auth");
      const tempDecoded = jwt_decode(authToken);

      const onSave = () => {
            if (input?.length > 0) {
                  const datas = {
                        command: input,
                        createdTime: new Date(),
                        createdBy: tempDecoded?.name,
                        property_id: props?.property_id,
                        profileimage:tempDecoded?.profileimage
                  }
                  axios.post(`${config.api_url}command/create`, datas).then((res) => {
                        setInput("")
                        getCommand()
                  }).catch((err) => {
                        <Alert severity="error">SomeThing Went Wrong</Alert>
                  });
            }

      };

      const handleChange = (v) => {
            setInput(v);
      };
      const getCommand = () => {
            axios.get(`${config.api_url}get/command?id=${props?.property_id}`).then((res) => {
                  setData(res?.data)
            }).catch((err) => {
                  <Alert severity="error">SomeThing Went Wrong</Alert>
            });
      }
      React.useEffect(() => {
            getCommand()
            // eslint-disable-next-line
      }, [])
      console.log(data)
      return (
            <div className={classes.drawermain}>
                  <Box className={classes.headertitle} style={{ padding: 26 }}>
                        <Typography className={classes.suggestiontitlte}>
                              Suggestions ({data?.length})
                        </Typography>
                        <Typography
                              className={classes.close}
                              onClick={() => props?.closeDrawer()}
                        >
                              Close
                        </Typography>
                  </Box>
                  <div
                        style={{
                              height: size?.height - 239,
                              overflow: "auto",
                        }}
                  >
                        {data?.map((x) => {
                              return <CommentsSections comments={x} addReactions={props?.addReactions}/>;
                        })}
                  </div>
                  <div className={classes.commentBox}>
                        <Grid container padding={"10px"}>
                              <Grid item lg={1.5} md={2.5} sm={2.5} xs={2.5}>
                                    <Avatar
                                          style={{ marginRight: 8 }}
                                          src={tempDecoded?.profileimage}
                                    />
                              </Grid>
                              <Grid className={classes.buttonIn} lg={10.5} md={9.5} sm={9.5} xs={9.5}>
                                    <input
                                          className={classes.input}
                                          type={"text"}
                                          onChange={(e) => handleChange(e.target.value)}
                                          value={input}
                                          style={{ background: "#22252C" }}
                                          placeholder="Leave your suggestion"
                                    />

                                    <IconButton className={classes.iconButton} onClick={() => onSave()}>
                                          <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                width="20"
                                                height="20"
                                                viewBox="0 0 20 20"
                                          >
                                                <path
                                                      id="icons8-sent"
                                                      d="M4.723,4a.75.75,0,0,0-.672,1.02l2.75,7.1a.75.75,0,0,0,.579.469L14.852,13.8c.186.03.168.084.168.2s.018.167-.168.2L7.381,15.411a.75.75,0,0,0-.579.469l-2.75,7.1a.75.75,0,0,0,1.034.941l18.5-9.25a.75.75,0,0,0,0-1.342L5.085,4.08A.75.75,0,0,0,4.723,4Z"
                                                      transform="translate(-4.001 -4.001)"
                                                      fill="#b2c0ce"
                                                />
                                          </svg>
                                    </IconButton>
                              </Grid>
                        </Grid>
                  </div>
            </div>
      );
};
