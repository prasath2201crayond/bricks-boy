import makeStyles from "@mui/styles/makeStyles";
export const sumbirStyles = makeStyles((theme) => ({
      body: {
            backgroundColor: theme.palette.primary.dark,
            padding: "90px 0px",
      },
      title: {
            color: "#fff",
            fontSize: "16px",
            textAlign: "left",
            fontFamily: "crayond_medium",
      },
      des: {
            color: "#B2C0CE",
            fontSize: "14px",
            fontFamily: "crayond_regular",
            textAlign: "left",
      },
      btn1: {
            backgroundColor: theme.palette.dispense.primary,
            color: "#fff",
            fontSize: "12px",
            borderRadius: "6px",
            "&:hover": {
                  backgroundColor: theme.palette.dispense.primary,
                  color: "#fff",
            },
      },
      btn2: {
            backgroundColor: theme.palette.text.secondary,
            color: theme.palette.primary.dark,
            fontSize: "12px",
            borderRadius: "6px",
            "&:hover": {
                  backgroundColor: theme.palette.text.secondary,
                  color: theme.palette.primary.dark,
            },
      },
      title1: {
            color: theme.palette.secondary.main,
            fontSize: "16px",
            fontFamily: "crayond_bold",
            marginBottom: "16px",
      },
      title2: {
            color: "#FFFFFF",
            fontFamily: "crayond_medium",
            fontSize: "16px",
      },
      des2: {
            color: "#B2C0CE",
            fontFamily: "crayond_regular",
            fontSize: "14px",
            marginTop: "4px",
            marginBottom: "16px",
      },
      sumbittitle: {
            color: "#B2C0CE",
            fontSize: "14px",
            fontFamily: "crayond_regular",
      },
      sumbitaddress: {
            color: "#fff",
            fontSize: "16px",
            fontFamily: "crayond_medium",
      },
      suggestions: {
            color: "#0D9E8D",
            fontFamily: "crayond_medium",
            fontSize: "14px",
            cursor: "pointer",
      },
      messagebox: {
            display: "flex",
            alignItems: "center",
            padding: "15px",
      },
      bottomBtn: {
            backgroundColor: theme.palette.primary.dark,
            position: "fixed",
            bottom: "0px",
            left: 0,
            right: 0,
            width: "100%",
            justifyContent: "center",
            alignItems: "center",
            textAlign: "center",
            display: "flex",
            padding: "17px",
      },
      bottomtwoBtn: {
            backgroundColor: theme.palette.primary.main,
            justifyContent: "center",
            alignItems: "center",
            textAlign: "center",
            display: "flex",
            padding: "17px 17px 0px 17px",
      },
      cancel: {
            height: "46px",
            width: "175px",
            backgroundColor: theme.palette.dispense.primary,
            marginRight: "15px",
            "&:hover": {
                  backgroundColor: theme.palette.dispense.primary,
            },
            fontSize: 13,
            boxShadow: "none",
            borderRadius: 12,
            [theme.breakpoints.down("sm")]: {
                  width: "127px",
                },
      },
      sumbit: {
            height: "46px",
            width: "175px",
            backgroundColor: theme.palette.positive.main,
            "&:hover": {
                  backgroundColor: theme.palette.positive.main,
            },
            fontSize: 13,
            boxShadow: "none",
            borderRadius: 12,
            [theme.breakpoints.down("sm")]: {
                  width: "127px",
                },
      },
      repayment: {
            padding: "16px",
            [theme.breakpoints.down("sm")]: {
                  padding: 0,
            },
      },
      drawermain: {
            backgroundColor: theme.palette.primary.main,
            position: "relative",
      },
      close: {
            color: "#0D9E8D",
            fontSize: "16px",
            fontFamily: "crayond_medium",
            position: "absolute",
            cursor: "pointer",
            [theme.breakpoints.down("sm")]: {
                  left: "19px",
                  top: "25px",
            },
            [theme.breakpoints.up("sm")]: {
                  right: "19px",
                  top: "25px",
            },
      },
      suggestiontitlte: {
            color: "#fff",
            fontSize: "16px",
            fontFamily: "crayond_medium",
      },
      headertitle: {
            borderBottom: "1px solid #FFFFFF3D",
            padding: "24px",
            position: "relative",
            textAlign: "center",
      },
      commentBox: {
            backgroundColor: "#2F363E",
            borderTop: "1px solid #FFFFFF29",
            padding: "8px",
      },
      input: {
            width: "100%",
            padding: "14px",
            borderRadius: "50px",
            outline: "none",
            border: "none",
            fontFamily:'crayond_regular'
      },
      buttonIn: {
            position: "relative",
      },

      timelineText: {
            fontSize: "16px",
            color: "#D3AE7A",
      },
      timelineIcon: {
            backgroundColor: "transparent",
            padding: "10px",
            border: "1px dashed #D3AE7A",
      },
      timelineIconDiv: {
            justifyContent: "center",
            display: "flex",
            padding: "10px 1px",
      },
      topText: {
            color: "#FFFFFF",
            fontSize: "19px",
            marginBottom: 6,
            [theme.breakpoints.down("sm")]: {
                  fontSize: "15px",
            },
      },
      subText: {
            color: "#B2C0CE",
            fontSize: "16px",
            // paddingBottom:"10px"
      },
      dividerLine: {
            // color:"#D3AE7A",
            background: "#D3AE7A",
            height: "16px",
            width: "2px",
            textAlign: "center",
            margin: "auto",
            paddingBottom: "8px",
      },
      memberBtn: {
            backgroundColor: "#0D9E8D",
            width: "100%",
            boxShadow: "none",
            padding: 10,

            "&:hover": {
                  backgroundColor: "#0D9E8D",
            },
            borderRadius: 10,
      },
      continueBtn: {
            backgroundColor: "#0D9E8D",
            width: "50%",
            boxShadow: "none",
            padding: 10,
            margin: "auto",

            "&:hover": {
                  backgroundColor: "#0D9E8D",
            },
            borderRadius: 10,
      },
      successTitle: {
            fontSize: "18px",
            color: "#FFFFFF",
            marginTop: 10,
      },
      successSubTitle: {
            fontSize: "14px",
            color: "#B2C0CE",
            marginTop: -20,
      },
      closeBtn: {
            backgroundColor: "#B2C0CE3D",
            width: "100%",
            boxShadow: "none",
            padding: 10,
            borderRadius: 10,
            "&:hover": {
                  backgroundColor: "#B2C0CE3D",
            },
      },
      successcloseBtn: {
            backgroundColor: "#B2C0CE3D",
            width: "60%",
            boxShadow: "none",
            padding: 10,
            borderRadius: 10,
            margin: "auto",
            "&:hover": {
                  backgroundColor: "#B2C0CE3D",
            },
      },

      iconButton: {
            position: "absolute",
            borderRadius: "5px",
            right: "10px",
            zIndex: "2",
            border: "none",
            top: "7px",
            height: "30px",
            cursor: "pointer",
            color: "white",
            backgroundcolor: "#1e90ff",
            transform: "translateX(2px)",
      },
      proposal: {
            color: "#D3AE7A",
      },
      walletText: {
            fontSize: "18px",
            color: "#FFFFFF"
      },
      leftCard: {
            border: "1px solid #0D9E8D",
            height: "165px",
            borderRadius: "12px",
            position: "relative",
      },
      cardIcon: {
            display: "flex",
            justifyContent: "center",
            textAlign: "center",
            margin: "auto",
            paddingTop: "50px"



      },
      cardText: {
            color: "#B2C0CE",
            fontSize: "16px",
      },
      okIcon: {
            position: "absolute",
            top: "12px",
            right: "12px",
            width: "20px",
            height: "20px"
      },
      rightCard: {
            border: "1px solid #B2C0CE3D",
            height: "165px",
            borderRadius: "12px",

      },
      sell: {
            background: "#D34C54",
            boxShadow: "none",
            padding: "10px 20%",
            width: "100%",
            fontSize: 14,
            "&:hover": {
                  background: "#D34C54",
            },
            borderRadius: 14,
            marginTop: 10,
      },
      found: {
            background: "#0D9E8D",
            width: '100%',
            "&:hover": {
                  background: "#0D9E8D",
                  width: '100%',
            },
      },
      dialog: {
            padding: 30,
            background: theme.palette.primary.dark,
            paddingTop: 0,
      },
      badch: {
            background:
                  "transparent linear-gradient(90deg, #D3AE7A 0%, #937A57 100%) 0% 0% no-repeat padding-box",
            padding: 10,
            borderRadius: 8,
      },
      description: {
            '& .ql-editor': {
                  padding: '0px 0px 16px 0px',
                  color: '#fff',
                  fontSize: "14px",
                  fontFamily: "crayond_bold",
            }
      },
      addressCard: {
            borderRadius: "12px",
            border: "1px solid #FFFFFF0A",
            backgroundColor: theme.palette.dispense.primary,
            display: "flex",
            alignItems: "center",
            padding: "12px",
            overflowX: 'scroll',
            '&::-webkit-scrollbar': {
                  width: 0,
                  height: 0
            }
      },
      desAddress: {
            color: "#fff",
            fontFamily: "crayond_regular",
            textAlign: "left",
            fontSize: 16,
            marginTop: 2,
      },
      assests: {
            width: '300px',
            height: '300px',
            borderRadius: "12px"
      },
      avatar: {
            background: '#98A0AC',
            height: '120px',
            width: '120px'
      },

}));
