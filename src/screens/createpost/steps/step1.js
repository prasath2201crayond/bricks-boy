import React from 'react';
import { Typography, Box, Grid, Avatar, InputAdornment } from "@mui/material";
import DoneOutlinedIcon from "@mui/icons-material/DoneOutlined";
import { Editor, SelectBox, TextBox, DatePickerNew, MobileNumberInputComponent } from "../../../components";
import { useStylesCreation } from "../style";

const peridtype = [
      {
            label: "Daily",
            value: 'Daily',
      },
      {
            label: "Weekly",
            value: 'Weekly',
      },
      {
            label: "Monthly",
            value: 'Monthly',
      },
      {
            label: "Yearly",
            value: 'Yearly',
      },
]

const propertyPurposeList = [
      {
            label: "Commercial",
            value: "Commercial",
      },
      {
            label: "Mixed",
            value: "Mixed",
      },
      {
            label: "Residential",
            value: "Residential",
      },
      {
            label: "Storage",
            value: "Storage",
      },
];

const revenueType = [
      {
            value: "Lease",
            label: "Lease"
      },
      {
            value: "Sale",
            label: "Sale"
      },
      {
            value: "Maintenance",
            label: "Maintenance"
      }
]

const propertyType = [
      {
            value: "Apartment",
            label: "Apartment"
      },
      {
            value: "House",
            label: "House"
      },
      {
            value: "Block",
            label: "Block"
      },
      {
            value: "Floor",
            label: "Floor"
      },
]

export const Step1 = (props) => {
      const classes = useStylesCreation();
      return (
            <div className={classes.root}>
                  <Grid container>
                        <Grid item xs={12} style={{ marginBottom: "24px" }}>
                              <Box className={classes.imagebox}>
                                    {/* image */}
                                    <Typography className={classes.title}>
                                          PROPERTY IMAGE
                                    </Typography>
                                    <Avatar src={props?.data?.img?.length > 0 ? props?.data?.img : "/images/citynew.svg"} style={{ margin: '0 auto' }} className={props?.data?.img?.length > 0 ? classes.avatar : classes.avatar1} />
                                    <div>
                                          <label> <Box className={classes.button}  style={{ marginTop: '10px' }}>    Upload image</Box> <input type='file' style={{ display: 'none' }} accept="image/*"
                                                onClick={(event) => {
                                                      event.target.value = null
                                                }}
                                                onChange={(e) => props?.handleImg(e?.target?.files?.[0])} /> </label>
                                    </div>
                                    {props?.data?.img?.length > 0 ? <Typography className={classes.removeimg} onClick={props?.removeimg}>Remove Image</Typography> : props?.data?.error?.img?.length > 0 ? (
                                          <span style={{ fontSize: "12px", color: "red" }}>
                                                Property Image is required
                                          </span>
                                    ) : <span style={{ fontSize: "12px" }}>
                                          <Box height={"18px"} />
                                    </span>}

                              </Box>
                        </Grid>
                        {/* property details */}
                        <Grid item xs={12}>
                              <Box className={classes.imagebox1}>
                                    <Typography className={classes.title}>
                                          PROPERTY DETAILS
                                    </Typography>
                                    <Grid container spacing={3}>
                                          <Grid item xs={6}>
                                                {/* Property Name */}
                                                <TextBox
                                                      isRequired
                                                      label="Property Name"
                                                      placeholder="Select Property Name"
                                                      value={props?.data?.property ?? ""}
                                                      onChange={(e) => {
                                                            props?.updateState("property", e.target.value);
                                                      }}
                                                      isError={props?.data?.error?.property?.length > 0}
                                                      errorMessage={props?.data?.error?.property} />
                                          </Grid>
                                          <Grid item xs={6}>
                                                {/* Payment Period */}
                                                <SelectBox
                                                      required
                                                      label="Payment Period"
                                                      placeholder="Select Payment Period"
                                                      options={peridtype ?? []}
                                                      value={props?.data?.period ?? ""}
                                                      onChange={(value) => {
                                                            props?.updateState("period", value);
                                                      }}
                                                      isError={props?.data?.error?.period?.length > 0}
                                                      errorMessage={props?.data?.error?.period} />

                                          </Grid>
                                    </Grid>
                                    <Grid container style={{ marginTop: '10px' }}>
                                          {/* Property Description */}
                                          <Grid item xs={12}>
                                                <Editor
                                                      value={props?.data?.des ?? ""}
                                                      label="Property Description"
                                                      handleChange={(e, delta, source, editor) => {
                                                            props?.updateState("des", e);
                                                      }}
                                                      className={classes.editor}
                                                      emoji
                                                />
                                          </Grid>
                                    </Grid>
                              </Box>
                        </Grid>
                  </Grid>

                  <Grid container>
                        <Grid item xs={12}>
                              <Box className={classes.imagebox1} style={{ marginTop: '24px' }}>
                                    <Grid container spacing={3}>
                                          <Grid item xs={12} md={6} lg={6}>
                                                {/* Property Purpose */}
                                                <SelectBox
                                                      required
                                                      label="Property Purpose"
                                                      placeholder="Select Property Purpose"
                                                      options={propertyPurposeList ?? []}
                                                      value={props?.data?.purpose ?? ""}
                                                      onChange={(value) => {
                                                            props?.updateState("purpose", value);
                                                      }}
                                                      isError={props?.data?.error?.purpose?.length > 0}
                                                      errorMessage={props?.data?.error?.purpose} />
                                          </Grid>
                                          <Grid item xs={12} md={6} lg={6}>
                                                {/* Property Purpose */}
                                                <SelectBox
                                                      required
                                                      label="Property Type"
                                                      placeholder="Select Property Type"
                                                      options={propertyType ?? []}
                                                      value={props?.data?.propertyType ?? ""}
                                                      onChange={(value) => {
                                                            props?.updateState("propertyType", value);
                                                      }}
                                                      isError={props?.data?.error?.propertyType?.length > 0}
                                                      errorMessage={props?.data?.error?.propertyType} />
                                          </Grid>
                                          <Grid item xs={12} md={6} lg={6}>
                                                {/* Revenue Type */}
                                                <SelectBox
                                                      required
                                                      label="Revenue Type"
                                                      placeholder="Select Revenue Type"
                                                      options={revenueType ?? []}
                                                      value={props?.data?.revenue ?? ""}
                                                      onChange={(value) => {
                                                            props?.updateState("revenue", value);
                                                      }}
                                                      isError={props?.data?.error?.revenue?.length > 0}
                                                      errorMessage={props?.data?.error?.revenue} />
                                          </Grid>
                                          <Grid item xs={12} md={6} lg={6}>
                                                {/* Measurement Unit */}
                                                <TextBox
                                                      isrequired
                                                      label="Measurement Unit"
                                                      placeholder="Select Measurement Unit"
                                                      // options={measurementUnits ?? []}
                                                      value={props?.data?.unit ?? ""}
                                                      onChange={(value) => {
                                                            props?.updateState("unit", value?.target?.value);
                                                      }}
                                                      isError={props?.data?.error?.unit?.length > 0}
                                                      errorMessage={props?.data?.error?.unit} />
                                          </Grid>
                                          <Grid item xs={12} md={6} lg={6}>
                                                {/* Carpet Area */}
                                                <TextBox
                                                      type='number'
                                                      onKeyPress={(e) => {
                                                            if (e.key === 'e') {
                                                                  e.preventDefault();
                                                            }
                                                      }}
                                                      isrequired
                                                      label="Carpet Area"
                                                      placeholder="Enter Carpet Area"
                                                      value={props?.data?.capture ?? ""}
                                                      onChange={(e) => {
                                                            props?.updateState("capture", e.target.value);
                                                      }}
                                                      endAdornment={<InputAdornment position="end">{props?.data?.unit}</InputAdornment>}
                                                      isError={props?.data?.error?.capture?.length > 0}
                                                      errorMessage={props?.data?.error?.capture} />
                                          </Grid>
                                          <Grid item xs={12} md={6} lg={6}>
                                                {/* Total Area */}
                                                <TextBox
                                                      type='number'
                                                      onKeyPress={(e) => {
                                                            if (e.key === 'e') {
                                                                  e.preventDefault();
                                                            }
                                                      }}
                                                      isrequired
                                                      label="Total Area"
                                                      placeholder="Enter Total Area"
                                                      value={props?.data?.total ?? ""}
                                                      onChange={(e) => {
                                                            props?.updateState("total", e.target.value);
                                                      }}
                                                      endAdornment={<InputAdornment position="end">{props?.data?.unit}</InputAdornment>}
                                                      isError={props?.data?.error?.total?.length > 0}
                                                      errorMessage={props?.data?.error?.total} />
                                          </Grid>
                                          <Grid item xs={12} md={6} lg={6}>
                                                {/* Year Built */}
                                                <DatePickerNew
                                                      placeholder="Year Built"
                                                      handleChange={(value) => {

                                                            props?.updateState("built", value)
                                                      }}
                                                      value={props?.data?.built}
                                                      label={'Year Built'}
                                                      isrequired
                                                      isError={props?.data?.error?.built?.length > 0}
                                                      errorMessage={props?.data?.error?.built}
                                                />
                                          </Grid>
                                          <Grid item xs={12} md={6} lg={6}>
                                                <DatePickerNew
                                                      placeholder="Handover Date"
                                                      handleChange={(value) => props?.updateState("handover", value)}
                                                      value={props?.data?.handover}
                                                      label={'Handover Date'}
                                                      isrequired
                                                      isError={props?.data?.error?.handover?.length > 0}
                                                      errorMessage={props?.data?.error?.handover}
                                                />

                                          </Grid>
                                          <Grid item xs={12} md={6} lg={6}>
                                                <Grid item xs={12}>
                                                      <Typography variant="body1" style={{color:'#0D9E8D'}} className={classes.label} gutterBottom>
                                                            Pets Allowed
                                                      </Typography>
                                                </Grid>
                                                <Grid container height={"45px"} alignItems={"center"}>
                                                      <Avatar style={{color:'#0D9E8D'}}
                                                            className={
                                                                  props?.data?.pet
                                                                        ? classes.completedCheckboxStyle
                                                                        : classes.checkboxStyle
                                                            }
                                                            variant="circular"
                                                            onClick={() => {
                                                                  props?.updateState(
                                                                        "pet",
                                                                        !props?.data?.pet
                                                                  );
                                                            }}
                                                      >
                                                            <DoneOutlinedIcon className={classes.checkboxIconStyle} />
                                                      </Avatar>
                                                </Grid>
                                                {props?.data?.error?.pet?.length > 0 && (
                                                      <Typography variant={"caption"} color={"error"}>
                                                            {props?.data?.error?.pet}
                                                      </Typography>
                                                )}
                                          </Grid>
                                          <Grid item xs={12} md={6} lg={6}>
                                                {/* Revenue Type */}
                                                <MobileNumberInputComponent
                                                      color
                                                      isRequired
                                                      label="Mobile Number"
                                                      placeholder="Enter Mobile Number"
                                                      value={props?.data?.mobile}
                                                      handleChange={(value) => props?.updateState("mobile", value)}
                                                      isError={props?.data?.error?.mobile?.length > 0}
                                                      errorMessage={props?.data?.error?.mobile}
                                                />
                                          </Grid>
                                          <Grid item xs={12} md={6} lg={6}>
                                                {/* Measurement Unit */}
                                                <TextBox
                                                      isrequired
                                                      label="Email"
                                                      placeholder="Enter Email"
                                                      value={props?.data?.email ?? ""}
                                                      onChange={(value) => {
                                                            props?.updateState("email", value?.target?.value);
                                                      }}
                                                      isError={props?.data?.error?.email?.length > 0}
                                                      errorMessage={props?.data?.error?.email} />
                                          </Grid>
                                          <Grid item xs={12} md={6} lg={6}>
                                                {/* Measurement Unit */}
                                                <TextBox
                                                      isrequired
                                                      label="Website"
                                                      placeholder="Enter Website"
                                                      value={props?.data?.website ?? ""}
                                                      onChange={(value) => {
                                                            props?.updateState("website", value?.target?.value);
                                                      }}
                                                      isError={props?.data?.error?.website?.length > 0}
                                                      errorMessage={props?.data?.error?.website} />
                                          </Grid>
                                    </Grid>
                              </Box>

                        </Grid>

                  </Grid>
            </div>
      )
}