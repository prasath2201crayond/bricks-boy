import { Grid, Typography } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import React from 'react';
import { BackdropContext } from "../../../contexts";
import { getDownloadURL, ref, storage, uploadBytesResumable } from "../../../firebase";
const useStyles = makeStyles((theme) => ({
      root: {
            marginTop: "20px",
            border: "1px dashed #B2C0CE3D",
            borderRadius: "10px",
            padding: "34px",
            cursor: "pointer"
      },
      img: {
            width: "100%",
            objectFit: "cover",
            height: "100%",
            borderRadius: "12px",
      },
      imgRoot: {
            border: "1px dashed #B2C0CE3D",
            height: "150px",
            borderRadius: "12px",
            position: "relative",
            marginTop: "12px"
      },
      upload: {
            display: "none"
      },
      iconBtn: {
            border: "1px dashed #B2C0CE3D",
            borderRadius: "10px",
            marginTop: "10px"
      },
      icon: {
            color: "gray",
            fontSize: "58px",
      },
      title: {
            fontSize: "16px",
            color: "#98A0AC",
      },
      imgChild: {
            position: "absolute",
            top: "0px",
            right: "0px",
            cursor: "pointer"
      }
}))
export const UploadBtn = props => {
      const classes = useStyles();
      const backdrop = React.useContext(BackdropContext);
      const handleImg = async (file) => {
            if (!file) return
            const storageRef = ref(storage, `files/${file?.name}`)
            const uploadFile = uploadBytesResumable(storageRef, file)
            await uploadFile.on("state_changed", (snapshot) => {
                  const value = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100)
                  backdrop.setBackDrop({
                        ...backdrop,
                        open: true,
                        message: `Uploading... ${value}%`,
                  });
            }, (err) => {
                  console.log(err)
            }, () => {
                  getDownloadURL(uploadFile.snapshot.ref).then((url) => {
                        props?.setuploadImg([...props?.uploadImg, url])
                        backdrop.setBackDrop({
                              ...backdrop,
                              open: false,
                              message: "",
                        });
                  })
            })
      }
      return <div >
            <>
                  <label htmlFor="uplodebtn" >
                        <div className={classes.root}>
                              <center>
                                    <img src="/images/download.svg" alt="download" />
                                    <Typography className={classes.title}>Upload Here</Typography>
                              </center>
                        </div>
                  </label>
                  <input type="file" accept='image/*' name="img" id="uplodebtn" onChange={(e) => handleImg(e.target.files[0])} className={classes.upload} onClick={(event) => {
                        event.target.value = null
                  }} />
            </>
            {
                  props?.uploadImg?.length > 0 ?
                        <Grid container spacing={1}>
                              {
                                    props?.uploadImg?.map((val, index) => {
                                          return (
                                                <Grid item lg={4}  md={4} xs={6}   >
                                                      <div className={classes.imgRoot} onClick={() => props?.onDelete(index)}>
                                                            <img src={val} alt="" className={classes.img} />
                                                            <div className={classes.imgChild} onClick={props?.delete}>
                                                                  <img src="/images/delete.svg" alt="" width={'25px'} height={'25px'} />
                                                            </div>
                                                      </div>
                                                </Grid>
                                          )
                                    })
                              }
                        </Grid>
                        :
                        ""
            }
      </div>
}