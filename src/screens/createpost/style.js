import makeStyles from "@mui/styles/makeStyles";


export const useStylesCreation = makeStyles((theme) => ({
  imagebox: {
    textAlign: 'center',
    padding: '16px',
  },
  imagebox1: {
    padding: '16px',
  },
  Requirements: {
    padding: "10px",
    textAlign: "center",
    borderRadius: "10px",
    color: "white",
    fontSize: "14px",
    backgroundColor: "#5078E1",
    cursor: "pointer",
  },
  Requirementsqfts: {
    padding: "10px",
    textAlign: "center",
    borderRadius: "10px",
    fontSize: "14px",
    cursor: "pointer",
    border: "1px solid #E4E8EE",
  },
  completedCheckboxStyle: {
    height: "20px",
    width: "20px",
    backgroundColor: "#0D9E8D",
    border: "1px solid #0D9E8D",
  },
  checkboxIconStyle: {
    fontSize: "small",
    color: "#22252C",
  },
  checkboxStyle: {
    height: "20px",
    width: "20px",
    backgroundColor: "#22252C",
    border: "1px solid #0D9E8D",
  },
  avatar: {
    height: '120px',
    width: '120px',
    backgroundColor: theme.palette.primary.main
  },
  title: {
    fontSize: '12px',
    color: '#0D9E8D',
    marginBottom: '12px'
  },
  label: {
    fontSize: "12px",
  },
  tabListAssests: {
    display: 'flex'
  },
  listText: {
    backgroundColor: '#5078E1',
    width: '120px !important',
    color: '#fff',
    borderRadius: '6px',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer'
  },
  listText1: {
    width: '120px !important',
    color: '#98A0AC',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer'
  },
  tabname: {
    fontSize: '12px',
    cursor: 'pointer'
  },
  list: {
    '&.MuiListItem-root': {
      width: '135px !important',
      padding: '0px!important',
      justifyContent: 'center',
      alignItems: 'center',
      textAlign: 'center',
      border: '1px solid #E4E8EE',
      cursor: 'pointer'
    }
  },
  next: {
    marginLeft: "10px",
    color: "#fff",
    backgroundColor: '#0D9E8D',
    "&:hover": {
      backgroundColor: '#0D9E8D',
    },
  },
  Cancel: {
    color: "#0D9E8D",
    fontSize: "14px",
    fontWeight: 600,
    border: '1px solid #0D9E8D',
    "&:hover": {
      border: '1px solid #0D9E8D',
    },
  },
  bottombtn: {
    padding: '11px 16px',
    backgroundColor: '#fff',
    boxShadow: '0px 0px 16px #00000014',
    display: 'flex',
    justifyContent: 'space-between',
    border: '1px solid #E4E8EE',
    position: 'sticky',
    bottom: '0px',
    width: '100%',
    zIndex: 2,
  },
  removeimg: {
    textDecoration: 'underline',
    color: '#fff',
    fontSize: '12px',
    marginTop: '6px',
    cursor: 'pointer'
  },
  avatar1: {
    padding: '35px',
    background: '#0D9E8D',
    height: '120px',
    width: '120px'
  },
  tabtitle: {
    fontSize: '12px',
    color: '#4E5A6B',
  },
  tabtitle1: {
    fontSize: '12px',
    color: '#5078E1',
  },
  subtabtitle: {
    fontSize: '10px',
    color: '#4E5A6B',
  },
  subtabtitle1: {
    fontSize: '10px',
    color: '#5078E1',
  },
  selectBack1: {
    backgroundColor: '#F1F7FF',
    display: 'flex',
    alignItems: 'center',
    padding: '10px 28px',
  },
  selectBack: {
    display: 'flex',
    alignItems: 'center',
    padding: '10px 28px',
  },
  root: {
    background: theme.palette.primary.dark,
    width: "100%",
    height: "100%",
    overflow: "auto",
    padding:'50px 0px 50px',
    [theme.breakpoints.up("sm")]: {
      marginTop: 64,
    },
  },
  button: {
    border: `1px dashed ${theme.palette.tertiary.main}`,
    width: "100%",
    borderRadius: "12px",
    color: theme.palette.tertiary.main,
    height: "55px",
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    "&:hover": {
      border: `1px dashed ${theme.palette.tertiary.main}`,
    },
  },
  grow: {
    flexGrow: 1,
    zIndex: theme.zIndex.drawer + 1
  },
  bottomBtn: {
    backgroundColor: theme.palette.primary.dark,
    position: "fixed",
    bottom: "0px",
    left: 0,
    right: 0,
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    display: "flex",
    padding: "10px 0px",
  },
  bottomtwoBtn: {
    backgroundColor: theme.palette.primary.main,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    display: "flex",
    padding: "17px 17px 0px 17px",
  },
  cancel: {
    height: "46px",
    width: "175px",
    backgroundColor: theme.palette.dispense.primary,
    marginRight: "15px",
    "&:hover": {
      backgroundColor: theme.palette.dispense.primary,
    },
    fontSize: 13,
    boxShadow: "none",
    borderRadius: 12,
    [theme.breakpoints.down("sm")]: {
      width: "127px",
    },
  },
  sumbit: {
    height: "46px",
    width: "175px",
    backgroundColor: theme.palette.positive.main,
    "&:hover": {
      backgroundColor: theme.palette.positive.main,
    },
    fontSize: 13,
    boxShadow: "none",
    borderRadius: 12,
    [theme.breakpoints.down("sm")]: {
      width: "127px",
    },
  },
}));