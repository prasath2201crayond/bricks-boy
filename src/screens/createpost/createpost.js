import React from 'react';
import { Box, Grid, Button, Container, Alert } from "@mui/material";
import { BackdropContext, AlertContext } from "../../contexts";
import { getDownloadURL, ref, storage, uploadBytesResumable } from "../../firebase";
import { useStylesCreation } from "./style";
import { Step1 } from './steps/step1';
import { UploadBtn } from './steps/imageupload';
import axios from "axios";
import { useHistory } from 'react-router-dom';
import { MapWithFields } from '../../components'
import { useLocation } from 'react-router-dom';
import { mapResult } from '../../utils/common';
import { TopNavBar } from '../../components/navbars/topNavbar/topNavbar';
import { AlertProps } from "../../utils";
import {config}from '../../config'

const initial = {
      id: '',
      img: '',
      period: "",
      des: "",
      type: '',
      purpose: '',
      property: "",
      revenue: '',
      unit: '',
      capture: '',
      total: '',
      built: '',
      handover: '',
      pet: '',
      propertyType: "",
      mobile: "",
      email: "",
      website: "",
      doorno: '',
      addressLineOne: '',
      addressLineTwo: '',
      addressLineThree: "",
      district: "",
      state: "",
      country: "",
      pincode: "",
      latitude: "",
      longitude: "",
      error: {
            img: '',
            period: "",
            des: "",
            type: '',
            purpose: '',
            property: "",
            revenue: '',
            unit: '',
            capture: '',
            total: '',
            built: '',
            handover: '',
            pet: '',
            propertyType: "",
            mobile: "",
            email: "",
            website: "",
            doorno: '',
            addressLineOne: '',
            addressLineTwo: '',
            addressLineThree: "",
            district: "",
            state: "",
            country: "",
            pincode: "",
            latitude: "",
            longitude: "",
      }
}


export const CreatePost = (props) => {
      const { state } = useLocation();
      const classes = useStylesCreation();
      const backdrop = React.useContext(BackdropContext);
      const alert = React.useContext(AlertContext);
      const [data, setData] = React.useState({ ...initial })
      const [step, setStep] = React.useState(0)
      const [uploadImg, setuploadImg] = React.useState([])
      const [mapLoad, setMapLoad] = React.useState(!Boolean(state?.main?.isEdit))
      const history = useHistory();


      const updateState = (key, value) => {
            let error = data?.error;
            error[key] = "";
            setData({ ...data, [key]: value, error })
      }

      // image uploading function
      const handleImg = async (file) => {
            if (!file) return
            const storageRef = ref(storage, `files/${file?.name}`)
            const uploadFile = uploadBytesResumable(storageRef, file)
            await uploadFile.on("state_changed", (snapshot) => {
                  const value = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100)
                  backdrop.setBackDrop({
                        ...backdrop,
                        open: true,
                        message: `Uploading... ${value}%`,
                  });
            }, (err) => {
                  console.log(err)
            }, () => {
                  getDownloadURL(uploadFile.snapshot.ref).then((url) => {
                        updateState('img', url)
                        backdrop.setBackDrop({
                              ...backdrop,
                              open: false,
                              message: "",
                        });
                  })
            })
      }
      // remove function
      const removeimg = () => {
            setData({ ...data, img: "" })
      }
      // next
      const next = () => {
            if (step === 0) {
                  if (validate()) {
                        setStep(1)
                  }
            }
            if (step === 1) {
                  setStep(2);
            }
            if (step === 2) {
                  if (validate1()) {
                        setStep(3);
                  }
            }
      };

      // previous
      const previous = () => {
            setStep(prevValue => prevValue - 1);
            if (!state?.main?.isEdit) {
                  setMapLoad(false)
            }
      }
      const onDelete = (i) => {
            uploadImg.splice(i, 1);
      }
      const deleteImg = () => {
            setData({ ...data, img: "" })
      }
      // google map function
      const mapResultData = (map) => {
            console.log(map)
            let result = mapResult(map);
            setData({ ...data, ...result })
      }
      const sumbit = () => {
            if (validate()) {
                  const datas = {
                        name: data?.property ?? "",
                        property_image: data?.img ?? "",
                        period: data?.period?.value ?? "",
                        purpose: data?.purpose?.value ?? "",
                        description: data?.des,
                        property_type: data?.propertyType?.value ?? "",
                        revenue_type: data?.revenue?.value ?? "",
                        measure_unit: data?.unit,
                        capture_area: data?.capture,
                        total_area: data?.total,
                        year_built: data?.built,
                        hand_over: data?.handover,
                        pets_allowed: data?.pet,
                        mobile_number: data?.mobile?.mobile,
                        mobile_number_country_code: data?.mobile?.mobile_code,
                        email: data?.email,
                        website: data?.website,
                        assests: uploadImg ?? [],
                        user_id: localStorage.getItem('UserID'),
                        door_no: data?.doorno,
                        address_line1: data?.addressLineOne,
                        address_line2: data?.addressLineTwo,
                        landmark: data?.addressLineThree,
                        district: data?.district,
                        state: data?.state,
                        country: data?.country,
                        pincode: data?.pincode ?? "",
                        longitude: data?.longitude ?? "",
                        latitude: data?.latitude ?? ""
                  }


                  if (state?.isEdit === true) {
                        datas["_id"] = state?.details;
                  }
                  if (state?.isEdit === true) {
                        axios.put(`${config}update/property`, datas).then((res) => {
                              alert.setSnack({
                                    ...alert,
                                    open: true,
                                    severity: AlertProps.severity.success,
                                    msg: "Property Updated Successfully",
                              });
                              history.goBack(-1);

                        }).catch((err) => {
                              alert.setSnack({
                                    ...alert,
                                    open: true,
                                    severity: AlertProps.severity.error,
                                    msg: "SomeThing Went Wrong",
                              });
                        });;
                  }
                  else {
                        axios.post(`${config}create/property`, datas).then((res) => {
                              alert.setSnack({
                                    ...alert,
                                    open: true,
                                    severity: AlertProps.severity.success,
                                    msg: "Property Created Successfully",
                              });
                              history.goBack(-1);
                        }).catch((err) => {
                              alert.setSnack({
                                    ...alert,
                                    open: true,
                                    severity: AlertProps.severity.error,
                                    msg: "SomeThing Went Wrong",
                              });
                        });;
                  }
            }

      }

      React.useEffect(() => {
            if (state?.isEdit) {
                  backdrop.setBackDrop({
                        ...backdrop,
                        open: true,
                        message: "Loading",
                  });
                  axios.get(`${config.api_url}get_propertyBy_id?id=${state?.details}`).then((res) => {
                        let editData = res?.data
                        setData({
                              id: editData?._id ?? "",
                              img: editData?.property_image ?? "",
                              period: {
                                    value: editData?.period ?? "",
                                    label: editData?.period ?? ""
                              },
                              des: editData?.description ?? "",
                              type: {
                                    value: editData?.property_type ?? "",
                                    label: editData?.property_type ?? ""
                              },
                              purpose: {
                                    value: editData?.revenue_type ?? "",
                                    label: editData?.revenue_type ?? ""
                              },
                              property: editData?.name ?? "",
                              revenue: {
                                    value: editData?.revenue_type ?? "",
                                    label: editData?.revenue_type ?? ""
                              },
                              unit: editData?.measure_unit ?? "",
                              capture: editData?.capture_area ?? "",
                              total: editData?.total_area ?? "",
                              built: new Date(editData?.year_built) ?? '',
                              handover: new Date(editData?.hand_over) ?? '',
                              pet: editData?.pets_allowed ?? false,
                              propertyType: {
                                    value: editData?.property_type ?? "",
                                    label: editData?.property_type ?? ""
                              },
                              mobile: {
                                    mobile: editData?.mobile_number ?? "",
                                    mobile_code: editData?.mobile_number_country_code ?? "",
                              },
                              email: editData?.email ?? "",
                              website: editData?.website ?? "",
                              doorno: editData?.door_no ?? "",
                              addressLineOne: editData?.address_line1 ?? "",
                              addressLineTwo: editData?.address_line2 ?? "",
                              addressLineThree: editData?.landmark ?? "",
                              district: editData?.district ?? "",
                              state: editData?.state ?? "",
                              country: editData?.country ?? "",
                              pincode: editData?.pincode ?? "",
                              latitude: editData?.latitude ?? "",
                              longitude: editData?.longitude ?? "",
                              error: {
                                    img: '',
                                    period: "",
                                    des: "",
                                    type: '',
                                    purpose: '',
                                    revenue: '',
                                    unit: '',
                                    capture: '',
                                    total: '',
                                    built: '',
                                    handover: '',
                                    pet: '',
                              }
                        })
                        setuploadImg(editData?.assests ?? [])
                        backdrop.setBackDrop({
                              ...backdrop,
                              open: false,
                              message: "",
                        });

                  }).catch((err) => {
                        backdrop.setBackDrop({
                              ...backdrop,
                              open: false,
                              message: "",
                        });
                        <Alert severity="error">SomeThing Went Wrong</Alert>
                  });
            }
            // eslint-disable-next-line
      }, [state?.isEdit])

      const validate = () => {
            let isValid = true;
            let error = data.error;
            if (data?.img?.length === 0) {
                  isValid = false;
                  error.img = "Property Image is Required";
            }
            if (data?.period?.length === 0) {
                  isValid = false;
                  error.period = "Payment Period is Required";
            }
            if (data?.property?.length === 0) {
                  isValid = false;
                  error.property = "Property Name is Required";
            }
            if (data?.propertyType?.length === 0) {
                  isValid = false;
                  error.propertyType = "Property Type is Required";
            }
            if (data?.purpose?.length === 0) {
                  isValid = false;
                  error.purpose = "Property Purpose is Required";
            }
            if (data?.revenue?.length === 0) {
                  isValid = false;
                  error.revenue = "Revenue Type is Required";
            }
            if (data?.unit?.length === 0) {
                  isValid = false;
                  error.unit = "Measurement Unit is Required";
            }
            if (data?.capture?.length === 0) {
                  isValid = false;
                  error.capture = "Carpet Area is Required";
            }
            if (data?.total?.length === 0) {
                  isValid = false;
                  error.total = "Total Area is Required";
            }
            if (data?.built?.length === 0) {
                  isValid = false;
                  error.built = "Year Built is Required";
            }
            if (data?.handover?.length === 0) {
                  isValid = false;
                  error.handover = "Handover Date is Required";
            }
            if (data?.mobile?.length === 0) {
                  isValid = false;
                  error.mobile = "Mobile Number is Required";
            }
            if (data?.email?.length === 0) {
                  isValid = false;
                  error.email = "Email is Required";
            }
            if (data?.website?.length === 0) {
                  isValid = false;
                  error.website = "Website is Required";
            }

            setData({ ...data, error });

            return isValid;
      };
      const validate1 = () => {
            let isValid = true;
            let error = data.error;
            if (data?.addressLineOne?.length === 0) {
                  isValid = false;
                  error.addressLineOne = "AddressLine 1 is Required";
            }
            if (data?.addressLineThree?.length === 0) {
                  isValid = false;
                  error.addressLineThree = "Land Mark is Required";
            }
            if (data?.district?.length === 0) {
                  isValid = false;
                  error.district = "District is Required";
            }
            if (data?.state?.length === 0) {
                  isValid = false;
                  error.state = "State is Required";
            }
            if (data?.country?.length === 0) {
                  isValid = false;
                  error.country = "Country is Required";
            }
            if (data?.pincode?.length === 0) {
                  isValid = false;
                  error.pincode = "Pincode is Required";
            }

            setData({ ...data, error });

            return isValid;
      };
      return (
            <>
                  <TopNavBar isBack Url={"/"} />
                  <div className={classes.root}>

                        <Container maxWidth='sm'>
                              <Grid>
                                    {
                                          step === 0 && <Step1 data={data} updateState={updateState} handleImg={handleImg} removeimg={removeimg} />
                                    }
                                    {
                                          step === 1 && <>
                                                <UploadBtn
                                                      value={uploadImg}
                                                      delete={deleteImg}
                                                      setuploadImg={setuploadImg}
                                                      uploadImg={uploadImg}
                                                      onDelete={onDelete}
                                                /></>
                                    }
                                    {
                                          step === 2 &&
                                          <div style={{ paddingBottom: '100px' }}>
                                                {/* map component */}
                                                <MapWithFields
                                                      imagebox={classes.imagebox1}
                                                      mapOptions={{
                                                            isInput: true,
                                                            center: {
                                                                  lat: data?.latitude,
                                                                  lng: data?.longitude
                                                            },
                                                            lat: data?.latitude,
                                                            lng: data?.longitude,
                                                            mapLoad
                                                      }}
                                                      mapResult={mapResultData}
                                                      autoCompletePlacement={{
                                                            top: 14,
                                                      }}
                                                      fields={[
                                                            // Door Number
                                                            {
                                                                  label: "Door Number",
                                                                  component: "TextField",
                                                                  value: data?.doorno,
                                                                  state_name: 'doorno',
                                                                  isrequired: false,
                                                                  placeholder: "Enter Door Number",
                                                                  error: data?.error?.doorno,
                                                                  errorMessage: data?.error?.doorno?.length > 0,
                                                                  breakpoints: {
                                                                        sm: 12,
                                                                        md: 12,
                                                                        lg: 12,
                                                                  }
                                                            },
                                                            // Address Line 1
                                                            {
                                                                  label: "Address Line 1",
                                                                  component: "TextField",
                                                                  value: data?.addressLineOne,
                                                                  state_name: 'addressLineOne',
                                                                  isrequired: true,
                                                                  placeholder: "Enter Address Line 1",
                                                                  errorMessage: data?.error?.addressLineOne,
                                                                  error: data?.error?.addressLineOne?.length > 0,
                                                                  breakpoints: {
                                                                        sm: 12,
                                                                        md: 12,
                                                                        lg: 12,
                                                                  }
                                                            },
                                                            // Address Line 2
                                                            {
                                                                  label: "Address Line 2",
                                                                  component: "TextField",
                                                                  value: data?.addressLineTwo,
                                                                  state_name: 'addressLineTwo',
                                                                  isrequired: false,
                                                                  placeholder: "Enter Address Line 2",
                                                                  errorMessage: data?.error?.addressLineTwo,
                                                                  error: data?.error?.addressLineTwo?.length > 0,
                                                                  breakpoints: {
                                                                        sm: 12,
                                                                        md: 12,
                                                                        lg: 12,
                                                                  }
                                                            },
                                                            // Landmark
                                                            {
                                                                  label: "Landmark",
                                                                  component: "TextField",
                                                                  value: data?.addressLineThree,
                                                                  state_name: 'addressLineThree',
                                                                  isrequired: false,
                                                                  placeholder: "Enter Landmark",
                                                                  errorMessage: data?.error?.addressLineThree,
                                                                  error: data?.error?.addressLineThree?.length > 0,
                                                                  breakpoints: {
                                                                        sm: 12,
                                                                        md: 12,
                                                                        lg: 12,
                                                                  }
                                                            },
                                                            // City
                                                            {
                                                                  label: "City",
                                                                  component: "TextField",
                                                                  value: data?.district,
                                                                  state_name: 'district',
                                                                  isrequired: true,
                                                                  placeholder: "Enter City",
                                                                  errorMessage: data?.error?.district,
                                                                  error: data?.error?.district?.length > 0,
                                                                  breakpoints: {
                                                                        sm: 12,
                                                                        md: 12,
                                                                        lg: 12,
                                                                  }
                                                            },
                                                            // State
                                                            {
                                                                  label: "State",
                                                                  component: "TextField",
                                                                  state_name: 'state',
                                                                  value: data?.state,
                                                                  isrequired: true,
                                                                  placeholder: "Enter State",
                                                                  errorMessage: data?.error?.state,
                                                                  error: data?.error?.state?.length > 0,
                                                                  breakpoints: {
                                                                        sm: 12,
                                                                        md: 12,
                                                                        lg: 12,
                                                                  }
                                                            },
                                                            // Country
                                                            {
                                                                  label: "Country",
                                                                  component: "TextField",
                                                                  value: data?.country,
                                                                  state_name: 'country',
                                                                  isrequired: true,
                                                                  placeholder: "Enter Country",
                                                                  errorMessage: data?.error?.country,
                                                                  error: data?.error?.country?.length > 0,
                                                                  breakpoints: {
                                                                        sm: 12,
                                                                        md: 12,
                                                                        lg: 12,
                                                                  }
                                                            },
                                                            // Pincode
                                                            {
                                                                  label: "Pincode",
                                                                  component: "TextField",
                                                                  state_name: 'pincode',
                                                                  value: data?.pincode,
                                                                  isrequired: true,
                                                                  placeholder: "Enter Pincode",
                                                                  errorMessage: data?.error?.pincode,
                                                                  error: data?.error?.pincode?.length > 0,
                                                                  breakpoints: {
                                                                        sm: 12,
                                                                        md: 12,
                                                                        lg: 12,
                                                                  }
                                                            },
                                                      ]}
                                                      onChangeFields={(key, value) => updateState(key, value)}
                                                />
                                          </div>
                                    }
                              </Grid>
                        </Container>
                        {/* btn */}
                        <Box className={classes.bottomBtn}>
                              <Box>
                                    <Button
                                          variant="contained"
                                          className={classes.cancel}
                                          onClick={() => previous()}
                                          disabled={step === 0 ? true : false}
                                    >
                                          Previous
                                    </Button>
                              </Box>
                              <Box>
                                    <Button
                                          variant="contained"
                                          className={classes.sumbit}
                                          onClick={step === 2 ? sumbit : () => next()}
                                    >
                                          {step === 2 ? 'Submit' : 'Next'}
                                    </Button>
                              </Box>
                        </Box>

                  </div>
            </>
      )
}