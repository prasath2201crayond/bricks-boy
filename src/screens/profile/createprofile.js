import React from 'react';
import { Box, Grid, Avatar, Button } from "@mui/material";
import makeStyles from "@mui/styles/makeStyles";
import { withBottombar } from "./../../HOCs";
// import CameraAltOutlinedIcon from '@mui/icons-material/CameraAltOutlined';
import { TextBox, MobileNumberInputComponent, CreateProfileImg } from "../../components";
import axios from "axios";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "react-circular-progressbar/dist/styles.css";
import { useHistory } from 'react-router-dom';
import { Routes } from '../../router/routes';
import {config}from '../../config'

const useStyles = makeStyles((theme) => ({
      root: {
            backgroundImage: `url(${"/images/loginmain.svg"})`,
            height: '100%',
            backgroundRepeat:'no-repeat',
            backgroundSize:'cover',
            backgroundPosition:'center'
      },
      top: {
            height: '180px',
            position: 'relative',
            padding: '17px',
            marginBottom: '60px',
            backgroundSize: 'cover',
            backgroundPosition: 'center',
            backgroundRepeat: 'no-repeat'
      },
      profile: {
            position: 'absolute',
            bottom: '-48px',
            border: '2px solid #fff',
            borderRadius: '50%'
      },

      main: {
            padding: '0px 24px 72px',
            textAlign: 'center'
      },
      signin: {
            borderRadius: '30px',
            height: '47px',
            backgroundColor: '#A56FF8',
            width: '236px',
            marginTop: '35px',
            marginBottom: '10px',
            '&:hover': {
                  backgroundColor: '#fff',
                  color: '#000',
                  border: '1px solid #A56FF8',

            }
      },
      cameramain: {
            position: 'absolute',
            bottom: '5px',
            right: '0px'
      },
      cameraback: {
            position: 'absolute',
            bottom: '5px',
            right: '5px'
      },
      uploadimg: {
            borderRadius: '50%'
      },
      deleteimg: {
            position: 'absolute',
            top: '0px',
            right: '8px',
            width: '20px',
            height: '20px',
            cursor: 'pointer'
      },
      deleteimg1: {
            position: 'absolute',
            top: '14px',
            right: '8px',
            width: '20px',
            height: '20px',
            cursor: 'pointer'
      }

}));

const initial = {
      image: '',
      backgroungImage: '',
      name: '',
      nickname: '',
      email: '',
      bio: '',
      number: '',
      error: {
            image: '',
            name: '',
            email: '',
            bio: '',
            number: '',
      }
}

const CreateProfile = () => {

      const history = useHistory();
      const classes = useStyles();

      const [profile, setProfile] = React.useState({ ...initial })


      const updateState = (key, value) => {
            console.log(value)
            let error = profile.error;
            error[key] = "";
            setProfile({ ...profile, [key]: value, error })
      }


      const isIamValideTostep1 = () => {
            let isValid = true;
            let error = profile.error;
            //Checking name
            if (profile.name.length === 0) {
                  isValid = false;
                  error.name = "Name is Required";
            }
            //Checking email
            if (profile.email.length === 0) {
                  isValid = false;
                  error.email = "Email is Required";
            }
            //Checking email
            if (profile.number.length === 0) {
                  isValid = false;
                  error.number = "Mobile Number is Required";
            }
            //eslint-disable-next-line
            var email = /^\w+([\.-]?\ w+)*@\w+([\.-]?\ w+)*(\.\w{2,3})+$/.test(profile?.email);

            if (!email) {
                  isValid = false;
                  toast.error("Enter valid email");
            }

            setProfile({ ...profile, error });
            return isValid;
      };

      const Enter = () => {
            if (isIamValideTostep1()) {
                  const datas = {
                        name: profile?.name,
                        email: profile?.email,
                        phone: profile?.number?.mobile,
                        countrycode: profile?.number?.mobile_code,
                        bio: profile?.bio,
                        profile: profile?.image,
                        background: profile?.backgroungImage,
                        nickname: profile?.nickname,
                        user_id: localStorage.getItem('UserID'),
                  }
                  axios.post(`${config}profile/create`, datas).then((res) => {
                        setTimeout(() => {
                              toast.success("Succesfully Created");
                              setTimeout(() => {
                                    history.push(Routes.home);
                              }, 1000);
                        }, 1000);
                  });
                  console.log(datas)
            }

      }


      const deleteImg = (key) => {
            if (key === 'image') {
                  return setProfile({ ...profile, image: "" })
            }
            if (key === 'backgroungImage') {
                  return setProfile({ ...profile, backgroungImage: "" })
            }

      }
      console.log(profile.image, 'fkkfkf')
      return (
            <div className={classes.root}>
                  <Box className={classes.top} style={{ backgroundImage: profile.backgroungImage?.length > 0 ? `url(${profile?.backgroungImage})` : `url(${"/images/back.svg"})` }}>
                        <img src='/images/back.svg' alt='back' />
                        {
                              profile.backgroungImage?.length > 0 && <img src='/images/delete.svg' alt='delete' className={classes.deleteimg1} onClick={() => deleteImg('backgroungImage')} />
                        }

                        <CreateProfileImg
                              type='backgroungImage'
                              classposition={classes.cameraback}
                              value={profile?.backgroungImage}
                              delete={deleteImg}
                              onChange={(key, e) => updateState(key, e)} />

                        <Box className={classes.profile}>
                              {
                                    profile?.image?.length > 0 ? <div className={classes.uploadimg}><Avatar
                                          alt={profile?.image}
                                          src={profile?.image}
                                          sx={{ width: 100, height: 100 }}
                                    />

                                          <img src='/images/delete.svg' alt='delete' className={classes.deleteimg} onClick={() => deleteImg('image')} />
                                    </div> : <CreateProfileImg
                                          isAvatar
                                          classposition={classes.cameramain}
                                          type='image'
                                          value={profile?.image}
                                          delete={deleteImg}
                                          onChange={(key, e) => updateState(key, e)} />
                              }

                        </Box>
                  </Box>



                  {/* data */}
                  <div className={classes.main}>
                        <Grid container>
                              <ToastContainer />
                              <Grid item xs={12}>
                                    <TextBox
                                          label="Name"
                                          isRequired
                                          placeholder='Enter Your Name'
                                          value={profile?.name}
                                          onChange={(e) => updateState("name", e.target.value)}
                                          isError={profile?.error?.name?.length > 0}
                                          errorMessage={profile?.error?.name}
                                    />
                                    <Box height='10px' />
                                    <TextBox
                                          label="Nick Name"
                                          placeholder='Enter Your Nick Name'
                                          value={profile?.nickname}
                                          onChange={(e) => updateState("nickname", e.target.value)}
                                    />
                                    <Box height='10px' />
                                    <TextBox
                                          isRequired
                                          label="Email"
                                          placeholder='Enter Your Email'
                                          value={profile?.email}
                                          onChange={(e) => updateState("email", e.target.value)}
                                          isError={profile?.error?.email?.length > 0}
                                          errorMessage={profile?.error?.email}
                                    />
                                    <Box height='10px' />
                                    <MobileNumberInputComponent
                                          color
                                          isRequired
                                          label="Mobile Number"
                                          placeholder="Enter Mobile Number"
                                          value={profile.number}
                                          handleChange={(value) => updateState("number", value)}
                                          isError={profile?.error?.number?.length > 0}
                                          errorMessage={profile?.error?.number}
                                    />
                                    <Box height='10px' />
                                    <TextBox
                                          label="Bio"
                                          placeholder='Enter Your Bio'
                                          value={profile?.bio}
                                          onChange={(e) => updateState("bio", e.target.value)}
                                          multiline
                                    />
                                    <Box height='10px' />
                                    <Button
                                          variant={"contained"}
                                          color={"primary"}
                                          onClick={Enter}
                                          className={classes.signin}
                                    >
                                          Enter
                                    </Button>
                                    <Box height='10px' />
                              </Grid>
                        </Grid>
                  </div>
            </div>
      )
}
export default withBottombar(CreateProfile)