import * as React from "react";
import { Container, Grid, Hidden } from "@mui/material";
import makeStyles from "@mui/styles/makeStyles";
import { MessageBox } from "../../components/reactquill/textboxmsg";
import { UserCard } from '../../components';
import axios from "axios";
import { AlertContext } from "../../contexts";
import { AlertProps } from "../../utils";
import { useHistory } from 'react-router-dom';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import socketClient from "socket.io-client";
import { v4 as uuidv4 } from "uuid";
import jwt_decode from "jwt-decode";
import { config } from '../../config'

const useStyles = makeStyles((theme) => ({
      root: {
            background: theme.palette.primary.dark,
            width: "100%",
            height: "100%",
            overflow: "hidden",
            [theme.breakpoints.up("sm")]: {
                  marginTop: 64,
            },
      },
      editor: {
            position: 'absolute',
            bottom: 0,
            right: 0,
            left: 0,
            width: '100%',
            margin: 'auto',
            borderRadius: '12px',
            backgroundColor: '#22252C',
            paddingBottom: '24px',
            [theme.breakpoints.down("sm")]: {
                  padding: '0px 24px 73px',
                  maxWidth: '100%',
            },
      },
      mySelf: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'right',
            padding: '10px'
      },
      theirself: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'left',
            padding: '10px'
      },
      myselfmsg: {
            backgroundColor: '#146c62',
            color: '#fff',
            padding: '5px',
            borderRadius: '12px 12px 0px 12px',
            margin: '10px 0px'
      },
      theirselfmsg: {
            backgroundColor: '#fff',
            color: '#000',
            padding: '5px',
            borderRadius: '12px 12px 12px 0px',
            margin: '10px 0px'
      },
      message: {
            height: "100%",
            overflow: 'auto',
            paddingBottom: '77px'
      }
}));






export const Profile = () => {
      const history = useHistory();
      const classes = useStyles();
      const theme = useTheme()
      var socket = socketClient('https://bricksboy123.herokuapp.com');
      let isXs = useMediaQuery(theme.breakpoints.down('md'))
      const [msg, setMsg] = React.useState('')
      const [msgSend, setMsgSend] = React.useState(false)
      const [data, setData] = React.useState([])
      const [show, setShow] = React.useState(false);
      const [selectUser, setSelectUser] = React.useState({
            id: "",
            name: "",
            deviceToken: ""
      })
      const alert = React.useContext(AlertContext);
      const [messages, setMessages] = React.useState([])
      const authToken = localStorage.getItem("auth");
      const tempDecoded = jwt_decode(authToken);
      const getAllData = () => {
            axios.get(`${config.api_url}get/all/users`).then((res) => {
                  setData(res?.data)
            }).catch((err) => {
            });
      }

      React.useEffect(() => {
            getAllData()
      }, [])

      const onEmojiClick = (event, emojiObject) => {
            if (msg !== "") {
                  setMsg(msg + emojiObject.emoji);
            } else {
                  setMsg(emojiObject.emoji);
            }

            setShow(!show);
      };
      const selectuser = (e) => {
            if (isXs) {
                  history.push({
                        pathname: '/messages',
                        state: {
                              main: {
                                    profile: e,
                              },
                        },
                  })
            }
            else {
                  setSelectUser({ ...selectUser, id: e?._id, name: e?.name, deviceToken: e?.deviceToken })
                  getData(e)
            };
      }
      const sendData = () => {
            if (selectUser?.id?.length > 0 && msg?.length > 0) {
                  socket.emit("send-msg", {
                        from: tempDecoded?.id ?? localStorage.getItem('UserID'),
                        to: selectUser?.id,
                        message: msg
                  });

                  const newMsg = {
                        fromSelf: true,
                        message: msg,
                        createddat: new Date().toISOString(),
                        id: uuidv4()
                  }
                  setMessages(messages?.concat(newMsg))

                  const data = {
                        from: tempDecoded?.id ?? localStorage.getItem('UserID'),
                        to: selectUser?.id,
                        message: msg
                  }

                  axios.post(`${config.api_url}create/chat`, data).then((res) => {
                        setMsg("")
                        setMsgSend(!msgSend)
                        sendNotification()
                  }).catch((err) => {
                        alert.setSnack({
                              ...alert,
                              open: true,
                              severity: AlertProps.severity.error,
                              msg: "Some Thing Went Wrong",
                        });
                  });;
            }
            else {
                  alert.setSnack({
                        ...alert,
                        open: true,
                        severity: AlertProps.severity.info,
                        msg: "Please Click Your Friend",
                  });
            }

      }
      const getData = (e) => {
            const data = {
                  "from": tempDecoded?.id ?? tempDecoded?.id ?? localStorage.getItem('UserID'),
                  "to": e?._id
            }
            axios.post(`${config.api_url}get/message`, data).then((res) => {
                  setMessages(res?.data)
                  const element = document.getElementById(res?.data?.[res?.data?.length - 1]?.id);
                  element.scrollIntoView({ behavior: "smooth" });
            }).catch((err) => {

            });
      }
      socket.on("msg-recieve", (msg) => {
            console.log(msg)
            if (msg?.from === selectUser?.id && msg?.to === tempDecoded?.id) {
                  const newMsg = {
                        fromSelf: msg.from === tempDecoded?.id ?? localStorage.getItem('UserID') ? true : false,
                        message: msg.message,
                        createddat: new Date().toISOString(),
                        id: uuidv4()
                  }
                  setMessages(messages?.concat(newMsg))
                  let result = messages?.concat(newMsg);
                  const element = document?.getElementById(result?.[result?.length - 1]?.id);
                  element?.scrollIntoView({ behavior: "smooth" });
            }
      });
      const sendNotification = () => {
            const body = {
                  "to": selectUser?.deviceToken,
                  "notification": {
                        "body": msg,
                        "title": tempDecoded?.name,
                        "icon":tempDecoded?.profileimage
                  }
            };
            axios.post(`https://fcm.googleapis.com/fcm/send`, body, {
                  headers: {
                        'Authorization': `key=${config.serverKey}`, "Content-Type": "application/json"
                  }
            })
                  .then((res) => {
                        console.log(res)
                  })
                  .catch((err) => {
                        console.log(err);

                  });
      }
      React.useEffect(() => {
            if (messages?.length > 0) {
                  const element = document.getElementById(messages?.[messages?.length - 1]?.id);
                  element?.scrollIntoView({ behavior: "smooth" });
            }
            // eslint-disable-next-line
      }, [msgSend])

      return (
            <div className={classes.root}>
                  <Container maxWidth="lg" sx={{ position: 'relative' }}>
                        <Grid container>
                              <Grid item lg={6} md={6} sm={12} xs={12}>
                                    {
                                          data?.filter(x => x?._id !== tempDecoded?.id ?? localStorage.getItem('UserID'))?.map((x) => {
                                                return (
                                                      <UserCard data={x} selectuser={selectuser} user={selectUser} />
                                                )
                                          })
                                    }
                              </Grid>
                              <Hidden mdDown>
                                    <Grid item lg={6} md={6} sm={12} xs={12} style={{ position: 'relative', overflow: 'auto', height: '77vh', padding: '0px 24px', borderLeft: '1px dashed gray' }}>

                                          <div className={classes.message}>
                                                {
                                                      messages?.length > 0 ?
                                                            <>
                                                                  {
                                                                        messages?.map((x) => {
                                                                              return (
                                                                                    <div id={x?.id}>
                                                                                          <div className={x?.fromSelf ? classes.mySelf : classes.theirself}>
                                                                                                <span className={x?.fromSelf ? classes.myselfmsg : classes.theirselfmsg}>{x?.message}</span>
                                                                                          </div>
                                                                                    </div>
                                                                              )
                                                                        })
                                                                  }

                                                            </> :
                                                            <div style={{ height: '100%', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                                                                  <img src='/images/Building Construction Logo template (1).gif' alt='' width={'50%'} height={'50%'} />
                                                            </div>
                                                }

                                          </div>
                                          <div className={classes.editor}>
                                                <MessageBox
                                                      onEmojiClick={onEmojiClick}
                                                      value={msg ?? ""}
                                                      handleChange={(e) => {
                                                            setMsg(e.target.value);
                                                      }}
                                                      className={classes.editor}
                                                      show={show}
                                                      setShow={setShow}
                                                      sendData={sendData}
                                                />
                                          </div>

                                    </Grid>
                              </Hidden>
                        </Grid>




                  </Container>
            </div>
      )
}