export { default as NotFound } from "./notFound";
export { default as Home } from "./home";
export { default as Login } from "./login";
export { default as Profile } from "./profile";
export { default as CreatePost } from "./createpost";
export { default as PropertyDeatils } from './propertydetails';
export { default as MyProperty } from './myproperty'