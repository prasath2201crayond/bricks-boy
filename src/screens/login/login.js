import React from 'react';
import { Button, Typography, Grid, Box, Hidden } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import { useHistory, Link } from 'react-router-dom';
import { Routes } from '../../router/routes';
import { TextBox, Password } from "../../components";
import axios from "axios";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "react-circular-progressbar/dist/styles.css";
import jwt_decode from "jwt-decode";
import {AlertContext } from "../../contexts";
import { AlertProps } from "../../utils";
import {config}from '../../config'

const useStyles = makeStyles((theme) => ({
    root: {
        background: theme.palette.primary.dark,
        width: "100%",
        height: "100%",
        overflow: "auto",
        display: 'flex',
        alignItems: 'center',
        padding: '0px 40px'
    },
    signin: {
        borderRadius: '30px',
        height: '47px',
        backgroundColor: '#0D9C7D',
        width: '100%',
        marginTop: '35px',
        marginBottom: '10px',
        '&:hover': {
            backgroundColor: '#22252C',
            color: '#0D9C7D !important',
            border: '1px solid #0D9C7D',

        }
    },
    title: {
        color: '#DFB160',
        fontSize: '40px',
        fontWeight: 600,
        marginBottom: '24px'
    }
}))

const initial = {
    email: "",
    password: "",
    error: {
        email: "",
        password: "",
    }
};

export const Login = props => {

    const history = useHistory();
    const classes = useStyles();
    const alert = React.useContext(AlertContext);
    const [data, setData] = React.useState({ ...initial });
    const updateState = (key, value) => {
        let error = data.error;
        error[key] = "";
        setData({ ...data, [key]: value, error });
    };

    const isIamValideTostep1 = () => {
        let isValid = true;
        let error = data.error;
        //Checking email
        if (data.email.length === 0) {
            isValid = false;
            error.email = "Email is Required";
        }
        //Checking password
        if (data.password.length === 0) {
            isValid = false;
            error.password = "Password is Required";
        }
        setData({ ...data, error });
        return isValid;
    };

    const updateDeviceToken=(id,device_token)=>{
        const datas = {
            _id: id,
            deviceToken: device_token
        }
        axios.put(`${config.api_url}update/deviceToken`, datas).then((res) => {
            alert.setSnack({
                ...alert,
                open: true,
                severity: AlertProps.severity.success,
                msg: "Succesfully Login",
            });
            history.push(Routes.home);
        }).catch((err) => {
            alert.setSnack({
                ...alert,
                open: true,
                severity: AlertProps.severity.error,
                msg: "Some Thing Went Wrong",
            });
        });
    }

    const onLogin = () => {
        if (isIamValideTostep1()) {
            const datas = {
                email: data?.email,
                password: data?.password
            }
            axios.post(`${config.api_url}login`, datas).then((res) => {
                let userId = jwt_decode(res?.data);
                localStorage.setItem('UserID', userId?.id);
                localStorage.setItem('auth', res?.data);
                updateDeviceToken(userId?.id ,  localStorage.getItem('device_token'))
                history.push(Routes.home);
            }).catch((err) => {
                alert.setSnack({
                    ...alert,
                    open: true,
                    severity: AlertProps.severity.error,
                    msg: "Some Thing Went Wrong",
                });
            });
        }
    }

    return (
        <div className={classes.root}>
            <ToastContainer />
            <Grid container spacing={3} sx={{ alignItems: 'center' }}>
                <Grid item lg={6} md={12} sm={12}>
                    <Hidden smDown>
                        <img src='/images/mainlogo.png' alt='main' />
                    </Hidden>
                </Grid>
                <Grid item lg={6}>
                    <Box className={classes.login}>
                        <Typography className={classes.title}>Sign In</Typography>
                        <TextBox
                            label="Email ID"
                            isrequired
                            placeholder='Enter Your Email ID'
                            value={data?.email}
                            onChange={(e) => updateState("email", e.target.value)}
                            isError={data?.error?.email?.length > 0}
                            errorMessage={data?.error?.email}
                        />
                        <Box height='10px' />
                        <Password
                            label="Password"
                            isRequired
                            placeholder='Enter Password'
                            value={data?.password}
                            onChange={(e) => updateState("password", e.target.value)}
                            isError={data?.error?.password?.length > 0}
                            errorMessage={data?.error?.password}
                        />
                        <Button
                            variant={"contained"}
                            color={"primary"}
                            onClick={onLogin}
                            className={classes.signin}
                        >
                            Sign In
                        </Button>

                        <Typography style={{ fontSize: '12px', color: '#DFB160' }}>I Don't have an account?&nbsp;<Link to='/register'><span sx={{ color: '#0D9C7D !important' }}>Sign Up</span></Link></Typography>
                    </Box>
                </Grid>
            </Grid>

        </div >
    )
}
