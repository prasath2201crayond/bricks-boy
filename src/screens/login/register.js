import React from "react";
import { Button, Typography, Box, Grid, Hidden, Avatar } from "@mui/material";
import makeStyles from "@mui/styles/makeStyles";
import { TextBox, Password } from "../../components";
import axios from "axios";
import { useHistory } from 'react-router-dom';
import { Routes } from '../../router/routes';
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "react-circular-progressbar/dist/styles.css";
import { AlertContext } from "../../contexts";
import { AlertProps } from "../../utils";
import { BackdropContext } from "../../contexts";
import {config}from '../../config'
import { getDownloadURL, ref, storage, uploadBytesResumable } from "../../firebase";
const useStyles = makeStyles((theme) => ({
  root: {
    background: theme.palette.primary.dark,
    width: "100%",
    height: "100%",
    overflow: "auto",
    display: 'flex',
    alignItems: 'center',
    padding: '0px 40px',
    textAlign: 'left'
  },
  signin: {
    borderRadius: '30px',
    height: '47px',
    backgroundColor: '#0D9C7D',
    width: '100%',
    marginTop: '35px',
    marginBottom: '10px',
    '&:hover': {
      backgroundColor: '#22252C',
      color: '#0D9C7D !important',
      border: '1px solid #0D9C7D',

    }
  },
  title: {
    color: '#DFB160',
    fontSize: '40px',
    fontWeight: 600,
    marginBottom: '24px'
  },
  avatar: {
    height: '120px',
    width: '120px',
    backgroundColor: theme.palette.primary.main
  },
  avatar1: {
    padding: '35px',
    background: '#0D9E8D',
    height: '120px',
    width: '120px'
  },
  removeimg: {
    textDecoration: 'underline',
    color: '#fff',
    fontSize: '12px',
    marginTop: '6px',
    cursor: 'pointer',
    marginLeft:'25px'
  },
  button: {
    border: `1px dashed ${theme.palette.tertiary.main}`,
    width: "100%",
    borderRadius: "12px",
    color: theme.palette.tertiary.main,
    height: "55px",
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    maxWidth:'159px',
    "&:hover": {
      border: `1px dashed ${theme.palette.tertiary.main}`,
    },
  },
  imagebox:{
    // textAlign:'center',
    width:'100%'
  }
}));

const initial = {
  name: "",
  email: "",
  password: "",
  img: "",
  conformpassword: '',
  error: {
    name: "",
    email: "",
    password: "",
    conformpassword: '',
  }
};
export const Register = (props) => {
  const classes = useStyles();
  const history = useHistory();
  const [data, setData] = React.useState({ ...initial });
  const alert = React.useContext(AlertContext);
  const backdrop = React.useContext(BackdropContext);
  const updateState = (key, value) => {
    let error = data.error;
    error[key] = "";
    setData({ ...data, [key]: value, error });
  };

  const sumbit = () => {
    const datas = {
      name: data?.name,
      email: data?.email,
      password: data?.password,
      profileimage:data?.img
    }
    if (isIamValideTostep1()) {
      axios.post(`${config.api_url}register`, datas)
        .then((res) => {
          history.push(Routes.login);
          alert.setSnack({
            ...alert,
            open: true,
            severity: AlertProps.severity.success,
            msg: "Succesfully Register",
          });
        })
        .catch((err) => {
          alert.setSnack({
            ...alert,
            open: true,
            severity: AlertProps.severity.success,
            msg: "Some  Thing Went Wrong",
          });
        });
    };
  };

  const isIamValideTostep1 = () => {
    let isValid = true;
    let error = data.error;

    //Checking Name
    if (data?.name?.length === 0) {
      isValid = false;
      error.name = "Name is Required";
    }
    //Checking email
    if (data?.email?.length === 0) {
      isValid = false;
      error.email = "Email is Required";
    }
    //Checking password
    if (data?.password?.length === 0) {
      isValid = false;
      error.password = "Password is Required";
    }
    //Checking password
    if (data?.conformpassword?.length === 0) {
      isValid = false;
      error.conformpassword = "Conform Password is Required";
    }
    if (data?.password !== data?.conformpassword) {
      isValid = false;
      error.conformpassword = "Password & Conform Password must be same";
    }

    //eslint-disable-next-line
    var phone = /^\w+([\.-]?\ w+)*@\w+([\.-]?\ w+)*(\.\w{2,3})+$/.test(data?.email);

    if (!phone) {
      isValid = false;
      toast.error("Enter valid email");
    }

    setData({ ...data, error });
    return isValid;
  };

  // image uploading function
  const handleImg = async (file) => {
    if (!file) return
    const storageRef = ref(storage, `files/${file?.name}`)
    const uploadFile = uploadBytesResumable(storageRef, file)
    await uploadFile.on("state_changed", (snapshot) => {
      const value = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100)
      backdrop.setBackDrop({
        ...backdrop,
        open: true,
        message: `Uploading... ${value}%`,
      });
    }, (err) => {
      console.log(err)
    }, () => {
      getDownloadURL(uploadFile.snapshot.ref).then((url) => {
        updateState('img', url)
        backdrop.setBackDrop({
          ...backdrop,
          open: false,
          message: "",
        });
      })
    })
  }
        // remove function
        const removeimg = () => {
          setData({ ...data, img: "" })
    }
  return (

    <div className={classes.root}>
      <ToastContainer />
      <Grid container spacing={3} sx={{ alignItems: 'center' }}>
        <Grid item lg={6} md={12} sm={12}>
          <Hidden smDown>
            <img src='/images/mainlogo.png' alt='main' />
          </Hidden>
        </Grid>
        <Grid item lg={6} md={12} sm={12}>
          <Box className={classes.login}>
            <Typography className={classes.title}>Sign Up</Typography>
            <Box className={classes.imagebox}>
              <Avatar src={data?.img?.length > 0 ? data?.img : "/images/citynew.svg"}  className={data?.img?.length > 0 ? classes.avatar : classes.avatar1} />
              <div>
                <label> <Box className={classes.button} style={{ marginTop: '10px' }}>    Upload image</Box> <input type='file' style={{ display: 'none' }} accept="image/*"
                  onClick={(event) => {
                    event.target.value = null
                  }}
                  onChange={(e) => handleImg(e?.target?.files?.[0])} /> </label>
              </div>
              {data?.img?.length > 0 && <Typography className={classes.removeimg} onClick={removeimg}>Remove Image</Typography>}
              <Box height={"18px"} />


            </Box>
            <Box height='10px' />
            <TextBox
              type='text'
              isrequired
              label="Name"
              placeholder="Enter Name"
              value={data?.name}
              onChange={(e) => updateState("name", e.target.value)}
              isError={data?.error?.name?.length > 0}
              errorMessage={data?.error?.name}
            />
            <Box height='10px' />
            <TextBox
              type='email'
              isrequired
              label="Email"
              placeholder="Enter Email"
              value={data?.email}
              onChange={(e) => updateState("email", e.target.value)}
              isError={data?.error?.email?.length > 0}
              errorMessage={data?.error?.email}
            />
            <Box height='10px' />
            <Password
              isRequired
              type='password'
              label="Password"
              placeholder="Enter Password"
              value={data?.password}
              onChange={(e) => updateState("password", e.target.value)}
              isError={data?.error?.password?.length > 0}
              errorMessage={data?.error?.password}
            />
            <Box height='10px' />
            <Password
              isRequired
              type='password'
              label="Conform Password"
              placeholder="Enter Conform Password"
              value={data?.conformpassword}
              onChange={(e) => updateState("conformpassword", e.target.value)}
              isError={data?.error?.conformpassword?.length > 0}
              errorMessage={data?.error?.conformpassword}
            />

            <Button
              variant={"contained"}
              color={"primary"}
              onClick={sumbit}
              className={classes.signin}
            >
              Sign Up
            </Button>
          </Box>
        </Grid>
      </Grid>

    </div>
  );
};
