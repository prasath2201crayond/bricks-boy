import React from 'react';
import { Button,  Container, Box } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import { useHistory } from 'react-router-dom';
import { Routes } from '../../router/routes';

const useStyles = makeStyles((theme) => ({
      root: {
            textAlign: "center",
            overflow:"hidden"
      },
      login: {
            boxShadow: '0px 8px 69px #0000001A',
      },
      button: {
            display: 'block',
            position:'absolute',
            left:'0',
            right:"0",
            bottom:"0",
            padding:'10px 0px',
            background:"#fff"
      },
      loginbtn: {
            borderRadius: '30px',
            height: '47px',
            border: '1px solid #A56FF8',
            width: '236px',
            marginBottom: '12px',
            color: '#000',
            '&:hover': {
                  backgroundColor: '#A56FF8',
                  color: '#fff',

            }
      },
      signin: {
            borderRadius: '30px',
            height: '47px',
            backgroundColor: '#A56FF8',
            width: '236px',
            '&:hover': {
                  backgroundColor: '#fff',
                  color: '#000',
                  border: '1px solid #A56FF8',

            }
      },
      fit: {
            display: 'flex',
            justifyContent: 'center',
            '&img': {
                  objectFit: 'cover'
            }
      }
}))



export const WelcomePage = props => {

      const history = useHistory();
      const classes = useStyles();
      return (
            <Container maxWidth="sm" sx={{ padding: 0 }}>
                  <div className={classes.root} container='sm'>
                        <Box className={classes.login}>
                              <Box className={classes.fit}>
                                    <img src='/images/loginimg.jpg' alt='images' style={{ objectFit: 'cover', height: '100%', width: '100%' }} />
                              </Box>
                              <Box className={classes.button}>
                                    <Box>
                                          <Button
                                                variant={"outlined"}
                                                color={"primary"}
                                                onClick={()=>history.push(Routes.login)}
                                                className={classes.loginbtn}
                                          >
                                                Login
                                          </Button>
                                    </Box>

                                    <Box>
                                          <Button
                                                variant={"contained"}
                                                color={"primary"}
                                                className={classes.signin}
                                                onClick={()=>history.push(Routes.register)}
                                          >
                                                Create new account
                                          </Button>
                                    </Box>


                              </Box>
                        </Box>
                  </div>
            </Container>
      )
}
