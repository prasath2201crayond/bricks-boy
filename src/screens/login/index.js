import React from "react";
import { Login } from './login';
import { withRouter } from 'react-router-dom';

class LoginParent extends React.Component {
    render() {
        return <Login />;
    }
}

export default withRouter(LoginParent);
