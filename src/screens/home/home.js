import React from "react";
import makeStyles from "@mui/styles/makeStyles";
import { Container, Grid, Alert } from "@mui/material";
import { CardComponent } from "../../components/voteCard";
import { useHistory } from "react-router-dom";
import axios from "axios";
import {config}from '../../config'
const useStyles = makeStyles((theme) => ({
  root: {
    color: theme.palette.text.contrastText,
    margin: "40px 0px",
    height: "100%",
    [theme.breakpoints.down("sm")]: {
      paddingBottom: 40,
    },
  },
  fab: {
    position: "fixed",
    bottom: 16,
    right: 16,
    display: "flex",
    alignItems: "center",
    background: theme.palette.tertiary.main,
    boxShadow: "none",
    "&:hover": {
      background: theme.palette.tertiary.main,
    },
    [theme.breakpoints.down("sm")]: {
      bottom: 70,
      right: 14,
    },
  },
  fab1: {
    position: "fixed",
    bottom: 16,
    right: 16,
    display: "flex",
    alignItems: "center",
    background: theme.palette.tertiary.main,
    boxShadow: "none",
    width: "220px",
    borderRadius: "32px",
    "&:hover": {
      background: theme.palette.tertiary.main,
    },
    [theme.breakpoints.down("sm")]: {
      bottom: 70,
      right: 14,
    },
  },
  sumbitidea: {
    color: "#fff",
    marginLeft: "10px",
    fontSize: 13,
  },
}));

export const Home = (props) => {
  const classes = useStyles();
  const [details, setDeatils] = React.useState([])
  const [loading, setLoading] = React.useState(false)
  const history = useHistory();

  const getAllData = () => {
    setLoading(true)
    axios.get(`${config.api_url}get/all/property`).then((res) => {
      setDeatils(res?.data)
      setLoading(false)
    }).catch((err) => {
      <Alert severity="error">SomeThing Went Wrong</Alert>
      setLoading(false)
    });
  }
  const goProperty = (data) => {
    history.push({
      pathname: '/propertyview',
      state: {
        main: {
          id: data ?? "",
        },
      },
    });
  }
  React.useEffect(() => {
    getAllData()
  }, [])

  return (
    <Container>
      <div className={classes.root}>
        {
          loading ? <div style={{display:'flex',alignItems:'center',height:'100%',justifyContent:'center'}}><img src={'/images/loader.svg'} alt='' /></div>: <Grid container spacing={2}>
            {details?.map((item, index) => {
              return (
                <Grid item xs={12} sm={4}>
                  <CardComponent index={index} item={item} goProperty={goProperty} />
                </Grid>
              );
            })}
          </Grid>
        }

      </div>

    </Container>
  );
};
