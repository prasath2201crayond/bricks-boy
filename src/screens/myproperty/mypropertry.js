import React from "react";
import makeStyles from "@mui/styles/makeStyles";
import { Container, Grid, Typography, Alert } from "@mui/material";
import Fab from "@mui/material/Fab";
import { CardComponent } from "../../components/voteCard";
import { useHistory } from "react-router-dom";
import axios from "axios";
import {config}from '../../config'


const useStyles = makeStyles((theme) => ({
  root: {
    color: theme.palette.text.contrastText,
    margin: "40px 0px",
    height: "100%",
    [theme.breakpoints.down("sm")]: {
      paddingBottom: 40,
    },
  },
  fab: {
    position: "fixed",
    bottom: 16,
    right: 16,
    display: "flex",
    alignItems: "center",
    background: theme.palette.tertiary.main,
    boxShadow: "none",
    "&:hover": {
      background: theme.palette.tertiary.main,
    },
    [theme.breakpoints.down("sm")]: {
      bottom: 70,
      right: 14,
    },
  },
  fab1: {
    position: "fixed",
    bottom: 16,
    right: 16,
    display: "flex",
    alignItems: "center",
    background: theme.palette.tertiary.main,
    boxShadow: "none",
    width: "220px",
    borderRadius: "32px",
    "&:hover": {
      background: theme.palette.tertiary.main,
    },
    [theme.breakpoints.down("sm")]: {
      bottom: 70,
      right: 14,
    },
  },
  sumbitidea: {
    color: "#fff",
    marginLeft: "10px",
    fontSize: 13,
  },
}));

export const MyProperty = (props) => {
  const classes = useStyles();
  const [isShow, setisShow] = React.useState(false);
  const [details, setDeatils] = React.useState([])
  const history = useHistory();
  const sumbit = () => {
    history.push('/createpost');
  };
  const getAllData = () => {
    axios.get(`${config.api_url}get_propertyBy_user?id=${localStorage.getItem('UserID')}`).then((res) => {
      setDeatils(res?.data)
    }).catch((err) => {
      <Alert severity="error">SomeThing Went Wrong</Alert>
    });
  }
  const goProperty = (data) => {
    history.push({
      pathname: '/propertyview',
      state: {
        main: {
          id: data ?? "",
          isEdit:true
        },
      },
    });
  }
  React.useEffect(() => {
    getAllData()
  }, [])

  return (
    <Container>
      <div className={classes.root}>
        <Grid container spacing={2}>
          {details?.map((item, index) => {
            return (
              <Grid item xs={12} sm={4}>
                <CardComponent index={index} item={item} goProperty={goProperty} />
              </Grid>
            );
          })}
        </Grid>

        <Fab
          className={isShow ? classes.fab1 : classes.fab}
          size="large"
          color="primary"
          onClick={() => sumbit()}
          onMouseOver={() => setisShow(true)}
          onMouseLeave={() => setisShow(false)}
        >
          <img src="/images/icons8-add.svg" width={"20px"} alt="alt" />
          {isShow && (
            <Typography className={classes.sumbitidea}>
              Sumbit Your Idea
            </Typography>
          )}
        </Fab>
      </div>
    </Container>
  );
};
