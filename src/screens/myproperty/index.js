import React from "react";
import { MyProperty } from './mypropertry';
import { withNavBars } from "./../../HOCs";

class MyPropertyParent extends React.Component {
  render() {
    return <MyProperty />;
  }
}

export default withNavBars(MyPropertyParent);
