import dotenv from "dotenv";
dotenv.config();

let config = {};

config.api_url = `https://dev-bricks-boy.onrender.com/`;
config.graphql_url = `${process.env.REACT_APP_GRAPHQL_PROTOCAL}${process.env.REACT_APP_GRAPHQL_HOST}${process.env.REACT_APP_GRAPHQL_PORT}${process.env.REACT_APP_GRAPHQL_BASE_URL}`;
config.graphql_web_socket_url = `${process.env.REACT_APP_GRAPHQL_WEB_SOCKET_PROTOCAL}${process.env.REACT_APP_GRAPHQL_WEB_SOCKET_HOST}${process.env.REACT_APP_GRAPHQL_WEB_SOCKET_PORT}${process.env.REACT_APP_GRAPHQL_WEB_SOCKET_BASE_URL}`;
config.serverKey = "AAAAk-03oRw:APA91bEmuyXGw62RJwxSNUSb06flr34FSzUa59RCTaJr35gROR5HE1FmaL-jcJtqTEMWu620WCmhrwWbdm7NgHKLNx21zVfebl9CzWLfd9X12tw4wzAgRm36qZ_B5C0Lt0D2IKyw56OR"
export { config };
