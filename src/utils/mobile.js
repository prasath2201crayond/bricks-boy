export const countryCode = [
    {
        "countryName": "Afghanistan",
        "dial_code": "+93",
        "code": "AF",
        "label": "Afghanistan",
        "value": "+93"
    },
    {
        "countryName": "Albania",
        "dial_code": "+355",
        "code": "AL",
        "label": "Albania",
        "value": "+355"
    },
    {
        "countryName": "Algeria",
        "dial_code": "+213",
        "code": "DZ",
        "label": "Algeria",
        "value": "+213"
    },
    {
        "countryName": "Andorra",
        "dial_code": "+376",
        "code": "AD",
        "label": "Andorra",
        "value": "+376"
    },
    {
        "countryName": "Angola",
        "dial_code": "+244",
        "code": "AO",
        "label": "Angola",
        "value": "+244"
    },
    {
        "countryName": "Antarctica",
        "dial_code": "+672",
        "code": "AQ",
        "label": "Antarctica",
        "value": "+672"
    },
    {
        "countryName": "Antigua & Barbuda",
        "dial_code": "+1268",
        "code": "AG",
        "label": "Antigua & Barbuda",
        "value": "+1268"
    },
    {
        "countryName": "Argentina",
        "dial_code": "+54",
        "code": "AR",
        "label": "Argentina",
        "value": "+54"
    },
    {
        "countryName": "Armenia",
        "dial_code": "+374",
        "code": "AM",
        "label": "Armenia",
        "value": "+374"
    },
    {
        "countryName": "Aruba",
        "dial_code": "+297",
        "code": "AW",
        "label": "Aruba",
        "value": "+297"
    },
    {
        "countryName": "Australia",
        "dial_code": "+61",
        "code": "AU",
        "label": "Australia",
        "value": "+61"
    },
    {
        "countryName": "Austria",
        "dial_code": "+43",
        "code": "AT",
        "label": "Austria",
        "value": "+43"
    },
    {
        "countryName": "Azerbaijan",
        "dial_code": "+994",
        "code": "AZ",
        "label": "Azerbaijan",
        "value": "+994"
    },
    {
        "countryName": "Bahrain",
        "dial_code": "+973",
        "code": "BH",
        "label": "Bahrain",
        "value": "+973"
    },
    {
        "countryName": "Bangladesh",
        "dial_code": "+880",
        "code": "BD",
        "label": "Bangladesh",
        "value": "+880"
    },
    {
        "countryName": "Belarus",
        "dial_code": "+375",
        "code": "BY",
        "label": "Belarus",
        "value": "+375"
    },
    {
        "countryName": "Belgium",
        "dial_code": "+32",
        "code": "BE",
        "label": "Belgium",
        "value": "+32"
    },
    {
        "countryName": "Belize",
        "dial_code": "+501",
        "code": "BZ",
        "label": "Belize",
        "value": "+501"
    },
    {
        "countryName": "Benin",
        "dial_code": "+229",
        "code": "BJ",
        "label": "Benin",
        "value": "+229"
    },
    {
        "countryName": "Bhutan",
        "dial_code": "+975",
        "code": "BT",
        "label": "Bhutan",
        "value": "+975"
    },
    {
        "countryName": "Bolivia",
        "dial_code": "+591",
        "code": "BO",
        "label": "Bolivia",
        "value": "+591"
    },
    {
        "countryName": "Bosnia & Herzegovina",
        "dial_code": "+387",
        "code": "BA",
        "label": "Bosnia & Herzegovina",
        "value": "+387"
    },
    {
        "countryName": "Botswana",
        "dial_code": "+267",
        "code": "BW",
        "label": "Botswana",
        "value": "+267"
    },
    {
        "countryName": "Brazil",
        "dial_code": "+55",
        "code": "BR",
        "label": "Brazil",
        "value": "+55"
    },
    {
        "countryName": "British IOT",
        "dial_code": "+246",
        "code": "IO",
        "label": "British IOT",
        "value": "+246"
    },
    {
        "countryName": "Brunei Darussalam",
        "dial_code": "+673",
        "code": "BN",
        "label": "Brunei Darussalam",
        "value": "+673"
    },
    {
        "countryName": "Bulgaria",
        "dial_code": "+359",
        "code": "BG",
        "label": "Bulgaria",
        "value": "+359"
    },
    {
        "countryName": "Burkina Faso",
        "dial_code": "+226",
        "code": "BF",
        "label": "Burkina Faso",
        "value": "+226"
    },
    {
        "countryName": "Burundi",
        "dial_code": "+257",
        "code": "BI",
        "label": "Burundi",
        "value": "+257"
    },
    {
        "countryName": "Cambodia",
        "dial_code": "+855",
        "code": "KH",
        "label": "Cambodia",
        "value": "+855"
    },
    {
        "countryName": "Cameroon",
        "dial_code": "+237",
        "code": "CM",
        "label": "Cameroon",
        "value": "+237"
    },
    {
        "countryName": "Cape Verde",
        "dial_code": "+238",
        "code": "CV",
        "label": "Cape Verde",
        "value": "+238"
    },
    {
        "countryName": "Cayman Islands",
        "dial_code": "+345",
        "code": "KY",
        "label": "Cayman Islands",
        "value": "+345"
    },
    {
        "countryName": "Central Africa",
        "dial_code": "+236",
        "code": "CF",
        "label": "Central Africa",
        "value": "+236"
    },
    {
        "countryName": "Chad",
        "dial_code": "+235",
        "code": "TD",
        "label": "Chad",
        "value": "+235"
    },
    {
        "countryName": "Chile",
        "dial_code": "+56",
        "code": "CL",
        "label": "Chile",
        "value": "+56"
    },
    {
        "countryName": "China",
        "dial_code": "+86",
        "code": "CN",
        "label": "China",
        "value": "+86"
    },
    {
        "countryName": "Colombia",
        "dial_code": "+57",
        "code": "CO",
        "label": "Colombia",
        "value": "+57"
    },
    {
        "countryName": "Comoros",
        "dial_code": "+269",
        "code": "KM",
        "label": "Comoros",
        "value": "+269"
    },
    {
        "countryName": "Congo",
        "dial_code": "+242",
        "code": "CG",
        "label": "Congo",
        "value": "+242"
    },
    {
        "countryName": "Congo",
        "dial_code": "+243",
        "code": "CD",
        "label": "Congo",
        "value": "+243"
    },
    {
        "countryName": "Cook Islands",
        "dial_code": "+682",
        "code": "CK",
        "label": "Cook Islands",
        "value": "+682"
    },
    {
        "countryName": "Costa Rica",
        "dial_code": "+506",
        "code": "CR",
        "label": "Costa Rica",
        "value": "+506"
    },
    {
        "countryName": "Cote d'Ivoire",
        "dial_code": "+225",
        "code": "CI",
        "label": "Cote d'Ivoire",
        "value": "+225"
    },
    {
        "countryName": "Croatia",
        "dial_code": "+385",
        "code": "HR",
        "label": "Croatia",
        "value": "+385"
    },
    {
        "countryName": "Cuba",
        "dial_code": "+53",
        "code": "CU",
        "label": "Cuba",
        "value": "+53"
    },
    {
        "countryName": "Cyprus",
        "dial_code": "+537",
        "code": "CY",
        "label": "Cyprus",
        "value": "+537"
    },
    {
        "countryName": "Czech Republic",
        "dial_code": "+420",
        "code": "CZ",
        "label": "Czech Republic",
        "value": "+420"
    },
    {
        "countryName": "Denmark",
        "dial_code": "+45",
        "code": "DK",
        "label": "Denmark",
        "value": "+45"
    },
    {
        "countryName": "Djibouti",
        "dial_code": "+253",
        "code": "DJ",
        "label": "Djibouti",
        "value": "+253"
    },
    {
        "countryName": "Ecuador",
        "dial_code": "+593",
        "code": "EC",
        "label": "Ecuador",
        "value": "+593"
    },
    {
        "countryName": "Egypt",
        "dial_code": "+20",
        "code": "EG",
        "label": "Egypt",
        "value": "+20"
    },
    {
        "countryName": "El Salvador",
        "dial_code": "+503",
        "code": "SV",
        "label": "El Salvador",
        "value": "+503"
    },
    {
        "countryName": "Equatorial Guinea",
        "dial_code": "+240",
        "code": "GQ",
        "label": "Equatorial Guinea",
        "value": "+240"
    },
    {
        "countryName": "Eritrea",
        "dial_code": "+291",
        "code": "ER",
        "label": "Eritrea",
        "value": "+291"
    },
    {
        "countryName": "Estonia",
        "dial_code": "+372",
        "code": "EE",
        "label": "Estonia",
        "value": "+372"
    },
    {
        "countryName": "Ethiopia",
        "dial_code": "+251",
        "code": "ET",
        "label": "Ethiopia",
        "value": "+251"
    },
    {
        "countryName": "Faroe Islands",
        "dial_code": "+298",
        "code": "FO",
        "label": "Faroe Islands",
        "value": "+298"
    },
    {
        "countryName": "Fiji",
        "dial_code": "+679",
        "code": "FJ",
        "label": "Fiji",
        "value": "+679"
    },
    {
        "countryName": "Finland",
        "dial_code": "+358",
        "code": "FI",
        "label": "Finland",
        "value": "+358"
    },
    {
        "countryName": "France",
        "dial_code": "+33",
        "code": "FR",
        "label": "France",
        "value": "+33"
    },
    {
        "countryName": "French Guiana",
        "dial_code": "+594",
        "code": "GF",
        "label": "French Guiana",
        "value": "+594"
    },
    {
        "countryName": "French Polynesia",
        "dial_code": "+689",
        "code": "PF",
        "label": "French Polynesia",
        "value": "+689"
    },
    {
        "countryName": "Gabon",
        "dial_code": "+241",
        "code": "GA",
        "label": "Gabon",
        "value": "+241"
    },
    {
        "countryName": "Gambia",
        "dial_code": "+220",
        "code": "GM",
        "label": "Gambia",
        "value": "+220"
    },
    {
        "countryName": "Georgia",
        "dial_code": "+995",
        "code": "GE",
        "label": "Georgia",
        "value": "+995"
    },
    {
        "countryName": "Germany",
        "dial_code": "+49",
        "code": "DE",
        "label": "Germany",
        "value": "+49"
    },
    {
        "countryName": "Ghana",
        "dial_code": "+233",
        "code": "GH",
        "label": "Ghana",
        "value": "+233"
    },
    {
        "countryName": "Gibraltar",
        "dial_code": "+350",
        "code": "GI",
        "label": "Gibraltar",
        "value": "+350"
    },
    {
        "countryName": "Greece",
        "dial_code": "+30",
        "code": "GR",
        "label": "Greece",
        "value": "+30"
    },
    {
        "countryName": "Greenland",
        "dial_code": "+299",
        "code": "GL",
        "label": "Greenland",
        "value": "+299"
    },
    {
        "countryName": "Guadeloupe",
        "dial_code": "+590",
        "code": "GP",
        "label": "Guadeloupe",
        "value": "+590"
    },
    {
        "countryName": "Guatemala",
        "dial_code": "+502",
        "code": "GT",
        "label": "Guatemala",
        "value": "+502"
    },
    {
        "countryName": "Guinea",
        "dial_code": "+224",
        "code": "GN",
        "label": "Guinea",
        "value": "+224"
    },
    {
        "countryName": "Guinea-Bissau",
        "dial_code": "+245",
        "code": "GW",
        "label": "Guinea-Bissau",
        "value": "+245"
    },
    {
        "countryName": "Haiti",
        "dial_code": "+509",
        "code": "HT",
        "label": "Haiti",
        "value": "+509"
    },
    {
        "countryName": "Holy See",
        "dial_code": "+379",
        "code": "VA",
        "label": "Holy See",
        "value": "+379"
    },
    {
        "countryName": "Honduras",
        "dial_code": "+504",
        "code": "HN",
        "label": "Honduras",
        "value": "+504"
    },
    {
        "countryName": "Hong Kong",
        "dial_code": "+852",
        "code": "HK",
        "label": "Hong Kong",
        "value": "+852"
    },
    {
        "countryName": "Hungary",
        "dial_code": "+36",
        "code": "HU",
        "label": "Hungary",
        "value": "+36"
    },
    {
        "countryName": "Iceland",
        "dial_code": "+354",
        "code": "IS",
        "label": "Iceland",
        "value": "+354"
    },
    {
        "countryName": "India",
        "dial_code": "+91",
        "code": "IN",
        "label": "India",
        "value": "+91"
    },
    {
        "countryName": "Indonesia",
        "dial_code": "+62",
        "code": "ID",
        "label": "Indonesia",
        "value": "+62"
    },
    {
        "countryName": "Iran",
        "dial_code": "+98",
        "code": "IR",
        "label": "Iran",
        "value": "+98"
    },
    {
        "countryName": "Iraq",
        "dial_code": "+964",
        "code": "IQ",
        "label": "Iraq",
        "value": "+964"
    },
    {
        "countryName": "Ireland",
        "dial_code": "+353",
        "code": "IE",
        "label": "Ireland",
        "value": "+353"
    },
    {
        "countryName": "Israel",
        "dial_code": "+972",
        "code": "IL",
        "label": "Israel",
        "value": "+972"
    },
    {
        "countryName": "Italy",
        "dial_code": "+39",
        "code": "IT",
        "label": "Italy",
        "value": "+39"
    },
    {
        "countryName": "Japan",
        "dial_code": "+81",
        "code": "JP",
        "label": "Japan",
        "value": "+81"
    },
    {
        "countryName": "Jordan",
        "dial_code": "+962",
        "code": "JO",
        "label": "Jordan",
        "value": "+962"
    },
    {
        "countryName": "Kenya",
        "dial_code": "+254",
        "code": "KE",
        "label": "Kenya",
        "value": "+254"
    },
    {
        "countryName": "Kiribati",
        "dial_code": "+686",
        "code": "KI",
        "label": "Kiribati",
        "value": "+686"
    },
    {
        "countryName": "Korea",
        "dial_code": "+82",
        "code": "KR",
        "label": "Korea",
        "value": "+82"
    },
    {
        "countryName": "Korea",
        "dial_code": "+850",
        "code": "KP",
        "label": "Korea",
        "value": "+850"
    },
    {
        "countryName": "Kuwait",
        "dial_code": "+965",
        "code": "KW",
        "label": "Kuwait",
        "value": "+965"
    },
    {
        "countryName": "Kyrgyzstan",
        "dial_code": "+996",
        "code": "KG",
        "label": "Kyrgyzstan",
        "value": "+996"
    },
    {
        "countryName": "Lao",
        "dial_code": "+856",
        "code": "LA",
        "label": "Lao",
        "value": "+856"
    },
    {
        "countryName": "Latvia",
        "dial_code": "+371",
        "code": "LV",
        "label": "Latvia",
        "value": "+371"
    },
    {
        "countryName": "Lebanon",
        "dial_code": "+961",
        "code": "LB",
        "label": "Lebanon",
        "value": "+961"
    },
    {
        "countryName": "Lesotho",
        "dial_code": "+266",
        "code": "LS",
        "label": "Lesotho",
        "value": "+266"
    },
    {
        "countryName": "Liberia",
        "dial_code": "+231",
        "code": "LR",
        "label": "Liberia",
        "value": "+231"
    },
    {
        "countryName": "Libyan Arab Jamahiriya",
        "dial_code": "+218",
        "code": "LY",
        "label": "Libyan Arab Jamahiriya",
        "value": "+218"
    },
    {
        "countryName": "Liechtenstein",
        "dial_code": "+423",
        "code": "LI",
        "label": "Liechtenstein",
        "value": "+423"
    },
    {
        "countryName": "Lithuania",
        "dial_code": "+370",
        "code": "LT",
        "label": "Lithuania",
        "value": "+370"
    },
    {
        "countryName": "Luxembourg",
        "dial_code": "+352",
        "code": "LU",
        "label": "Luxembourg",
        "value": "+352"
    },
    {
        "countryName": "Macao",
        "dial_code": "+853",
        "code": "MO",
        "label": "Macao",
        "value": "+853"
    },
    {
        "countryName": "Macedonia",
        "dial_code": "+389",
        "code": "MK",
        "label": "Macedonia",
        "value": "+389"
    },
    {
        "countryName": "Madagascar",
        "dial_code": "+261",
        "code": "MG",
        "label": "Madagascar",
        "value": "+261"
    },
    {
        "countryName": "Malawi",
        "dial_code": "+265",
        "code": "MW",
        "label": "Malawi",
        "value": "+265"
    },
    {
        "countryName": "Malaysia",
        "dial_code": "+60",
        "code": "MY",
        "label": "Malaysia",
        "value": "+60"
    },
    {
        "countryName": "Maldives",
        "dial_code": "+960",
        "code": "MV",
        "label": "Maldives",
        "value": "+960"
    },
    {
        "countryName": "Mali",
        "dial_code": "+223",
        "code": "ML",
        "label": "Mali",
        "value": "+223"
    },
    {
        "countryName": "Malta",
        "dial_code": "+356",
        "code": "MT",
        "label": "Malta",
        "value": "+356"
    },
    {
        "countryName": "Marshall Islands",
        "dial_code": "+692",
        "code": "MH",
        "label": "Marshall Islands",
        "value": "+692"
    },
    {
        "countryName": "Martinique",
        "dial_code": "+596",
        "code": "MQ",
        "label": "Martinique",
        "value": "+596"
    },
    {
        "countryName": "Mauritania",
        "dial_code": "+222",
        "code": "MR",
        "label": "Mauritania",
        "value": "+222"
    },
    {
        "countryName": "Mauritius",
        "dial_code": "+230",
        "code": "MU",
        "label": "Mauritius",
        "value": "+230"
    },
    {
        "countryName": "Mayotte",
        "dial_code": "+262",
        "code": "YT",
        "label": "Mayotte",
        "value": "+262"
    },
    {
        "countryName": "Mexico",
        "dial_code": "+52",
        "code": "MX",
        "label": "Mexico",
        "value": "+52"
    },
    {
        "countryName": "Micronesia",
        "dial_code": "+691",
        "code": "FM",
        "label": "Micronesia",
        "value": "+691"
    },
    {
        "countryName": "Moldova",
        "dial_code": "+373",
        "code": "MD",
        "label": "Moldova",
        "value": "+373"
    },
    {
        "countryName": "Monaco",
        "dial_code": "+377",
        "code": "MC",
        "label": "Monaco",
        "value": "+377"
    },
    {
        "countryName": "Mongolia",
        "dial_code": "+976",
        "code": "MN",
        "label": "Mongolia",
        "value": "+976"
    },
    {
        "countryName": "Montenegro",
        "dial_code": "+382",
        "code": "ME",
        "label": "Montenegro",
        "value": "+382"
    },
    {
        "countryName": "Montserrat",
        "dial_code": "+1664",
        "code": "MS",
        "label": "Montserrat",
        "value": "+1664"
    },
    {
        "countryName": "Morocco",
        "dial_code": "+212",
        "code": "MA",
        "label": "Morocco",
        "value": "+212"
    },
    {
        "countryName": "Mozambique",
        "dial_code": "+258",
        "code": "MZ",
        "label": "Mozambique",
        "value": "+258"
    },
    {
        "countryName": "Myanmar",
        "dial_code": "+95",
        "code": "MM",
        "label": "Myanmar",
        "value": "+95"
    },
    {
        "countryName": "Namibia",
        "dial_code": "+264",
        "code": "NA",
        "label": "Namibia",
        "value": "+264"
    },
    {
        "countryName": "Nauru",
        "dial_code": "+674",
        "code": "NR",
        "label": "Nauru",
        "value": "+674"
    },
    {
        "countryName": "Nepal",
        "dial_code": "+977",
        "code": "NP",
        "label": "Nepal",
        "value": "+977"
    },
    {
        "countryName": "Netherlands",
        "dial_code": "+31",
        "code": "NL",
        "label": "Netherlands",
        "value": "+31"
    },
    {
        "countryName": "Netherlands Antilles",
        "dial_code": "+599",
        "code": "AN",
        "label": "Netherlands Antilles",
        "value": "+599"
    },
    {
        "countryName": "New Caledonia",
        "dial_code": "+687",
        "code": "NC",
        "label": "New Caledonia",
        "value": "+687"
    },
    {
        "countryName": "New Zealand",
        "dial_code": "+64",
        "code": "NZ",
        "label": "New Zealand",
        "value": "+64"
    },
    {
        "countryName": "Nicaragua",
        "dial_code": "+505",
        "code": "NI",
        "label": "Nicaragua",
        "value": "+505"
    },
    {
        "countryName": "Niger",
        "dial_code": "+227",
        "code": "NE",
        "label": "Niger",
        "value": "+227"
    },
    {
        "countryName": "Nigeria",
        "dial_code": "+234",
        "code": "NG",
        "label": "Nigeria",
        "value": "+234"
    },
    {
        "countryName": "Niue",
        "dial_code": "+683",
        "code": "NU",
        "label": "Niue",
        "value": "+683"
    },
    {
        "countryName": "Norway",
        "dial_code": "+47",
        "code": "NO",
        "label": "Norway",
        "value": "+47"
    },
    {
        "countryName": "Oman",
        "dial_code": "+968",
        "code": "OM",
        "label": "Oman",
        "value": "+968"
    },
    {
        "countryName": "Pakistan",
        "dial_code": "+92",
        "code": "PK",
        "label": "Pakistan",
        "value": "+92"
    },
    {
        "countryName": "Palau",
        "dial_code": "+680",
        "code": "PW",
        "label": "Palau",
        "value": "+680"
    },
    {
        "countryName": "Palestinia",
        "dial_code": "+970",
        "code": "PS",
        "label": "Palestinia",
        "value": "+970"
    },
    {
        "countryName": "Panama",
        "dial_code": "+507",
        "code": "PA",
        "label": "Panama",
        "value": "+507"
    },
    {
        "countryName": "Papua New Guinea",
        "dial_code": "+675",
        "code": "PG",
        "label": "Papua New Guinea",
        "value": "+675"
    },
    {
        "countryName": "Paraguay",
        "dial_code": "+595",
        "code": "PY",
        "label": "Paraguay",
        "value": "+595"
    },
    {
        "countryName": "Peru",
        "dial_code": "+51",
        "code": "PE",
        "label": "Peru",
        "value": "+51"
    },
    {
        "countryName": "Philippines",
        "dial_code": "+63",
        "code": "PH",
        "label": "Philippines",
        "value": "+63"
    },
    {
        "countryName": "Pitcairn",
        "dial_code": "+872",
        "code": "PN",
        "label": "Pitcairn",
        "value": "+872"
    },
    {
        "countryName": "Poland",
        "dial_code": "+48",
        "code": "PL",
        "label": "Poland",
        "value": "+48"
    },
    {
        "countryName": "Portugal",
        "dial_code": "+351",
        "code": "PT",
        "label": "Portugal",
        "value": "+351"
    },
    {
        "countryName": "Qatar",
        "dial_code": "+974",
        "code": "QA",
        "label": "Qatar",
        "value": "+974"
    },
    {
        "countryName": "Romania",
        "dial_code": "+40",
        "code": "RO",
        "label": "Romania",
        "value": "+40"
    },
    {
        "countryName": "Russia",
        "dial_code": "+7",
        "code": "RU",
        "label": "Russia",
        "value": "+7"
    },
    {
        "countryName": "Rwanda",
        "dial_code": "+250",
        "code": "RW",
        "label": "Rwanda",
        "value": "+250"
    },
    {
        "countryName": "Saint Helena",
        "dial_code": "+290",
        "code": "SH",
        "label": "Saint Helena",
        "value": "+290"
    },
    {
        "countryName": "Saint Pierre",
        "dial_code": "+508",
        "code": "PM",
        "label": "Saint Pierre",
        "value": "+508"
    },
    {
        "countryName": "Samoa",
        "dial_code": "+685",
        "code": "WS",
        "label": "Samoa",
        "value": "+685"
    },
    {
        "countryName": "San Marino",
        "dial_code": "+378",
        "code": "SM",
        "label": "San Marino",
        "value": "+378"
    },
    {
        "countryName": "Sao Tome",
        "dial_code": "+239",
        "code": "ST",
        "label": "Sao Tome",
        "value": "+239"
    },
    {
        "countryName": "Saudi Arabia",
        "dial_code": "+966",
        "code": "SA",
        "label": "Saudi Arabia",
        "value": "+966"
    },
    {
        "countryName": "Senegal",
        "dial_code": "+221",
        "code": "SN",
        "label": "Senegal",
        "value": "+221"
    },
    {
        "countryName": "Serbia",
        "dial_code": "+381",
        "code": "RS",
        "label": "Serbia",
        "value": "+381"
    },
    {
        "countryName": "Seychelles",
        "dial_code": "+248",
        "code": "SC",
        "label": "Seychelles",
        "value": "+248"
    },
    {
        "countryName": "Sierra Leone",
        "dial_code": "+232",
        "code": "SL",
        "label": "Sierra Leone",
        "value": "+232"
    },
    {
        "countryName": "Singapore",
        "dial_code": "+65",
        "code": "SG",
        "label": "Singapore",
        "value": "+65"
    },
    {
        "countryName": "Slovakia",
        "dial_code": "+421",
        "code": "SK",
        "label": "Slovakia",
        "value": "+421"
    },
    {
        "countryName": "Slovenia",
        "dial_code": "+386",
        "code": "SI",
        "label": "Slovenia",
        "value": "+386"
    },
    {
        "countryName": "Solomon Islands",
        "dial_code": "+677",
        "code": "SB",
        "label": "Solomon Islands",
        "value": "+677"
    },
    {
        "countryName": "Somalia",
        "dial_code": "+252",
        "code": "SO",
        "label": "Somalia",
        "value": "+252"
    },
    {
        "countryName": "South Africa",
        "dial_code": "+27",
        "code": "ZA",
        "label": "South Africa",
        "value": "+27"
    },
    {
        "countryName": "South Georgia",
        "dial_code": "+500",
        "code": "GS",
        "label": "South Georgia",
        "value": "+500"
    },
    {
        "countryName": "Spain",
        "dial_code": "+34",
        "code": "ES",
        "label": "Spain",
        "value": "+34"
    },
    {
        "countryName": "Sri Lanka",
        "dial_code": "+94",
        "code": "LK",
        "label": "Sri Lanka",
        "value": "+94"
    },
    {
        "countryName": "Sudan",
        "dial_code": "+249",
        "code": "SD",
        "label": "Sudan",
        "value": "+249"
    },
    {
        "countryName": "Suriname",
        "dial_code": "+597",
        "code": "SR",
        "label": "Suriname",
        "value": "+597"
    },
    {
        "countryName": "Swaziland",
        "dial_code": "+268",
        "code": "SZ",
        "label": "Swaziland",
        "value": "+268"
    },
    {
        "countryName": "Sweden",
        "dial_code": "+46",
        "code": "SE",
        "label": "Sweden",
        "value": "+46"
    },
    {
        "countryName": "Switzerland",
        "dial_code": "+41",
        "code": "CH",
        "label": "Switzerland",
        "value": "+41"
    },
    {
        "countryName": "Syria",
        "dial_code": "+963",
        "code": "SY",
        "label": "Syria",
        "value": "+963"
    },
    {
        "countryName": "Taiwan",
        "dial_code": "+886",
        "code": "TW",
        "label": "Taiwan",
        "value": "+886"
    },
    {
        "countryName": "Tajikistan",
        "dial_code": "+992",
        "code": "TJ",
        "label": "Tajikistan",
        "value": "+992"
    },
    {
        "countryName": "Tanzania",
        "dial_code": "+255",
        "code": "TZ",
        "label": "Tanzania",
        "value": "+255"
    },
    {
        "countryName": "Thailand",
        "dial_code": "+66",
        "code": "TH",
        "label": "Thailand",
        "value": "+66"
    },
    {
        "countryName": "Timor-Leste",
        "dial_code": "+670",
        "code": "TL",
        "label": "Timor-Leste",
        "value": "+670"
    },
    {
        "countryName": "Togo",
        "dial_code": "+228",
        "code": "TG",
        "label": "Togo",
        "value": "+228"
    },
    {
        "countryName": "Tokelau",
        "dial_code": "+690",
        "code": "TK",
        "label": "Tokelau",
        "value": "+690"
    },
    {
        "countryName": "Tonga",
        "dial_code": "+676",
        "code": "TO",
        "label": "Tonga",
        "value": "+676"
    },
    {
        "countryName": "Tunisia",
        "dial_code": "+216",
        "code": "TN",
        "label": "Tunisia",
        "value": "+216"
    },
    {
        "countryName": "Turkey",
        "dial_code": "+90",
        "code": "TR",
        "label": "Turkey",
        "value": "+90"
    },
    {
        "countryName": "Turkmenistan",
        "dial_code": "+993",
        "code": "TM",
        "label": "Turkmenistan",
        "value": "+993"
    },
    {
        "countryName": "Tuvalu",
        "dial_code": "+688",
        "code": "TV",
        "label": "Tuvalu",
        "value": "+688"
    },
    {
        "countryName": "UAE",
        "dial_code": "+971",
        "code": "AE",
        "label": "UAE",
        "value": "+971"
    },
    {
        "countryName": "Uganda",
        "dial_code": "+256",
        "code": "UG",
        "label": "Uganda",
        "value": "+256"
    },
    {
        "countryName": "Ukraine",
        "dial_code": "+380",
        "code": "UA",
        "label": "Ukraine",
        "value": "+380"
    },
    {
        "countryName": "United Kingdom",
        "dial_code": "+44",
        "code": "GB",
        "label": "United Kingdom",
        "value": "+44"
    },
    {
        "countryName": "United States",
        "dial_code": "+1",
        "code": "US",
        "label": "United States",
        "value": "+1"
    },
    {
        "countryName": "Uruguay",
        "dial_code": "+598",
        "code": "UY",
        "label": "Uruguay",
        "value": "+598"
    },
    {
        "countryName": "Uzbekistan",
        "dial_code": "+998",
        "code": "UZ",
        "label": "Uzbekistan",
        "value": "+998"
    },
    {
        "countryName": "Vanuatu",
        "dial_code": "+678",
        "code": "VU",
        "label": "Vanuatu",
        "value": "+678"
    },
    {
        "countryName": "Venezuela",
        "dial_code": "+58",
        "code": "VE",
        "label": "Venezuela",
        "value": "+58"
    },
    {
        "countryName": "Viet Nam",
        "dial_code": "+84",
        "code": "VN",
        "label": "Viet Nam",
        "value": "+84"
    },
    {
        "countryName": "Wallis & Futuna",
        "dial_code": "+681",
        "code": "WF",
        "label": "Wallis & Futuna",
        "value": "+681"
    },
    {
        "countryName": "Yemen",
        "dial_code": "+967",
        "code": "YE",
        "label": "Yemen",
        "value": "+967"
    },
    {
        "countryName": "Zambia",
        "dial_code": "+260",
        "code": "ZM",
        "label": "Zambia",
        "value": "+260"
    },
    {
        "countryName": "Zimbabwe",
        "dial_code": "+263",
        "code": "ZW",
        "label": "Zimbabwe",
        "value": "+263"
    }
]


export const currencyCode = [
    { "name": "Afghan Afghani", "value": "AFA", "label": "؋" },
    { "name": "Albanian Lek", "value": "ALL", "label": "Lek" },
    { "name": "Algerian Dinar", "value": "DZD", "label": "دج" },
    { "name": "Angolan Kwanza", "value": "AOA", "label": "Kz" },
    { "name": "Argentine Peso", "value": "ARS", "label": "$" },
    { "name": "Armenian Dram", "value": "AMD", "label": "֏" },
    { "name": "Aruban Florin", "value": "AWG", "label": "ƒ" },
    { "name": "Australian Dollar", "value": "AUD", "label": "$" },
    { "name": "Azerbaijani Manat", "value": "AZN", "label": "m" },
    { "name": "Bahamian Dollar", "value": "BSD", "label": "B$" },
    { "name": "Bahraini Dinar", "value": "BHD", "label": ".د.ب" },
    { "name": "Bangladeshi Taka", "value": "BDT", "label": "৳" },
    { "name": "Barbadian Dollar", "value": "BBD", "label": "Bds$" },
    { "name": "Belarusian Ruble", "value": "BYR", "label": "Br" },
    { "name": "Belgian Franc", "value": "BEF", "label": "fr" },
    { "name": "Belize Dollar", "value": "BZD", "label": "$" },
    { "name": "Bermudan Dollar", "value": "BMD", "label": "$" },
    { "name": "Bhutanese Ngultrum", "value": "BTN", "label": "Nu." },
    { "name": "Bitcoin", "value": "BTC", "label": "฿" },
    { "name": "Bolivian Boliviano", "value": "BOB", "label": "Bs." },
    { "name": "Bosnia-Herzegovina Convertible Mark", "value": "BAM", "label": "KM" },
    { "name": "Botswanan Pula", "value": "BWP", "label": "P" },
    { "name": "Brazilian Real", "value": "BRL", "label": "R$" },
    { "name": "British Pound Sterling", "value": "GBP", "label": "£" },
    { "name": "Brunei Dollar", "value": "BND", "label": "B$" },
    { "name": "Bulgarian Lev", "value": "BGN", "label": "Лв." },
    { "name": "Burundian Franc", "value": "BIF", "label": "FBu" },
    { "name": "Cambodian Riel", "value": "KHR", "label": "KHR" },
    { "name": "Canadian Dollar", "value": "CAD", "label": "$" },
    { "name": "Cape Verdean Escudo", "value": "CVE", "label": "$" },
    { "name": "Cayman Islands Dollar", "value": "KYD", "label": "$" },
    { "name": "CFA Franc BCEAO", "value": "XOF", "label": "CFA" },
    { "name": "CFA Franc BEAC", "value": "XAF", "label": "FCFA" },
    { "name": "CFP Franc", "value": "XPF", "label": "₣" },
    { "name": "Chilean Peso", "value": "CLP", "label": "$" },
    { "name": "Chinese Yuan", "value": "CNY", "label": "¥" },
    { "name": "Colombian Peso", "value": "COP", "label": "$" },
    { "name": "Comorian Franc", "value": "KMF", "label": "CF" },
    { "name": "Congolese Franc", "value": "CDF", "label": "FC" },
    { "name": "Costa Rican ColÃ³n", "value": "CRC", "label": "₡" },
    { "name": "Croatian Kuna", "value": "HRK", "label": "kn" },
    { "name": "Cuban Convertible Peso", "value": "CUC", "label": "$, CUC" },
    { "name": "Czech Republic Koruna", "value": "CZK", "label": "Kč" },
    { "name": "Danish Krone", "value": "DKK", "label": "Kr." },
    { "name": "Djiboutian Franc", "value": "DJF", "label": "Fdj" },
    { "name": "Dominican Peso", "value": "DOP", "label": "$" },
    { "name": "East Caribbean Dollar", "value": "XCD", "label": "$" },
    { "name": "Egyptian Pound", "value": "EGP", "label": "ج.م" },
    { "name": "Eritrean Nakfa", "value": "ERN", "label": "Nfk" },
    { "name": "Estonian Kroon", "value": "EEK", "label": "kr" },
    { "name": "Ethiopian Birr", "value": "ETB", "label": "Nkf" },
    { "name": "Euro", "value": "EUR", "label": "€" },
    { "name": "Falkland Islands Pound", "value": "FKP", "label": "£" },
    { "name": "Fijian Dollar", "value": "FJD", "label": "FJ$" },
    { "name": "Gambian Dalasi", "value": "GMD", "label": "D" },
    { "name": "Georgian Lari", "value": "GEL", "label": "ლ" },
    { "name": "German Mark", "value": "DEM", "label": "DM" },
    { "name": "Ghanaian Cedi", "value": "GHS", "label": "GH₵" },
    { "name": "Gibraltar Pound", "value": "GIP", "label": "£" },
    { "name": "Greek Drachma", "value": "GRD", "label": "₯, Δρχ, Δρ" },
    { "name": "Guatemalan Quetzal", "value": "GTQ", "label": "Q" },
    { "name": "Guinean Franc", "value": "GNF", "label": "FG" },
    { "name": "Guyanaese Dollar", "value": "GYD", "label": "$" },
    { "name": "Haitian Gourde", "value": "HTG", "label": "G" },
    { "name": "Honduran Lempira", "value": "HNL", "label": "L" },
    { "name": "Hong Kong Dollar", "value": "HKD", "label": "$" },
    { "name": "Hungarian Forint", "value": "HUF", "label": "Ft" },
    { "name": "Icelandic KrÃ³na", "value": "ISK", "label": "kr" },
    { "name": "Indian Rupee", "value": "INR", "label": "₹" },
    { "name": "Indonesian Rupiah", "value": "IDR", "label": "Rp" },
    { "name": "Iranian Rial", "value": "IRR", "label": "﷼" },
    { "name": "Iraqi Dinar", "value": "IQD", "label": "د.ع" },
    { "name": "Israeli New Sheqel", "value": "ILS", "label": "₪" },
    { "name": "Italian Lira", "value": "ITL", "label": "L,£" },
    { "name": "Jamaican Dollar", "value": "JMD", "label": "J$" },
    { "name": "Japanese Yen", "value": "JPY", "label": "¥" },
    { "name": "Jordanian Dinar", "value": "JOD", "label": "ا.د" },
    { "name": "Kazakhstani Tenge", "value": "KZT", "label": "лв" },
    { "name": "Kenyan Shilling", "value": "KES", "label": "KSh" },
    { "name": "Kuwaiti Dinar", "value": "KWD", "label": "ك.د" },
    { "name": "Kyrgystani Som", "value": "KGS", "label": "лв" },
    { "name": "Laotian Kip", "value": "LAK", "label": "₭" },
    { "name": "Latvian Lats", "value": "LVL", "label": "Ls" },
    { "name": "Lebanese Pound", "value": "LBP", "label": "£" },
    { "name": "Lesotho Loti", "value": "LSL", "label": "L" },
    { "name": "Liberian Dollar", "value": "LRD", "label": "$" },
    { "name": "Libyan Dinar", "value": "LYD", "label": "د.ل" },
    { "name": "Lithuanian Litas", "value": "LTL", "label": "Lt" },
    { "name": "Macanese Pataca", "value": "MOP", "label": "$" },
    { "name": "Macedonian Denar", "value": "MKD", "label": "ден" },
    { "name": "Malagasy Ariary", "value": "MGA", "label": "Ar" },
    { "name": "Malawian Kwacha", "value": "MWK", "label": "MK" },
    { "name": "Malaysian Ringgit", "value": "MYR", "label": "RM" },
    { "name": "Maldivian Rufiyaa", "value": "MVR", "label": "Rf" },
    { "name": "Mauritanian Ouguiya", "value": "MRO", "label": "MRU" },
    { "name": "Mauritian Rupee", "value": "MUR", "label": "₨" },
    { "name": "Mexican Peso", "value": "MXN", "label": "$" },
    { "name": "Moldovan Leu", "value": "MDL", "label": "L" },
    { "name": "Mongolian Tugrik", "value": "MNT", "label": "₮" },
    { "name": "Moroccan Dirham", "value": "MAD", "label": "MAD" },
    { "name": "Mozambican Metical", "value": "MZM", "label": "MT" },
    { "name": "Myanmar Kyat", "value": "MMK", "label": "K" },
    { "name": "Namibian Dollar", "value": "NAD", "label": "$" },
    { "name": "Nepalese Rupee", "value": "NPR", "label": "₨" },
    { "name": "Netherlands Antillean Guilder", "value": "ANG", "label": "ƒ" },
    { "name": "New Taiwan Dollar", "value": "TWD", "label": "$" },
    { "name": "New Zealand Dollar", "value": "NZD", "label": "$" },
    { "name": "Nicaraguan CÃ³rdoba", "value": "NIO", "label": "C$" },
    { "name": "Nigerian Naira", "value": "NGN", "label": "₦" },
    { "name": "North Korean Won", "value": "KPW", "label": "₩" },
    { "name": "Norwegian Krone", "value": "NOK", "label": "kr" },
    { "name": "Omani Rial", "value": "OMR", "label": ".ع.ر" },
    { "name": "Pakistani Rupee", "value": "PKR", "label": "₨" },
    { "name": "Panamanian Balboa", "value": "PAB", "label": "B/." },
    { "name": "Papua New Guinean Kina", "value": "PGK", "label": "K" },
    { "name": "Paraguayan Guarani", "value": "PYG", "label": "₲" },
    { "name": "Peruvian Nuevo Sol", "value": "PEN", "label": "S/." },
    { "name": "Philippine Peso", "value": "PHP", "label": "₱" },
    { "name": "Polish Zloty", "value": "PLN", "label": "zł" },
    { "name": "Qatari Rial", "value": "QAR", "label": "ق.ر" },
    { "name": "Romanian Leu", "value": "RON", "label": "lei" },
    { "name": "Russian Ruble", "value": "RUB", "label": "₽" },
    { "name": "Rwandan Franc", "value": "RWF", "label": "FRw" },
    { "name": "Salvadoran ColÃ³n", "value": "SVC", "label": "₡" },
    { "name": "Samoan Tala", "value": "WST", "label": "SAT" },
    { "name": "Saudi Riyal", "value": "SAR", "label": "﷼" },
    { "name": "Serbian Dinar", "value": "RSD", "label": "din" },
    { "name": "Seychellois Rupee", "value": "SCR", "label": "SRe" },
    { "name": "Sierra Leonean Leone", "value": "SLL", "label": "Le" },
    { "name": "Singapore Dollar", "value": "SGD", "label": "$" },
    { "name": "Slovak Koruna", "value": "SKK", "label": "Sk" },
    { "name": "Solomon Islands Dollar", "value": "SBD", "label": "Si$" },
    { "name": "Somali Shilling", "value": "SOS", "label": "Sh.so." },
    { "name": "South African Rand", "value": "ZAR", "label": "R" },
    { "name": "South Korean Won", "value": "KRW", "label": "₩" },
    { "name": "Special Drawing Rights", "value": "XDR", "label": "SDR" },
    { "name": "Sri Lankan Rupee", "value": "LKR", "label": "Rs" },
    { "name": "St. Helena Pound", "value": "SHP", "label": "£" },
    { "name": "Sudanese Pound", "value": "SDG", "label": ".س.ج" },
    { "name": "Surinamese Dollar", "value": "SRD", "label": "$" },
    { "name": "Swazi Lilangeni", "value": "SZL", "label": "E" },
    { "name": "Swedish Krona", "value": "SEK", "label": "kr" },
    { "name": "Swiss Franc", "value": "CHF", "label": "CHf" },
    { "name": "Syrian Pound", "value": "SYP", "label": "LS" },
    { "name": "São Tomé and Príncipe Dobra", "value": "STD", "label": "Db" },
    { "name": "Tajikistani Somoni", "value": "TJS", "label": "SM" },
    { "name": "Tanzanian Shilling", "value": "TZS", "label": "TSh" },
    { "name": "Thai Baht", "value": "THB", "label": "฿" },
    { "name": "Tongan Pa'anga", "value": "TOP", "label": "$" },
    { "name": "Trinidad & Tobago Dollar", "value": "TTD", "label": "$" },
    { "name": "Tunisian Dinar", "value": "TND", "label": "ت.د" },
    { "name": "Turkish Lira", "value": "TRY", "label": "₺" },
    { "name": "Turkmenistani Manat", "value": "TMT", "label": "T" },
    { "name": "Ugandan Shilling", "value": "UGX", "label": "USh" },
    { "name": "Ukrainian Hryvnia", "value": "UAH", "label": "₴" },
    { "name": "United Arab Emirates Dirham", "value": "AED", "label": "إ.د" },
    { "name": "Uruguayan Peso", "value": "UYU", "label": "$" },
    { "name": "US Dollar", "value": "USD", "label": "$" },
    { "name": "Uzbekistan Som", "value": "UZS", "label": "лв" },
    { "name": "Vanuatu Vatu", "value": "VUV", "label": "VT" },
    { "name": "Venezuelan BolÃvar", "value": "VEF", "label": "Bs" },
    { "name": "Vietnamese Dong", "value": "VND", "label": "₫" },
    { "name": "Yemeni Rial", "value": "YER", "label": "﷼" },
    { "name": "Zambian Kwacha", "value": "ZMK", "label": "ZK" }
];

export const vegitable = [
    {
        value: "acorn squash",
        label: "acorn squash",
    },
    {
        value: "alfalfa sprout",
        label: "alfalfa sprout",
    },
    {
        value: "amaranth",
        label: "amaranth",
    },
    {
        value: "anise",
        label: "anise",
    },
    {
        value: "artichoke",
        label: "artichoke",
    },
    {
        value: "arugula",
        label: "arugula",
    },
    {
        value: "asparagus",
        label: "asparagus",
    },
    {
        value: "aubergine",
        label: "aubergine",
    },
    {
        value: "azuki bean",
        label: "azuki bean",
    },
    {
        value: "banana squash",
        label: "banana squash",
    },
    {
        value: "basil",
        label: "basil",
    },
    {
        value: "bean sprout",
        label: "bean sprout",
    },
    {
        value: "beet",
        label: "beet",
    },
    {
        value: "black bean",
        label: "black bean",
    },
    {
        value: "black-eyed pea",
        label: "black-eyed pea",
    },
    {
        value: "bok choy",
        label: "bok choy",
    },
    {
        value: "borlotti bean",
        label: "borlotti bean",
    },
    {
        value: "broad beans",
        label: "broad beans",
    },

    {
        value: "broccoflower",
        label: "broccoflower",
    },
    {
        value: "broccoli",
        label: "broccoli",
    },
    {
        value: "brussels sprout",
        label: "brussels sprout",
    },
    {
        value: "butternut squash",
        label: "butternut squash",
    },
    {
        value: "cabbage",
        label: "cabbage",
    },
    {
        value: "calabrese",
        label: "calabrese",
    },
    {
        value: "caraway",
        label: "caraway",
    },
    {
        value: "carrot",
        label: "carrot",
    },
    {
        value: "cauliflower",
        label: "cauliflower",
    },
    {
        value: "cayenne pepper",
        label: "cayenne pepper",
    },
    {
        value: "celeriac",
        label: "celeriac",
    },
    {
        value: "celery",
        label: "celery",
    },
    {
        value: "chamomile",
        label: "chamomile",
    },
    {
        value: "chard",
        label: "chard",
    },
    {
        value: "chayote",
        label: "chayote",
    },
    {
        value: "chickpea",
        label: "chickpea",
    },
    {
        value: "chives",
        label: "chives",
    },
    {
        value: "cilantro",
        label: "cilantro",
    },



    {
        value: "collard green",
        label: "collard green",
    },
    {
        value: "corn",
        label: "corn",
    },
    {
        value: "corn salad",
        label: "corn salad",
    },
    {
        value: "courgette",
        label: "courgette",
    },
    {
        value: "cucumber",
        label: "cucumber",
    },
    {
        value: "daikon",
        label: "daikon",
    },
    {
        value: "delicata",
        label: "delicata",
    },
    {
        value: "dill",
        label: "dill",
    },
    {
        value: "eggplant",
        label: "eggplant",
    },
    {
        value: "endive",
        label: "endive",
    },
    {
        value: "fennel",
        label: "fennel",
    },
    {
        value: "fiddlehead",
        label: "fiddlehead",
    },
    {
        value: "frisee",
        label: "frisee",
    },
    {
        value: "garlic",
        label: "garlic",
    },
    {
        value: "gem squash",
        label: "gem squash",
    },
    {
        value: "ginger",
        label: "ginger",
    },
    {
        value: "green bean",
        label: "green bean",
    },
    {
        value: "green pepper",
        label: "green pepper",
    },

    {
        value: "habanero",
        label: "habanero",
    },
    {
        value: "herbs and spice",
        label: "herbs and spice",
    },
    {
        value: "horseradish",
        label: "horseradish",
    },
    {
        value: "hubbard squash",
        label: "hubbard squash",
    },
    {
        value: "jalapeno",
        label: "jalapeno",
    },
    {
        value: "jalapeno",
        label: "jalapeno",
    },
    {
        value: "jicama",
        label: "jicama",
    },
    {
        value: "kale",
        label: "kale",
    },
    {
        value: "kidney bean",
        label: "kidney bean",
    },
    {
        value: "kohlrabi",
        label: "kohlrabi",
    },
    {
        value: "lavender",
        label: "lavender",
    },
    {
        value: "leek ",
        label: "leek ",
    },
    {
        value: "legume",
        label: "legume",
    },
    {
        value: "lemon grass",
        label: "lemon grass",
    },
    {
        value: "lentils",
        label: "lentils",
    },
    {
        value: "lettuce",
        label: "lettuce",
    },
    {
        value: "lima bean",
        label: "lima bean",
    },
    {
        value: "mamey",
        label: "mamey",
    },
]