import { CssBaseline } from "@mui/material";
import React from "react";
import AppAlert from "./App.alert";
import AppAuth from "./App.auth";
import AppBackdrop from "./App.backdrop";
import AppDialog from "./App.dialog";
import AppDrawer from "./App.drawer";
import AppErrorBoundary from "./App.errorBoundry";
import ApolloClient from "./App.gqlclient";
import AppTheme from "./App.theme";
import RouterApp from "./router";
import AppFirebase from "./App.firebase"; //For Push Notification thing

const App = () => {
  return (
    <ApolloClient>
      <AppErrorBoundary>
        <AppAuth>
          <AppTheme>
            <CssBaseline />
            <AppAlert>
              <AppDialog>
                <AppDrawer>
                  <AppBackdrop>
                    <AppFirebase>
                      <RouterApp />
                    </AppFirebase>
                  </AppBackdrop>
                </AppDrawer>
              </AppDialog>
            </AppAlert>
          </AppTheme>
        </AppAuth>
      </AppErrorBoundary>
    </ApolloClient>
  );
}
export default App;