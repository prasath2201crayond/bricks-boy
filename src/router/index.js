import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  // Redirect,
} from "react-router-dom";
import { Routes } from "./routes";
import PrivateRouter from "./privateRouter";

import {
  NotFound,
  Home,
  Login,
  Profile,
  CreatePost,
  PropertyDeatils,
  MyProperty
} from './../screens';
import { Register } from '../screens/login/register'
import { WelcomePage } from '../screens/login/welcomepage'
import CreateProfile from '../screens/profile/createprofile'
import {MessageWeb} from '../screens/profile/messageweb'

const RouterApp = (props) => {

  return (
    <Router>
      <Switch>
        {/* WelcomePage Route */}
        <Route exact path={Routes.welcomepage}>
          <WelcomePage />
        </Route>


        {/* Login Route */}
        <Route exact path={Routes.login}>
          <Login />
        </Route>

        {/* Register Route */}
        <Route exact path={Routes.register}>
          <Register />
        </Route>

        {/* Home Route */}
        <PrivateRouter exact path={Routes.home}>
          <Home />
        </PrivateRouter>

        {/* Profile Route */}
        <PrivateRouter exact path={Routes.profile}>
          <Profile />
        </PrivateRouter>



        {/* CreateProfile Route */}
        <Route exact path={Routes.createprofile}>
          <CreateProfile />
        </Route>

        {/* CreatePost Route */}
        <Route exact path={Routes.createpost}>
          <CreatePost />
        </Route>


        <PrivateRouter exact path={Routes.propertyview}>
          <PropertyDeatils />
        </PrivateRouter>

        <PrivateRouter exact path={Routes.myproperties}>
          <MyProperty />
        </PrivateRouter>

        <PrivateRouter exact path={Routes.messages}>
          <MessageWeb />
        </PrivateRouter>
        {/* For unknow/non-defined path */}
        <Route exact path="*" component={NotFound} />
      </Switch>
    </Router>
  );
};

export default RouterApp;
