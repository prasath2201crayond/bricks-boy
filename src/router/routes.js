export const Routes = {
  home: "/",
  welcomepage: "/welcomepage",
  login: "/login",
  register: "/register",
  profile:'/profile',
  createprofile:'/createprofile',
  createpost:'/createpost',
  propertyview:'/propertyview',
  myproperties:'/myproperties',
  messages:'/messages'
};
